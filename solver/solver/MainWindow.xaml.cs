﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using solver.Dto;

namespace solver
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public static ViewModel viewModel { get; set; }

        public MainWindow()
        {
            InitializeComponent();
        }

        private void AddDisciplina(object sender, RoutedEventArgs e)
        {
            if (String.IsNullOrEmpty(nomeDisciplina.Text))
                return;

            viewModel.Disciplinas.Add(new Disciplina(nomeDisciplina.Text));

            atualizaTela();
         }

        private void MainWindow_OnLoaded(object sender, RoutedEventArgs e)
        {
            viewModel = new ViewModel();
            DataContext = viewModel;
        }

        private void AddProfessor(object sender, RoutedEventArgs e)
        {
            if (String.IsNullOrEmpty(nomeProfessor.Text))
                return;

            if (viewModel.AltaPreferencia.ToList().Where(l => l.Checked).Select(l => l.Codigo)
                .Intersect(
                    viewModel.MediaPreferencia.ToList().Concat(viewModel.Indisponibilidade).Where(l => l.Checked).Select(w => w.Codigo)
                ).Any())
            {
                MessageBox.Show("Não é possível escolher a mesma disciplina como preferência média, alta ou como indisponibilidade");
                return;
            }

            Professor professor = new Professor(viewModel.AltaPreferencia.ToList().Where(l => l.Checked).ToList(),
                viewModel.MediaPreferencia.ToList().Where(l => l.Checked).ToList(), nomeProfessor.Text, viewModel.Indisponibilidade.ToList(), "8");
            viewModel.Professores.Add(professor);

            atualizaTela();
        }

        private void atualizaTela()
        {
            nomeProfessor.Text = "";
            nomeDisciplina.Text = "";
            viewModel.AltaPreferencia.Clear();
            viewModel.MediaPreferencia.Clear();
            viewModel.Horarios.Clear();
            viewModel.Turmas.ToList().ForEach(l => viewModel.AltaPreferencia.Add((Turma) l.Clone()));
            viewModel.Turmas.ToList().ForEach(l => viewModel.MediaPreferencia.Add((Turma) l.Clone()));
            viewModel.Turmas.ToList().ForEach(l => viewModel.Indisponibilidade.Add((Turma)l.Clone()));
            viewModel.RaisePropertyChanged("MediaPreferencia");
            viewModel.RaisePropertyChanged("AltaPreferencia");
            viewModel.RaisePropertyChanged("Disciplina");
            viewModel.RaisePropertyChanged("Turmas");
            viewModel.RaisePropertyChanged("Indisponibilidade");
        }

        private void addHorarioTurma(object sender, RoutedEventArgs e)
        {
            DateTime dataInicial;
            DateTime dataFinal;
            if (!DateTime.TryParse(horarioInicial.Text, out dataInicial) || !DateTime.TryParse(horarioFinal.Text, out dataFinal))
            {
                MessageBox.Show("Formato de data invalido");
                return;
            }

            if (dataFinal <= dataInicial)
            {
                MessageBox.Show("Fim da aula deve ser posterior ao seu inicio");
                return;
            }

            if (viewModel.Horarios.Any(l => l.DiaSemana.ToString() == comboBoxDiaSemana.Text && l.DataInicial == horarioInicial.Text))
            {
                MessageBox.Show("Este horário já esta cadastrado pra essa displina");
                return;
            }

            viewModel.Horarios.Add(new Horario(comboBoxDiaSemana.Text, horarioInicial.Text, horarioFinal.Text));
        }

        private void addTurma(object sender, RoutedEventArgs e)
        {
         
            Turma turma = new Turma(comboBoxDisc.SelectedItem as Disciplina, viewModel.Horarios.ToList(), "68", Semestre.Semestre_1);
            viewModel.Turmas.Add(turma);
            atualizaTela();
        }

        private void btnGerar_Click(object sender, RoutedEventArgs e)
        {
            viewModel.gerarArquivo();

        }

        
    }
}
