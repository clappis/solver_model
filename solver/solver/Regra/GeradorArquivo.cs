﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using solver.Dto;

namespace solver.Regra
{
    public static class GeradorArquivo
    {

        public static void exec(List<Dto.Professor> Professores, List<Dto.Turma> Turmas)
        {
            var sb = getTexto(Professores, Turmas);

            using (System.IO.StreamWriter file =
            new System.IO.StreamWriter(@"C:\arquivo.txt", true))
            {
                file.Write(sb);
            }
        }

        public static String getTexto(List<Professor> Professores, List<Turma> Turmas)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("MAXIMIZE ");
            sb.Append("obj: ");
            bool flag = false;

            sb.Append(geraFuncaoObj(Professores, Turmas));

            sb.AppendLine();
            sb.AppendLine("SUBJECT TO");

            sb.Append(geraRestricaoDaRelacaoTurmaProfessor(Professores, Turmas));

            sb.Append(geraRestricaoDeConflitosDeHorarios(Professores, Turmas));

            sb.Append(geraRestricaoDeCargasHorarias(Professores, Turmas));

            sb.Append(geraRestricaoDeIntraTurno(Professores, Turmas));

            sb.Append(geraRestricaoDeIndisponibilidade(Professores));

            sb.Append(geraRestricaoDeTurmaConsolidada(Professores, Turmas));

            sb.AppendLine("BINARY");

            sb.Append(getVariaveisBinarias(Professores, Turmas));

            

            sb.AppendLine("END");
            return sb.ToString();
        }

        private static StringBuilder geraRestricaoDeTurmaConsolidada(List<Professor> professores, List<Turma> turmas)
        {
            StringBuilder sb = new StringBuilder();

            foreach (var turma in turmas.Where(e => e.Professor != null))
            {
                sb.AppendLine("P" + turma.Professor.Codigo + "T" + turma.Codigo + " = 1");
            }

            return sb;
        }

        private static StringBuilder geraRestricaoDeIndisponibilidade(List<Professor> professores)
        {
            bool flag = false;
            StringBuilder sb = new StringBuilder();

            foreach (var professor in professores)
            {
                foreach (var turma in professor.Indisponibilidade)
                {
                    if (flag)
                        sb.Append(" + ");

                    sb.AppendLine("P" + professor.Codigo + "T" + turma.Codigo + " = 0");
                    flag = true;
                }

                flag = false;
            }

            return sb;
        }

        private static StringBuilder geraRestricaoDeIntraTurno(List<Professor> professores, List<Turma> turmas)
        {
            var turmasEmConflito = getTurmasEmConflito(turmas);
            bool flag = false;
            StringBuilder sb = new StringBuilder();

            foreach (var key in turmasEmConflito.Keys.Where(e => e.EndsWith("21:20Semestre_1")))
            {
                List<Turma> turmasAux;
                turmasEmConflito.TryGetValue(key, out turmasAux);
                foreach (var turma in turmasAux)
                {
                    if (key.Equals("Segunda21:20Semestre_1"))
                    {
                        foreach (var professor in professores)
                        {
                            List<Turma> turmasConflito;
                            if (!turmasEmConflito.TryGetValue("Terça07:45Semestre_1", out turmasConflito))
                                turmasConflito = new List<Turma>();

                            foreach (var turmaConflito in turmasConflito)
                            {
                                if (flag)
                                    sb.Append(" + ");

                                sb.Append("P" + professor.Codigo + "T" + turmaConflito.Codigo);
                                flag = true;
                            }
                            if (flag)
                            {
                                sb.Append(" + P" + professor.Codigo + "T" + turma.Codigo);
                                sb.Append(" <= 1 ");
                                sb.AppendLine();
                            }

                            flag = false;

                            
                        }
                    }


                    else if (key.Equals("Terça21:20Semestre_1"))
                    {
                        foreach (var professor in professores)
                        {
                            List<Turma> turmasConflito;
                            if (!turmasEmConflito.TryGetValue("Quarta07:45Semestre_1", out turmasConflito))
                                turmasConflito = new List<Turma>();

                            foreach (var turmaConflito in turmasConflito)
                            {
                                if (flag)
                                    sb.Append(" + ");

                                sb.Append("P" + professor.Codigo + "T" + turmaConflito.Codigo);
                                flag = true;
                            }
                            if (flag)
                            {
                                sb.Append(" + P" + professor.Codigo + "T" + turma.Codigo);
                                sb.Append(" <= 1 ");
                                sb.AppendLine();
                            }

                            flag = false;


                        }
                    }

                    else if (key.Equals("Quarta21:20Semestre_1"))
                    {
                        foreach (var professor in professores)
                        {
                            List<Turma> turmasConflito;
                            if (!turmasEmConflito.TryGetValue("Quinta07:45Semestre_1", out turmasConflito))
                                turmasConflito = new List<Turma>();

                            foreach (var turmaConflito in turmasConflito)
                            {
                                if (flag)
                                    sb.Append(" + ");

                                sb.Append("P" + professor.Codigo + "T" + turmaConflito.Codigo);
                                flag = true;
                            }
                            if (flag)
                            {
                                sb.Append(" + P" + professor.Codigo + "T" + turma.Codigo);
                                sb.Append(" <= 1 ");
                                sb.AppendLine();
                            }

                            flag = false;


                        }
                    }


                    else if (key.Equals("Quinta21:20Semestre_1"))
                    {
                        foreach (var professor in professores)
                        {
                            List<Turma> turmasConflito;
                            if (!turmasEmConflito.TryGetValue("Sexta07:45Semestre_1", out turmasConflito))
                                turmasConflito = new List<Turma>();

                            foreach (var turmaConflito in turmasConflito)
                            {
                                if (flag)
                                    sb.Append(" + ");

                                sb.Append("P" + professor.Codigo + "T" + turmaConflito.Codigo);
                                flag = true;
                            }
                            if (flag)
                            {
                                sb.Append(" + P" + professor.Codigo + "T" + turma.Codigo);
                                sb.Append(" <= 1 ");
                                sb.AppendLine();
                            }

                            flag = false;


                        }
                    }

                    else if (key.Equals("Sexta21:20Semestre_1"))
                    {
                        foreach (var professor in professores)
                        {
                            List<Turma> turmasConflito;
                            if (!turmasEmConflito.TryGetValue("Sabado07:45Semestre_1", out turmasConflito))
                                turmasConflito = new List<Turma>();

                            foreach (var turmaConflito in turmasConflito)
                            {
                                if (flag)
                                    sb.Append(" + ");

                                sb.Append("P" + professor.Codigo + "T" + turmaConflito.Codigo);
                                flag = true;
                            }
                            if (flag)
                            {
                                sb.Append(" + P" + professor.Codigo + "T" + turma.Codigo);
                                sb.Append(" <= 1 ");
                                sb.AppendLine();
                            }

                            flag = false;
                        }
                    }
                }

            }

            

            foreach (var key in turmasEmConflito.Keys.Where(e => e.EndsWith("21:20Semestre_2")))
            {
                List<Turma> turmasAux;
                turmasEmConflito.TryGetValue(key, out turmasAux);
                foreach (var turma in turmasAux)
                {
                    if (key.Equals("Segunda21:20Semestre_2"))
                    {
                        foreach (var professor in professores)
                        {
                            List<Turma> turmasConflito;
                            if (!turmasEmConflito.TryGetValue("Terça07:45Semestre_2", out turmasConflito))
                                turmasConflito = new List<Turma>();

                            foreach (var turmaConflito in turmasConflito)
                            {
                                if (flag)
                                    sb.Append(" + ");

                                sb.Append("P" + professor.Codigo + "T" + turmaConflito.Codigo);
                                flag = true;
                            }
                            if (flag)
                            {
                                sb.Append(" + P" + professor.Codigo + "T" + turma.Codigo);
                                sb.Append(" <= 1 ");
                                sb.AppendLine();
                            }

                            flag = false;


                        }
                    }


                    else if (key.Equals("Terça21:20Semestre_2"))
                    {
                        foreach (var professor in professores)
                        {
                            List<Turma> turmasConflito;
                            if (!turmasEmConflito.TryGetValue("Quarta07:45Semestre_2", out turmasConflito))
                                turmasConflito = new List<Turma>();

                            foreach (var turmaConflito in turmasConflito)
                            {
                                if (flag)
                                    sb.Append(" + ");

                                sb.Append("P" + professor.Codigo + "T" + turmaConflito.Codigo);
                                flag = true;
                            }
                            if (flag)
                            {
                                sb.Append(" + P" + professor.Codigo + "T" + turma.Codigo);
                                sb.Append(" <= 1 ");
                                sb.AppendLine();
                            }

                            flag = false;


                        }
                    }

                    else if (key.Equals("Quarta21:20Semestre_2"))
                    {
                        foreach (var professor in professores)
                        {
                            List<Turma> turmasConflito;
                            if (!turmasEmConflito.TryGetValue("Quinta07:45Semestre_2", out turmasConflito))
                                turmasConflito = new List<Turma>();

                            foreach (var turmaConflito in turmasConflito)
                            {
                                if (flag)
                                    sb.Append(" + ");

                                sb.Append("P" + professor.Codigo + "T" + turmaConflito.Codigo);
                                flag = true;
                            }
                            if (flag)
                            {
                                sb.Append(" + P" + professor.Codigo + "T" + turma.Codigo);
                                sb.Append(" <= 1 ");
                                sb.AppendLine();
                            }

                            flag = false;


                        }
                    }


                    else if (key.Equals("Quinta21:00Semestre_2"))
                    {
                        foreach (var professor in professores)
                        {
                            List<Turma> turmasConflito;
                            if (!turmasEmConflito.TryGetValue("Sexta07:45Semestre_2", out turmasConflito))
                                turmasConflito = new List<Turma>();

                            foreach (var turmaConflito in turmasConflito)
                            {
                                if (flag)
                                    sb.Append(" + ");

                                sb.Append("P" + professor.Codigo + "T" + turmaConflito.Codigo);
                                flag = true;
                            }
                            if (flag)
                            {
                                sb.Append(" + P" + professor.Codigo + "T" + turma.Codigo);
                                sb.Append(" <= 1 ");
                                sb.AppendLine();
                            }

                            flag = false;


                        }
                    }

                    else if (key.Equals("Sexta21:20Semestre_2"))
                    {
                        foreach (var professor in professores)
                        {
                            List<Turma> turmasConflito;
                            if (!turmasEmConflito.TryGetValue("Sabado07:45Semestre_2", out turmasConflito))
                                turmasConflito = new List<Turma>();

                            foreach (var turmaConflito in turmasConflito)
                            {
                                if (flag)
                                    sb.Append(" + ");

                                sb.Append("P" + professor.Codigo + "T" + turmaConflito.Codigo);
                                flag = true;
                            }
                            if (flag)
                            {
                                sb.Append(" + P" + professor.Codigo + "T" + turma.Codigo);
                                sb.Append(" <= 1 ");
                                sb.AppendLine();
                            }

                            flag = false;
                        }
                    }
                }

            }




            return sb;
        }

        private static StringBuilder geraRestricaoDeCargasHorarias(List<Professor> professores, List<Turma> turmas)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(geraRestricaoDeCargasHorariasIntegral(professores, turmas));
            sb.Append(geraRestricaoDeCargasHorariasNoturna(professores, turmas));
            sb.Append(geraRestricaoDeDistribuicaoCargaHoraria(professores, turmas));
            return sb;
        }

        private static StringBuilder geraRestricaoDeCargasHorariasNoturna(List<Professor> professores, List<Turma> turmas)
        {
               StringBuilder sb = new StringBuilder();
            StringBuilder sbTemp = new StringBuilder();
            bool flag = false;

            foreach (var professor in professores)
            {
                foreach (var turma in turmas)
                {
                    if (!turma.Horarios.Any(e => e.IsNoturno))
                        continue;

                    if (flag)
                        sbTemp.Append(" + ");

                    sbTemp.Append((turma.Horarios.Where(e => e.IsNoturno).Count()).ToString().Replace(",", ".") + " P" + professor.Codigo + "T" + turma.Codigo);
                    //sbTemp.Append((turma.Horarios.Where(e => e.IsNoturno).Count() * 0.83).ToString().Replace(",",".") +  " P" + professor.Codigo + "T" + turma.Codigo);
                    flag = true;
                }
                if (flag)
                {
                    sb.AppendLine(sbTemp + " >= 2 ");
                }
                
                sbTemp.Clear();
                flag = false;

            }

            return sb;
        }
        // 50 = 60
        // x    100
        private static StringBuilder geraRestricaoDeCargasHorariasIntegral(List<Professor> professores, List<Turma> turmas)
        {
            StringBuilder sb = new StringBuilder();
            StringBuilder sbTemp = new StringBuilder();
            bool flag = false;

            foreach (var professor in professores)
            {
                foreach (var turma in turmas)
                {
                    if (flag)
                        sbTemp.Append(" + ");

                    sbTemp.Append(turma.Horarios.Count + " P" + professor.Codigo + "T" + turma.Codigo);
                    //sbTemp.Append((turma.Horarios.Count * 0.83).ToString().Replace(",",".") + " P" + professor.Codigo + "T" + turma.Codigo);
                    flag = true;
                }
                sb.AppendLine(sbTemp + " >=  " + professor.CargaMinima);
                sb.AppendLine(sbTemp + " <= 20 ");
                sbTemp.Clear();
                flag = false;
            }

            return sb;
        }

        private static StringBuilder geraRestricaoDeDistribuicaoCargaHoraria(List<Professor> professores, List<Turma> turmas)
        {
            StringBuilder sb = new StringBuilder();
            StringBuilder sbTemp = new StringBuilder();
            bool flag = false;

            foreach (var professor in professores)
            {
                foreach (var turma in turmas)
                {
                    if (flag)
                        sbTemp.Append(" + ");

                    sbTemp.Append(((16 / Int32.Parse(professor.CargaMinima)) * turma.Horarios.Count).ToString().Replace(",", ".") + " P" + professor.Codigo + "T" + turma.Codigo);
                    //sbTemp.Append(((16/Int32.Parse(professor.CargaMinima)) * turma.Horarios.Count * 0.83).ToString().Replace(",", ".") + " P" + professor.Codigo + "T" + turma.Codigo);
                    flag = true;
                }

                sb.AppendLine(sbTemp + " - L <= 0");
                sbTemp.Clear();
                flag = false;
            }

            return sb;
        }

        private static StringBuilder getVariaveisBinarias(List<Professor> Professores, List<Turma> Turmas)
        {
            StringBuilder sb = new StringBuilder();

            foreach (var turma in Turmas)
                foreach (var professor in Professores)
                    sb.AppendLine("P" + professor.Codigo + "T" + turma.Codigo);

            return sb;
        }

        private static StringBuilder geraRestricaoDeConflitosDeHorarios(List<Professor> Professores, List<Turma> Turmas)
        {
            var turmasEmConflito = getTurmasEmConflito(Turmas);

            StringBuilder sb = new StringBuilder();
            Boolean flag = false;
            
            foreach (var turmas in turmasEmConflito)
            {
                if (turmas.Value.Count > 1)
                    foreach (var professor in Professores)
                    {
                        foreach (var turma in turmas.Value)
                        {
                            if (flag)
                                sb.Append(" + ");

                            sb.Append("P" + professor.Codigo + "T" + turma.Codigo);
                            flag = true;
                        }
                        sb.Append(" <= 1 ");
                        sb.AppendLine();
                        flag = false;
                    }
            }

            return sb;
        }

        private static Dictionary<string, List<Turma>> getTurmasEmConflito(List<Turma> Turmas)
        {
            Dictionary<String, List<Turma>> turmasEmConflito = new Dictionary<string, List<Turma>>();
            foreach (var turma in Turmas.Where(e => e.Semestre == Semestre.Semestre_1 || e.Semestre == Semestre.Anual ))
            {
                foreach (var horarios in turma.Horarios)
                {
                    if (turma.Semestre == Semestre.Anual)
                    {
                        if (
                            !turmasEmConflito.ContainsKey(horarios.DiaSemana + horarios.DataInicial +
                                                          Semestre.Semestre_1))
                            turmasEmConflito.Add(horarios.DiaSemana + horarios.DataInicial + Semestre.Semestre_1,
                                new List<Turma>() {turma});
                        else
                        {
                            List<Turma> turmasAux;
                            turmasEmConflito.TryGetValue(
                                horarios.DiaSemana + horarios.DataInicial + Semestre.Semestre_1, out turmasAux);
                            turmasEmConflito.Remove(horarios.DiaSemana + horarios.DataInicial + Semestre.Semestre_1);
                            turmasAux.Add(turma);
                            turmasEmConflito.Add(horarios.DiaSemana + horarios.DataInicial + Semestre.Semestre_1,
                                turmasAux);
                        }

                        if (
                            !turmasEmConflito.ContainsKey(horarios.DiaSemana + horarios.DataInicial +
                                                          Semestre.Semestre_2))
                            turmasEmConflito.Add(horarios.DiaSemana + horarios.DataInicial + Semestre.Semestre_2,
                                new List<Turma>() {turma});
                        else
                        {
                            List<Turma> turmasAux;
                            turmasEmConflito.TryGetValue(
                                horarios.DiaSemana + horarios.DataInicial + Semestre.Semestre_2, out turmasAux);
                            turmasEmConflito.Remove(horarios.DiaSemana + horarios.DataInicial + Semestre.Semestre_2);
                            turmasAux.Add(turma);
                            turmasEmConflito.Add(horarios.DiaSemana + horarios.DataInicial + Semestre.Semestre_2,
                                turmasAux);
                        }

                    }
                    else
                    {
                        if (!turmasEmConflito.ContainsKey(horarios.DiaSemana + horarios.DataInicial + turma.Semestre))
                            turmasEmConflito.Add(horarios.DiaSemana + horarios.DataInicial + turma.Semestre,
                                new List<Turma>() {turma});
                        else
                        {
                            List<Turma> turmasAux;
                            turmasEmConflito.TryGetValue(horarios.DiaSemana + horarios.DataInicial + turma.Semestre, out turmasAux);
                            turmasEmConflito.Remove(horarios.DiaSemana + horarios.DataInicial + turma.Semestre);
                            turmasAux.Add(turma);
                            turmasEmConflito.Add(horarios.DiaSemana + horarios.DataInicial + turma.Semestre, turmasAux);
                        }
                    }
                }
            }

            return turmasEmConflito;
        }

        private static StringBuilder geraRestricaoDaRelacaoTurmaProfessor(List<Professor> Professores, List<Turma> Turmas)
        {
            StringBuilder sb = new StringBuilder();
            Boolean flag = false;

            foreach (var turma in Turmas)
            {
                foreach (var professor in Professores)
                {
                    if (flag)
                        sb.Append(" + ");

                    sb.Append("P" + professor.Codigo + "T" + turma.Codigo);
                    flag = true;
                }

                flag = false;

                sb.Append(" = 1 ");
                sb.AppendLine();
            }

            return sb;
        }

        private static StringBuilder geraFuncaoObj(List<Professor> Professores, List<Turma> Turmas)
        {
            StringBuilder sb = new StringBuilder();
            Boolean flag = false;

            foreach (var turma in Turmas)
            {
                foreach (var professor in Professores)
                {
                    if (flag)
                        sb.Append(" + ");

                    if (professor.TurmaAltaPreferencia.Any(e => e.Codigo == turma.Codigo))
                        sb.Append(" 3 P" + professor.Codigo + "T" + turma.Codigo);
                    else if (professor.TurmaMediaPreferencia.Any(e => e.Codigo == turma.Codigo))
                        sb.Append(" 2 P" + professor.Codigo + "T" + turma.Codigo);
                    else
                        sb.Append(" 1 P" + professor.Codigo + "T" + turma.Codigo);

                    flag = true;
                }
            }

            return sb.Append(" - 1 L ");
        }
    }
}
