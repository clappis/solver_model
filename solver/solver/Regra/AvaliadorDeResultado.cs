﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using solver.Dto;

namespace solver.Regra
{
    public class AvaliadorDeResultado
    {
        public void avalia(String resultado, List<Professor> professores, List<Turma> turmas)
        {
            Dictionary<String, String> turmasConsolidadas = new Dictionary<String, String>();
            double indiceTotal = 0;

            var split = resultado.Split('\n');

            foreach (var linha in split)
            {
                var index = linha.IndexOf("T");
                turmasConsolidadas.Add(linha.Substring(index).Replace("\r", ""), linha.Substring(0, index).Replace("\r",""));
            }

            foreach (var professor in professores)
            {
                double valorReal = 0;
                double valor = 0;

                foreach (var turma in professor.TurmaAltaPreferencia.Concat(professor.TurmasConsolidadas))
                {
                    string codigoProf;
                    turmasConsolidadas.TryGetValue("T" + turma.Codigo, out codigoProf);
                    if (codigoProf.Equals("P" + professor.Codigo))
                        valorReal += 3;

                    valor += 3;
                }

                foreach (var turma in professor.TurmaMediaPreferencia)
                {
                    string codigoProf;
                    turmasConsolidadas.TryGetValue("T" + turma.Codigo, out codigoProf);
                    if (codigoProf.Equals("P" + professor.Codigo))
                        valorReal += 2;

                    valor += 2;
                }

                //var baixaPref = new List<Turma>(turmas.Except(professor.TurmaMediaPreferencia.Concat(professor.TurmaAltaPreferencia)));

                
                //foreach (var turma in  baixaPref)
                //{
                //    string codigoProf;
                //    turmasConsolidadas.TryGetValue("T" + turma.Codigo, out codigoProf);
                //    if (codigoProf.Equals("P" + professor.Codigo))
                //        valorReal += 1;

                //    valor += 1;
                //}

                professor.IndiceSatisfacao = valorReal/valor;
                indiceTotal += valorReal/valor;
                
            }



             if (true)
                return;
        }

        private Turma buscaTurma(string substring)
        {
            throw new NotImplementedException();
        }

        private Professor buscaProfessor(string substring)
        {
            throw new NotImplementedException();
        }
    }
}
