﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solver.Dto
{
    public class Disciplina : ICloneable
    {
        public string Nome { get; set; }
        public int Codigo { get; set; }
        public bool Checked { get; set; }

        public Disciplina(String nome)
        {
            this.Nome = nome;
            Codigo = DataBase.getSequencieDisciplinas();
        }

        public Disciplina(String nome, int codigo)
        {
            this.Nome = nome;
            this.Codigo = codigo;
        }

        public override string ToString()
        {
            return Nome;
        }

        public object Clone()
        {
            Disciplina dic = new Disciplina(Nome, Codigo);
            dic.Codigo = Codigo;
            dic.Checked = Checked;
            return dic;
        }
    }
    
}
