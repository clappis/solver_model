﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solver.Dto
{
    public class Turma : ICloneable
    {
        public List<Horario> Horarios;
        public Professor Professor { get; set; }
        public String Codigo { get; set; }
        public Disciplina Disciplina { get; set; }
        public bool Checked { get; set; }
        public String CargaHoraria { get; set; }
        public Semestre Semestre { get; set; }

        public String Nome { get { return Disciplina != null ? 
            Codigo + " " + Disciplina.Nome + " " + horarioPrint()  : ""; } }


        public String HorariosTexto { get { return horarioPrint(); } }

        private string horarioPrint()
        {
            String text = ";";
            foreach (var horario in Horarios)
            {
                text += horario.DiaSemana + " " + horario.DataInicial + horario.DataFinal;
            }

            return text;
        }

        public Turma() { }

        public Turma(Disciplina disciplina, List<Horario> horarios, string CargaHoraria, Semestre Semestre)
        {
            Horarios = horarios;
            Disciplina = disciplina;
            this.CargaHoraria = CargaHoraria;
            this.Semestre = Semestre;
            Codigo = DataBase.getSequencieTurmas().ToString();
        }

        public Turma(Disciplina disciplina, List<Horario> horarios, string CargaHoraria, Semestre Semestre, Professor professor)
        {
            Horarios = horarios;
            this.Professor = professor;
            Disciplina = disciplina;
            this.CargaHoraria = CargaHoraria;
            this.Semestre = Semestre;
            Codigo = DataBase.getSequencieTurmas().ToString();
        }


        public Turma(Disciplina disciplina, List<Horario> horarios, String CargaHoraria, Semestre Semestre, int Codigo)
        {
            Horarios = horarios;
            Disciplina = disciplina;
            this.CargaHoraria = CargaHoraria;
            this.Semestre = Semestre;
            this.Codigo = Codigo.ToString();
        }

        public Turma(Disciplina disciplina, List<Horario> horarios, String CargaHoraria, Semestre Semestre, Professor professor, int Codigo)
        {
            Horarios = horarios;
            Professor = professor;
            professor.TurmasConsolidadas.Add(this);
            Disciplina = disciplina;
            this.CargaHoraria = CargaHoraria;
            this.Semestre = Semestre;
            this.Codigo = Codigo.ToString();
        }

        public Turma(Disciplina disciplina, List<Horario> horarios, String CargaHoraria, Semestre Semestre, String Codigo)
        {
            Horarios = horarios;
            Disciplina = disciplina;
            this.CargaHoraria = CargaHoraria;
            this.Semestre = Semestre;
            this.Codigo =  Codigo;
        }

        public Turma(Disciplina disciplina, List<Horario> horarios, String CargaHoraria, Semestre Semestre, Professor professor, String Codigo)
        {
            Horarios = horarios;
            Professor = professor;
            Disciplina = disciplina;
            professor.TurmasConsolidadas.Add(this);
            this.CargaHoraria = CargaHoraria;
            this.Semestre = Semestre;
            this.Codigo = Codigo;
        }



        public object Clone()
        {
            Turma turma = new Turma();
            turma.Disciplina = Disciplina;
            turma.Horarios = Horarios;
            turma.Codigo = Codigo;
            turma.Checked = Checked;
            return turma;
        }
    }
}
