﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solver.Dto
{
    public class Professor
    {
        public List<Turma> Indisponibilidade  { get; set; }
        public string CargaMinima { get; set; }
        public string Nome { get; set; }
        public int Codigo { get; set; }
        public List<Turma> TurmaAltaPreferencia { get; set; }
        public List<Turma> TurmaMediaPreferencia { get; set; }

        public List<Turma> TurmasConsolidadas = new List<Turma>();

        public double IndiceSatisfacao { get; set; }

        public String MediaPreferenciaString
        {
            get
            {
                return TurmaMediaPreferencia == null | !TurmaMediaPreferencia.Any()
                    ? "Sem preferência"
                    : turmaToString(TurmaMediaPreferencia);
            }
        }

        private string turmaToString(List<Turma> dic )
        {
            String texto = "";
            foreach (var disciplina in dic)
            {
                texto += disciplina.Nome + " - ";
            }

            return texto;
        }

        public String AltaPreferenciaString
        {
            get
            {
                return TurmaAltaPreferencia == null | !TurmaAltaPreferencia.Any() ? "Sem preferência" : turmaToString(TurmaAltaPreferencia);;
            }
        }

        public Professor(List<Turma> turmaAltaPreferencia, List<Turma> turmaMediaPreferencia, string nome, List<Turma> indisponibilidade, string cargaMinima)
        {
            Indisponibilidade = indisponibilidade;
            CargaMinima = cargaMinima;
            TurmaMediaPreferencia = turmaMediaPreferencia;
            TurmaAltaPreferencia = turmaAltaPreferencia;
            Nome = nome;
            Codigo = DataBase.getSequencieProfessores();
        }

        public override string ToString()
        {
            return Nome;
        }
    }
}
