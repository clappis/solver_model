﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solver.Dto
{
    public class Horario
    {
        public bool IsNoturno { get; set; }
        public String DataInicial { get; set; }
        public String DataFinal { get; set; }
        public Dia DiaSemana { get; set; }

        public Horario(String DiaSemana, String DataInicial, String DataFinal)
        {
            Dia dia;
            Dia.TryParse(DiaSemana, true, out dia);
            this.DiaSemana = dia;
            this.DataInicial = DataInicial;
            this.DataFinal = DataFinal;
            this.IsNoturno = DataInicial.Equals("19:30") || DataInicial.Equals("21:20");
        }


        public Horario(Dia DiaSemana, String DataInicial, String DataFinal)
        {
            this.DiaSemana = DiaSemana;
            this.DataInicial = DataInicial;
            this.DataFinal = DataFinal;
            this.IsNoturno = DataInicial.Equals("19:30") || DataInicial.Equals("21:20");
        }

        public override string ToString()
        {
            return DiaSemana + " " + DataInicial + DataFinal + " | ";
        }
    }
}
