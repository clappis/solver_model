﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using solver.Dto;

namespace solver
{
    public static class DataBase
    {
        private static int _sequencieDisplinas;
        private static int _sequencieProfessores;
        private static int _sequencieTurmas;

        public static List<Disciplina> disciplinas = new List<Disciplina>();

        internal static int getSequencieDisciplinas()
        {
            return _sequencieDisplinas++;
        }

        internal static int getSequencieProfessores()
        {
            return _sequencieProfessores++;
        }

        internal static int getSequencieTurmas()
        {
            return _sequencieTurmas++;
        }

        public static void resetBase()
        {
            _sequencieDisplinas = 0;
            _sequencieProfessores = 0;
            _sequencieTurmas = 0;
        }
    }
}
