﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using solver.Dto;
using solver.Regra;

namespace solver.Tests
{
    [TestClass]
    public class GeradorArquivoTest
    {
        [TestCleanup]
        public void cleanUp()
        {
            DataBase.resetBase();
        }

        [TestMethod]
        public void testaGeracaoArquivoHappyDay()
        {
            var dic0 = new Disciplina("DISC 0");
            var dic1 = new Disciplina("DISC 1");
            var dic2 = new Disciplina("DISC 2");

            var turma0 = new Turma(dic0, new List<Horario>() {new Horario("Segunda", "07:45", "09:25")}, "68", Semestre.Semestre_1);
            var turma1 = new Turma(dic1, new List<Horario>() { new Horario("Terça", "07:45", "09:25") }, "68", Semestre.Semestre_1);
            var turma2 = new Turma(dic2, new List<Horario>() { new Horario("Quarta", "07:45", "09:25") }, "68", Semestre.Semestre_1);

            var professor0 = new Professor(new List<Turma>() {turma0}, new List<Turma>() {turma1}, "Prof 0", new List<Turma>(), "8");
            var professor1 = new Professor(new List<Turma>() { turma1 }, new List<Turma>() { turma2 }, "Prof 1", new List<Turma>(), "8");
            var professor2 = new Professor(new List<Turma>() { turma2 }, new List<Turma>() { turma1 }, "Prof 2", new List<Turma>(), "8");

            string texto = GeradorArquivo.getTexto(new List<Professor>() {professor0, professor1, professor2},
                new List<Turma>() {turma0, turma1, turma2});

            var textoEsperado = "MAXIMIZE obj:  3 P0T0 +  1 P1T0 +  1 P2T0 +  2 P0T1 +  3 P1T1 +  2 P2T1 +  1 P0T2 +  2 P1T2 +  3 P2T2\r\nSUBJECT TO\r\nP0T0 + P1T0 + P2T0 = 1 \r\nP0T1 + P1T1 + P2T1 = 1 \r\nP0T2 + P1T2 + P2T2 = 1 \r\n1 P0T0 + 1 P0T1 + 1 P0T2 >= 20 \r\n1 P0T0 + 1 P0T1 + 1 P0T2 <= 40 \r\n1 P1T0 + 1 P1T1 + 1 P1T2 >= 20 \r\n1 P1T0 + 1 P1T1 + 1 P1T2 <= 40 \r\n1 P2T0 + 1 P2T1 + 1 P2T2 >= 20 \r\n1 P2T0 + 1 P2T1 + 1 P2T2 <= 40 \r\nBINARY\r\nP0T0\r\nP1T0\r\nP2T0\r\nP0T1\r\nP1T1\r\nP2T1\r\nP0T2\r\nP1T2\r\nP2T2\r\nEND\r\n";

            Assert.AreEqual(textoEsperado.ToString(), texto);
        }

        [TestMethod]
        public void testaGeracaoArquivoParaApendice()
        {
            var dic0 = new Disciplina("Algoritmos");
            var dic1 = new Disciplina("Engenharia de Software I");
            var dic2 = new Disciplina("Compiladores");

            var turma0 = new Turma(dic1, new List<Horario>() { new Horario("Segunda", "19:30", "20:20"), new Horario("Segunda", "19:30", "20:20") }, "68", Semestre.Semestre_1);
            var turma1 = new Turma(dic0, new List<Horario>() { new Horario("Terça", "07:45", "08:35"), new Horario("Terça", "08:35", "09:25") }, "68", Semestre.Semestre_1);
            var turma2 = new Turma(dic2, new List<Horario>() { new Horario("Quarta", "07:45", "08:35"), new Horario("Quarta", "08:35", "09:25") }, "68", Semestre.Semestre_1);
            var turma3 = new Turma(dic2, new List<Horario>() { new Horario("Quarta", "07:45", "08:35"), new Horario("Quarta", "08:35", "09:25") }, "68", Semestre.Semestre_1);


            var professor0 = new Professor(new List<Turma>() { turma3 }, new List<Turma>() { turma2 }, "Prof 0", new List<Turma>(){turma1}, "8");
            var professor1 = new Professor(new List<Turma>() { turma1 }, new List<Turma>(), "Prof 1", new List<Turma>() {turma3}, "8");
            var professor2 = new Professor(new List<Turma>() { turma0 }, new List<Turma>() { turma2 }, "Prof 2", new List<Turma>(), "8");

            turma1.Professor = professor1;

            string texto = GeradorArquivo.getTexto(new List<Professor>() { professor0, professor1, professor2 },
                new List<Turma>() { turma0, turma1, turma2 });

            var textoEsperado = "";

            Assert.AreEqual(textoEsperado.ToString(), texto);
        }


        [TestMethod]
        public void testaGeracaoArquivoConflitoHorarios()
        {
            var dic0 = new Disciplina("DISC 0");
            var dic1 = new Disciplina("DISC 1");
            var dic2 = new Disciplina("DISC 2");
            var dic3 = new Disciplina("DISC 3");

            var turma0 = new Turma(dic0, new List<Horario>() { new Horario("Segunda", "07:45", "09:25") }, "68", Semestre.Semestre_1);
            var turma1 = new Turma(dic1, new List<Horario>() { new Horario("Terça", "07:45", "09:25") }, "68", Semestre.Semestre_1);
            var turma2 = new Turma(dic2, new List<Horario>() { new Horario("Quarta", "07:45", "09:25") }, "68", Semestre.Semestre_1);
            var turma3 = new Turma(dic3, new List<Horario>() { new Horario("Segunda", "07:45", "09:25") }, "68", Semestre.Semestre_1);

            var professor0 = new Professor(new List<Turma>() { turma0, turma3 }, new List<Turma>() { turma1 }, "Prof 0", new List<Turma>(), "8");
            var professor1 = new Professor(new List<Turma>() { turma1 }, new List<Turma>() { turma2, turma3 }, "Prof 1", new List<Turma>(), "8");
            var professor2 = new Professor(new List<Turma>() { turma2 }, new List<Turma>() { turma1 }, "Prof 2", new List<Turma>(), "8");

            string texto = GeradorArquivo.getTexto(new List<Professor>() { professor0, professor1, professor2 },
                new List<Turma>() { turma0, turma1, turma2, turma3 });

            var textoEsperado =
                "MAXIMIZE obj:  3 P0T0 +  1 P1T0 +  1 P2T0 +  2 P0T1 +  3 P1T1 +  2 P2T1 +  1 P0T2 +  2 P1T2 +  3 P2T2 +  3 P0T3 +  2 P1T3 +  1 P2T3\r\nSUBJECT TO\r\nP0T0 + P1T0 + P2T0 = 1 \r\nP0T1 + P1T1 + P2T1 = 1 \r\nP0T2 + P1T2 + P2T2 = 1 \r\nP0T3 + P1T3 + P2T3 = 1 \r\nP0T0 + P0T3 <= 1 \r\nP1T0 + P1T3 <= 1 \r\nP2T0 + P2T3 <= 1 \r\n1 P0T0 + 1 P0T1 + 1 P0T2 + 1 P0T3 >= 20 \r\n1 P0T0 + 1 P0T1 + 1 P0T2 + 1 P0T3 <= 40 \r\n1 P1T0 + 1 P1T1 + 1 P1T2 + 1 P1T3 >= 20 \r\n1 P1T0 + 1 P1T1 + 1 P1T2 + 1 P1T3 <= 40 \r\n1 P2T0 + 1 P2T1 + 1 P2T2 + 1 P2T3 >= 20 \r\n1 P2T0 + 1 P2T1 + 1 P2T2 + 1 P2T3 <= 40 \r\nBINARY\r\nP0T0\r\nP1T0\r\nP2T0\r\nP0T1\r\nP1T1\r\nP2T1\r\nP0T2\r\nP1T2\r\nP2T2\r\nP0T3\r\nP1T3\r\nP2T3\r\nEND\r\n";

            Assert.AreEqual(textoEsperado, texto);
        }

        [TestMethod]
        public void testaGeracaoArquivoCargaHorariasIntegral()
        {
            var dic0 = new Disciplina("DISC 0");
            var dic1 = new Disciplina("DISC 1");
            var dic2 = new Disciplina("DISC 2");
            var dic3 = new Disciplina("DISC 3");

            var turma0 = new Turma(dic0, new List<Horario>() { new Horario("Segunda", "07:45", "09:25") }, "68", Semestre.Semestre_1);
            var turma1 = new Turma(dic1, new List<Horario>() { new Horario("Terça", "07:45", "09:25") }, "68", Semestre.Semestre_1);
            var turma2 = new Turma(dic2, new List<Horario>() { new Horario("Quarta", "07:45", "09:25") }, "68", Semestre.Semestre_1);
            var turma3 = new Turma(dic3, new List<Horario>() { new Horario("Quinta", "07:45", "09:25") }, "68", Semestre.Semestre_1);

            var professor0 = new Professor(new List<Turma>() { turma0, turma1, turma2, turma3 }, new List<Turma>() { }, "Prof 0", new List<Turma>(), "8");
            var professor1 = new Professor(new List<Turma>() { turma1 }, new List<Turma>(), "Prof 1", new List<Turma>(), "8");
            var professor2 = new Professor(new List<Turma>() { }, new List<Turma>() {turma2}, "Prof 2", new List<Turma>(), "8");

            string texto = GeradorArquivo.getTexto(new List<Professor>() { professor0, professor1, professor2 },
                new List<Turma>() { turma0, turma1, turma2, turma3 });

            var textoEsperado =
                "MAXIMIZE obj:  3 P0T0 +  1 P1T0 +  1 P2T0 +  3 P0T1 +  3 P1T1 +  1 P2T1 +  3 P0T2 +  1 P1T2 +  2 P2T2 +  3 P0T3 +  1 P1T3 +  1 P2T3\r\nSUBJECT TO\r\nP0T0 + P1T0 + P2T0 = 1 \r\nP0T1 + P1T1 + P2T1 = 1 \r\nP0T2 + P1T2 + P2T2 = 1 \r\nP0T3 + P1T3 + P2T3 = 1 \r\n1 P0T0 + 1 P0T1 + 1 P0T2 + 1 P0T3 >= 20 \r\n1 P0T0 + 1 P0T1 + 1 P0T2 + 1 P0T3 <= 40 \r\n1 P1T0 + 1 P1T1 + 1 P1T2 + 1 P1T3 >= 20 \r\n1 P1T0 + 1 P1T1 + 1 P1T2 + 1 P1T3 <= 40 \r\n1 P2T0 + 1 P2T1 + 1 P2T2 + 1 P2T3 >= 20 \r\n1 P2T0 + 1 P2T1 + 1 P2T2 + 1 P2T3 <= 40 \r\nBINARY\r\nP0T0\r\nP1T0\r\nP2T0\r\nP0T1\r\nP1T1\r\nP2T1\r\nP0T2\r\nP1T2\r\nP2T2\r\nP0T3\r\nP1T3\r\nP2T3\r\nEND\r\n";

            Assert.AreEqual(textoEsperado, texto);
        }

        [TestMethod]
        public void testaGeracaoArquivoCargaHorariasNoturnas()
        {
            var dic0 = new Disciplina("DISC 0");
            var dic1 = new Disciplina("DISC 1");
            var dic2 = new Disciplina("DISC 2");
            var dic3 = new Disciplina("DISC 3");

            var turma0 = new Turma(dic0, new List<Horario>() { new Horario("Segunda", "19:30", "21:00") }, "68", Semestre.Semestre_1);
            var turma1 = new Turma(dic1, new List<Horario>() { new Horario("Terça", "19:30", "21:00") }, "68", Semestre.Semestre_1);
            var turma2 = new Turma(dic2, new List<Horario>() { new Horario("Quarta", "19:30", "21:00") }, "68", Semestre.Semestre_1);
            var turma3 = new Turma(dic3, new List<Horario>() { new Horario("Quinta", "19:30", "21:00") }, "68", Semestre.Semestre_1);

            var professor0 = new Professor(new List<Turma>() { turma0, turma1, turma2, turma3 }, new List<Turma>() { }, "Prof 0", new List<Turma>(), "8");
            var professor1 = new Professor(new List<Turma>() { turma1 }, new List<Turma>(), "Prof 1", new List<Turma>(), "8");
            var professor2 = new Professor(new List<Turma>() { }, new List<Turma>() { turma2 }, "Prof 2", new List<Turma>(), "8");

            string texto = GeradorArquivo.getTexto(new List<Professor>() { professor0, professor1, professor2 },
                new List<Turma>() { turma0, turma1, turma2, turma3 });

            var textoEsperado =
                "MAXIMIZE obj:  3 P0T0 +  1 P1T0 +  1 P2T0 +  3 P0T1 +  3 P1T1 +  1 P2T1 +  3 P0T2 +  1 P1T2 +  2 P2T2 +  3 P0T3 +  1 P1T3 +  1 P2T3\r\nSUBJECT TO\r\nP0T0 + P1T0 + P2T0 = 1 \r\nP0T1 + P1T1 + P2T1 = 1 \r\nP0T2 + P1T2 + P2T2 = 1 \r\nP0T3 + P1T3 + P2T3 = 1 \r\n1 P0T0 + 1 P0T1 + 1 P0T2 + 1 P0T3 >= 20 \r\n1 P0T0 + 1 P0T1 + 1 P0T2 + 1 P0T3 <= 40 \r\n1 P1T0 + 1 P1T1 + 1 P1T2 + 1 P1T3 >= 20 \r\n1 P1T0 + 1 P1T1 + 1 P1T2 + 1 P1T3 <= 40 \r\n1 P2T0 + 1 P2T1 + 1 P2T2 + 1 P2T3 >= 20 \r\n1 P2T0 + 1 P2T1 + 1 P2T2 + 1 P2T3 <= 40 \r\n1 P0T0 + 1 P0T1 + 1 P0T2 + 1 P0T3 >= 2 \r\n1 P1T0 + 1 P1T1 + 1 P1T2 + 1 P1T3 >= 2 \r\n1 P2T0 + 1 P2T1 + 1 P2T2 + 1 P2T3 >= 2 \r\nBINARY\r\nP0T0\r\nP1T0\r\nP2T0\r\nP0T1\r\nP1T1\r\nP2T1\r\nP0T2\r\nP1T2\r\nP2T2\r\nP0T3\r\nP1T3\r\nP2T3\r\nEND\r\n";

            Assert.AreEqual(textoEsperado, texto);
        }
        
        [TestMethod]
        public void testaGeracaoArquivoRestricaoIntraturno()
        {
            var dic0 = new Disciplina("DISC 0");
            var dic1 = new Disciplina("DISC 1");
            var dic2 = new Disciplina("DISC 2");
            var dic3 = new Disciplina("DISC 3");

            var turma0 = new Turma(dic0, new List<Horario>() { new Horario("Segunda", "21:20", "23:00") }, "68", Semestre.Semestre_1);
            var turma1 = new Turma(dic1, new List<Horario>() { new Horario("Terça", "07:45", "09:25") }, "68", Semestre.Semestre_1);
            var turma2 = new Turma(dic2, new List<Horario>() { new Horario("Quarta", "21:20", "23:00") }, "68", Semestre.Semestre_1);
            var turma3 = new Turma(dic3, new List<Horario>() { new Horario("Quinta", "07:45", "09:25") }, "68", Semestre.Semestre_1);

            var professor0 = new Professor(new List<Turma>() { turma0, turma1, turma2, turma3 }, new List<Turma>() { }, "Prof 0", new List<Turma>(), "8");
            var professor1 = new Professor(new List<Turma>() { turma1 }, new List<Turma>(), "Prof 1", new List<Turma>(), "8");
            var professor2 = new Professor(new List<Turma>() { }, new List<Turma>() { turma2 }, "Prof 2", new List<Turma>(), "8");

            string texto = GeradorArquivo.getTexto(new List<Professor>() { professor0, professor1, professor2 },
                new List<Turma>() { turma0, turma1, turma2, turma3 });

            var textoEsperado =
                "MAXIMIZE obj:  3 P0T0 +  1 P1T0 +  1 P2T0 +  3 P0T1 +  3 P1T1 +  1 P2T1 +  3 P0T2 +  1 P1T2 +  2 P2T2 +  3 P0T3 +  1 P1T3 +  1 P2T3\r\nSUBJECT TO\r\nP0T0 + P1T0 + P2T0 = 1 \r\nP0T1 + P1T1 + P2T1 = 1 \r\nP0T2 + P1T2 + P2T2 = 1 \r\nP0T3 + P1T3 + P2T3 = 1 \r\n1 P0T0 + 1 P0T1 + 1 P0T2 + 1 P0T3 >= 20 \r\n1 P0T0 + 1 P0T1 + 1 P0T2 + 1 P0T3 <= 40 \r\n1 P1T0 + 1 P1T1 + 1 P1T2 + 1 P1T3 >= 20 \r\n1 P1T0 + 1 P1T1 + 1 P1T2 + 1 P1T3 <= 40 \r\n1 P2T0 + 1 P2T1 + 1 P2T2 + 1 P2T3 >= 20 \r\n1 P2T0 + 1 P2T1 + 1 P2T2 + 1 P2T3 <= 40 \r\n1 P0T0 + 1 P0T2 >= 2 \r\n1 P1T0 + 1 P1T2 >= 2 \r\n1 P2T0 + 1 P2T2 >= 2 \r\nP0T1 + P0T0 <= 1 \r\nP1T1 + P1T0 <= 1 \r\nP2T1 + P2T0 <= 1 \r\nP0T3 + P0T2 <= 1 \r\nP1T3 + P1T2 <= 1 \r\nP2T3 + P2T2 <= 1 \r\nBINARY\r\nP0T0\r\nP1T0\r\nP2T0\r\nP0T1\r\nP1T1\r\nP2T1\r\nP0T2\r\nP1T2\r\nP2T2\r\nP0T3\r\nP1T3\r\nP2T3\r\nEND\r\n";

            Assert.AreEqual(textoEsperado, texto);
        }



        [TestMethod]
        public void testaGeracaoArquivoRestricaoTurmasConsolidadas()
        {
            var dic0 = new Disciplina("DISC 0");
            var dic1 = new Disciplina("DISC 1");
            var dic2 = new Disciplina("DISC 2");
         

            var turma0 = new Turma(dic0, new List<Horario>() { new Horario("Segunda", "21:00", "23:00") }, "68", Semestre.Semestre_1);
            var turma1 = new Turma(dic1, new List<Horario>() { new Horario("Terça", "07:45", "09:25") }, "68", Semestre.Semestre_1);
         

            var professor0 = new Professor(new List<Turma>() { turma0 }, new List<Turma>() { }, "Prof 0", new List<Turma>(), "8");
            var professor1 = new Professor(new List<Turma>() { turma1 }, new List<Turma>(), "Prof 1", new List<Turma>(), "8");

            var turma2 = new Turma(dic2, new List<Horario>() { new Horario("Terça", "07:45", "09:25") }, "68", Semestre.Semestre_1, professor0);

            string texto = GeradorArquivo.getTexto(new List<Professor>() { professor0, professor1 },
                new List<Turma>() { turma0, turma1, turma2 });

            var textoEsperado =
                "MAXIMIZE obj:  3 P0T0 +  1 P1T0 +  1 P0T1 +  3 P1T1 +  1 P0T2 +  1 P1T2\r\nSUBJECT TO\r\nP0T0 + P1T0 = 1 \r\nP0T1 + P1T1 = 1 \r\nP0T2 + P1T2 = 1 \r\nP0T1 + P0T2 <= 1 \r\nP1T1 + P1T2 <= 1 \r\n1 P0T0 + 1 P0T1 + 1 P0T2 >= 20 \r\n1 P0T0 + 1 P0T1 + 1 P0T2 <= 40 \r\n1 P1T0 + 1 P1T1 + 1 P1T2 >= 20 \r\n1 P1T0 + 1 P1T1 + 1 P1T2 <= 40 \r\nP0T2 = 1\r\nBINARY\r\nP0T0\r\nP1T0\r\nP0T1\r\nP1T1\r\nP0T2\r\nP1T2\r\nEND\r\n";

            Assert.AreEqual(textoEsperado, texto);
        }

        [TestMethod]
        public void testaGeracaoArquivoIndisponobilidade()
        {
            var dic0 = new Disciplina("DISC 0");
            var dic1 = new Disciplina("DISC 1");
            var dic2 = new Disciplina("DISC 2");

            var turma0 = new Turma(dic0, new List<Horario>() { new Horario("Segunda", "07:45", "09:25") }, "68", Semestre.Semestre_1);
            var turma1 = new Turma(dic1, new List<Horario>() { new Horario("Terça", "07:45", "09:25") }, "68", Semestre.Semestre_1);
            var turma2 = new Turma(dic2, new List<Horario>() { new Horario("Quarta", "07:45", "09:25") }, "68", Semestre.Semestre_1);

            var professor0 = new Professor(new List<Turma>() { turma0 }, new List<Turma>() { turma1 }, "Prof 0", new List<Turma>() { turma2}, "8");
            var professor1 = new Professor(new List<Turma>() { turma1 }, new List<Turma>() { turma2 }, "Prof 1", new List<Turma>(), "8");
            var professor2 = new Professor(new List<Turma>() { turma2 }, new List<Turma>() { turma1 }, "Prof 2", new List<Turma>(), "8");

            string texto = GeradorArquivo.getTexto(new List<Professor>() { professor0, professor1, professor2 },
                new List<Turma>() { turma0, turma1, turma2 });

            var textoEsperado =
                "MAXIMIZE obj:  3 P0T0 +  1 P1T0 +  1 P2T0 +  2 P0T1 +  3 P1T1 +  2 P2T1 +  1 P0T2 +  2 P1T2 +  3 P2T2\r\nSUBJECT TO\r\nP0T0 + P1T0 + P2T0 = 1 \r\nP0T1 + P1T1 + P2T1 = 1 \r\nP0T2 + P1T2 + P2T2 = 1 \r\n1 P0T0 + 1 P0T1 + 1 P0T2 >= 20 \r\n1 P0T0 + 1 P0T1 + 1 P0T2 <= 40 \r\n1 P1T0 + 1 P1T1 + 1 P1T2 >= 20 \r\n1 P1T0 + 1 P1T1 + 1 P1T2 <= 40 \r\n1 P2T0 + 1 P2T1 + 1 P2T2 >= 20 \r\n1 P2T0 + 1 P2T1 + 1 P2T2 <= 40 \r\nP0T2 = 0\r\nBINARY\r\nP0T0\r\nP1T0\r\nP2T0\r\nP0T1\r\nP1T1\r\nP2T1\r\nP0T2\r\nP1T2\r\nP2T2\r\nEND\r\n";

            Assert.AreEqual(textoEsperado.ToString(), texto);
        }


        [TestMethod]
        public void testaGeracaoArquivoCasoRealDIN()
        {
            var turmas = new List<Turma>();
            var professores = new List<Professor>();

            var compiladores = new Disciplina("Compiladores");
            var redes = new Disciplina("Redes de Computadores");
            var aspectosPsicologicos = new Disciplina("Aspectos Psicológicos e Sociais da Informática");
            var lp = new Disciplina("Linguagens de Programação");
            var tccCordenacao = new Disciplina("Trabalho de Graduação (Coordenação)");
            var eng3 = new Disciplina("Processo de Engenharia de Software III");
            var eng4 = new Disciplina("Processo de Engenharia de Software IV");
            var tcc = new Disciplina("Trabalho de Conclusão de Curso (Coordenação)");
            var automacao = new Disciplina("Automação");
            var progSistWeb = new Disciplina("Programação de sistemas WEB");
            var so2 = new Disciplina("Sistemas Operacionais II");
            var ia = new Disciplina("Inteligência Artificial");
            var mf1 = new Disciplina("Métodos Formais I");
            var teoriaDosGrafos = new Disciplina("Teoria dos Grafos e Análise de Algoritmos");
            var bd2 = new Disciplina("Banco de Dados II");
            var ambienteDesenv = new Disciplina("Ambientes de Desenvolvimento de Software");
            var mf2 = new Disciplina("Métodos Formais II");
            var sd = new Disciplina("Sistemas Distribuídos");
            var arq2 = new Disciplina("Arquitetura de Computadores II");
            var cg = new Disciplina("Computação Gráfica");
            var topicosPesquisa = new Disciplina("Tópicos em Pesquisa Operacional");
            var programacaoLinear = new Disciplina("Programação Linear");
            var simulacaoSistemas = new Disciplina("Simulação de Sistemas");
            var fundamentosEletronica = new Disciplina("Fundamentos de Eletrônica");
            var ti = new Disciplina("Fundamentos de Tecnologia da Informação");
            var pesquisa = new Disciplina("Fundamentos de Pesquisa");
            var cd1 = new Disciplina("Circuitos Digitais I");
            var algoritmos = new Disciplina("Fundamentos de Algoritmos");
            var cd2 = new Disciplina("Circuitos Digitais II");
            var lingFormaisAutomatos = new Disciplina("Linguagens Formais e Automatos");
            var estrutura = new Disciplina("Estruturas de Dados");
            var eng1 = new Disciplina("Proc. de Soft. e Eng. de Requisitos");
            var arq = new Disciplina("Arq. e Organiz. de Computadores");
            var paa = new Disciplina("Projeto e Análise de Algoritmos");
            var poo = new Disciplina("Par.de Prog.Imp.e Orient.a Objetos");
            var ihc = new Disciplina("Interação ser humano computador");
            var ass = new Disciplina("Análise de Sistemas de Software");
            var bd1 = new Disciplina("Banco de Dados I");
            var progInterfaceHardSoft = new Disciplina("Progr. Interfaceamento de Hard e Soft");
            var computabilidade = new Disciplina("Computabilidade");
            var ord = new Disciplina("Organização e Recuperação de Dados");
            var grafos = new Disciplina("Algortimos em Grafos");
            var pss = new Disciplina("Projeto de Sistemas de Software");
            var matematicaComputacional = new Disciplina("Matemática Computacional");
            var so = new Disciplina("Sistemas Operacionais");
            var logica = new Disciplina("Paradigma Prog. Lógica e Funcional");
            var modelagem = new Disciplina("Modelagem e Otimização Algorítmica");
            var iss = new Disciplina("Implementação de Sistemas de Software");
            var informaticaESociedade = new Disciplina("Informática e Sociedade");
            var concorrente = new Disciplina("Programação Concorrentes");
            var aprendizagemMaquina = new Disciplina("Aprendizagem de Máquina");
            var estagio = new Disciplina("Estágio Docência");
            var gerenciamentoProjeto = new Disciplina("Gerenciamento de Projetos de Software");
            var metodosModelagem = new Disciplina("Métodos de Modelagem de Software");
            var pesquisaOperacional = new Disciplina("Pesquisa Operacional");
            var seminario = new Disciplina("Seminários I");
            var seminarios2 = new Disciplina("Seminários II");
            var tc = new Disciplina("Teoria da Computação");
            var sistemasComputacao = new Disciplina("Tópicos em Sistemas de Computacão I");
            var sistemasComputacao2 = new Disciplina("Tópicos em Sistemas de Computacão II");
            var sistemasComputacao3 = new Disciplina("Tópicos em Sistemas de Computacão III");
            var sistemaInformacao = new Disciplina("Tópicos em Sistemas de Informação I");
            var sistemaInformacao2 = new Disciplina("Tópicos em Sistemas de Informação II");
            var trabIndividual = new Disciplina("Trabalho Individual");
            var organizacaoComputadores = new Disciplina("Organização de Computadores e Sistemas Operacionais");
            var eng2 = new Disciplina("Engenharia de Software II");
            var qualidade = new Disciplina("Qualidade de Software");
            var projetoSoftware = new Disciplina("Projeto de Software");
            var fundProgramacao = new Disciplina("Fundamentos de Programação");
            var tecAvancadasSI = new Disciplina("Tecnologias Avançadas para Sistemas de Informação");
            var progSistemas = new Disciplina("Programação de Sistemas");
            var algoritmosEProg = new Disciplina("Algoritmos e Prog Comp");
            var assembly = new Disciplina("Programação em Ling de Montagem");
            var paradigmaImpOO = new Disciplina("Par.de Prog.Imp.e Orient.a Objetos");
            var infoAgro = new Disciplina("Informática Aplicada a Agronomia");
            var fundComputacao = new Disciplina("Fundamentos da Computação (MEC)");
            var secretariado = new Disciplina("Introdução à Informática Secreatiado");
            var labProgramacao = new Disciplina("Lab. Fund de Programação");

            //turmas.Add(new Turma(redes, new List<Horario>() { new Horario(Dia.Quarta, "09:40", "10:30"), new Horario(Dia.Quarta, "10:30", "11:20") }, "68", Semestre.Anual, 1023));
            turmas.Add(new Turma(tccCordenacao, new List<Horario>() { new Horario(Dia.Terça, "13:30", "14:20"), new Horario(Dia.Terça, "14:20", "15:10") }, "68", Semestre.Anual, "1029b"));
            //new Turma(automacao, new List<Horario>() { new Horario(Dia.Quarta, "15:30", "16:20"), new Horario(Dia.Quarta, "16:20", "17:10"), new Horario(Dia.Quarta, "17:10", "18:00") }, "102", Semestre.Anual, 1034));
            //new Turma(mf1, new List<Horario>() { new Horario(Dia.Segunda, "13:30", "14:20"), new Horario(Dia.Segunda, "14:20", "15:10"), new Horario(Dia.Terça, "15:30", "16:20"), new Horario(Dia.Terça, "16:20", "17:10") }, "68", Semestre.Semestre_2, 1744));
            turmas.Add(new Turma(teoriaDosGrafos, new List<Horario>() { new Horario(Dia.Segunda, "13:30", "14:20"), new Horario(Dia.Segunda, "14:20", "15:10"), new Horario(Dia.Terça, "15:30", "16:20"), new Horario(Dia.Terça, "16:20", "17:10"), new Horario(Dia.Quinta, "13:30", "14:20"), new Horario(Dia.Quinta, "14:20", "13:10") }, "102", Semestre.Semestre_1, 1747));
            turmas.Add(new Turma(mf2, new List<Horario>() { new Horario(Dia.Terça, "15:30", "16:20"), new Horario(Dia.Terça, "16:20", "17:10"), new Horario(Dia.Quarta, "07:45", "08:35"), new Horario(Dia.Quarta, "08:35", "09:25")}, "68", Semestre.Semestre_2, 1754));
            //new Turma(topicosPesquisa, new List<Horario>() { new Horario(Dia.Segunda, "13:30", "14:20"), new Horario(Dia.Segunda, "14:20", "15:10"), new Horario(Dia.Quarta, "13:30", "14:20"), new Horario(Dia.Quarta, "14:20", "15:10"), new Horario(Dia.Quinta, "09:40", "10:30"), new Horario(Dia.Quinta, "10:30", "11:20") }, "102", Semestre.Semestre_2, 1760));
            //turmas.Add(new Turma(programacaoLinear, new List<Horario>() { new Horario(Dia.Quinta, "07:45", "08:35"), new Horario(Dia.Quinta, "08:35", "09:25"), new Horario(Dia.Sexta, "07:45", "08:35"), new Horario(Dia.Sexta, "08:35", "09:25")}, "68", Semestre.Semestre_1, 1761));
            turmas.Add(new Turma(simulacaoSistemas, new List<Horario>() { new Horario(Dia.Terça, "13:30", "14:20"), new Horario(Dia.Terça, "14:20", "15:10"), new Horario(Dia.Quinta, "07:45", "08:35"), new Horario(Dia.Quinta, "08:35", "09:25")}, "68", Semestre.Semestre_2, 1762));
            turmas.Add(new Turma(arq, new List<Horario>() { new Horario(Dia.Quarta, "09:40", "10:30"), new Horario(Dia.Quarta, "10:30", "11:20"), new Horario(Dia.Quinta, "15:30", "16:20"), new Horario(Dia.Quinta, "16:20", "17:10"), new Horario(Dia.Sexta, "07:45", "08:35"), new Horario(Dia.Sexta, "08:35", "09:25") }, "102", Semestre.Semestre_2, "6887b"));
            turmas.Add(new Turma(fundamentosEletronica, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20"), new Horario(Dia.Terça, "07:45", "08:35"), new Horario(Dia.Terça, "08:35", "09:25"), new Horario(Dia.Sexta, "09:40", "10:30"), new Horario(Dia.Sexta, "10:30", "11:20") }, "102", Semestre.Semestre_1, "6872b"));
            turmas.Add(new Turma(fundamentosEletronica, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20"), new Horario(Dia.Terça, "07:45", "08:35"), new Horario(Dia.Terça, "08:35", "09:25"), new Horario(Dia.Sexta, "09:40", "10:30"), new Horario(Dia.Sexta, "10:30", "11:20") }, "102", Semestre.Semestre_1, "6872c"));
            turmas.Add(new Turma(ass, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20"), new Horario(Dia.Quinta, "09:40", "10:30"), new Horario(Dia.Quinta, "10:30", "11:20") }, "68", Semestre.Semestre_2, "6891b"));
            turmas.Add(new Turma(programacaoLinear, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20"), new Horario(Dia.Segunda, "11:20", "12:10"), new Horario(Dia.Quarta, "09:40", "10:30"), new Horario(Dia.Quarta, "10:30", "11:20"), new Horario(Dia.Quarta, "11:20", "12:10") }, "102", Semestre.Semestre_2, "6889b"));
            
            turmas.Add(new Turma(mf2, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00") }, "68", Semestre.Semestre_1, 1684));
            //turmas.Add(new Turma(eng4, new List<Horario>() { new Horario(Dia.Terça, "19:30", "20:20"), new Horario(Dia.Terça, "20:20", "21:10"), new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10"), new Horario(Dia.Quinta, "21:20", "22:10"), new Horario(Dia.Quinta, "22:10", "23:00") }, "136", Semestre.Semestre_1, 1837));
            turmas.Add(new Turma(eng4, new List<Horario>() { new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10"), new Horario(Dia.Quinta, "21:20", "22:10"), new Horario(Dia.Quinta, "22:10", "23:00") }, "136", Semestre.Semestre_1, "1837b"));
            turmas.Add(new Turma(ass, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00") }, "68", Semestre.Semestre_2, "5181b"));
            
            turmas.Add(new Turma(organizacaoComputadores, new List<Horario>() { new Horario(Dia.Sabado, "13:30", "14:20"), new Horario(Dia.Sabado, "14:20", "15:10"), new Horario(Dia.Sabado, "15:30", "16:20") }, "102", Semestre.Anual, 1452));
            turmas.Add(new Turma(sd, new List<Horario>() { new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00") }, "68", Semestre.Anual, 1487));
            turmas.Add(new Turma(eng2, new List<Horario>() { new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Anual, 1470));
            
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Terça, "07:45", "08:35"), new Horario(Dia.Quarta, "07:45", "08:35"), new Horario(Dia.Quarta, "08:35", "09:25") }, "136", Semestre.Anual, 1590));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Terça, "08:35", "09:25"), new Horario(Dia.Quarta, "07:45", "08:35"), new Horario(Dia.Quarta, "08:35", "09:25") }, "136", Semestre.Anual, "1590b"));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10") }, "68", Semestre.Semestre_2, 1640));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10") }, "68", Semestre.Semestre_2, "1640b"));
            turmas.Add(new Turma(fundProgramacao, new List<Horario>() { new Horario(Dia.Quarta, "09:40", "10:30"), new Horario(Dia.Quarta, "10:30", "11:20"), new Horario(Dia.Quarta, "11:20", "12:10")}, "51", Semestre.Semestre_2, 2536));
            turmas.Add(new Turma(fundProgramacao, new List<Horario>() { new Horario(Dia.Quarta, "15:30", "16:20"), new Horario(Dia.Quarta, "16:20", "17:10"), new Horario(Dia.Quarta, "17:10", "18:00") }, "51", Semestre.Semestre_2, "2536b"));
            turmas.Add(new Turma(labProgramacao, new List<Horario>() { new Horario(Dia.Quarta, "13:30", "14:20") }, "17", Semestre.Semestre_2, 2541));
            turmas.Add(new Turma(labProgramacao, new List<Horario>() { new Horario(Dia.Quarta, "07:45", "08:35") }, "17", Semestre.Semestre_2, "2541b"));
            turmas.Add(new Turma(labProgramacao, new List<Horario>() { new Horario(Dia.Quinta, "13:30", "14:20") }, "17", Semestre.Semestre_2, "2541c"));
            turmas.Add(new Turma(labProgramacao, new List<Horario>() { new Horario(Dia.Quinta, "07:45", "08:35") }, "17", Semestre.Semestre_2, "2541d"));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Sexta, "09:40", "10:30"), new Horario(Dia.Sexta, "10:30", "11:20"), new Horario(Dia.Quarta, "15:30", "16:20") }, "68", Semestre.Anual, "208"));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Quarta, "16:20", "17:10"), new Horario(Dia.Sexta, "09:40", "10:30"), new Horario(Dia.Sexta, "10:30", "11:20") }, "68", Semestre.Anual, "208c"));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Quarta, "17:10", "18:00"), new Horario(Dia.Sexta, "09:40", "10:30"), new Horario(Dia.Sexta, "10:30", "11:20") }, "68", Semestre.Anual, "208d"));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Segunda, "15:30", "16:20"), new Horario(Dia.Segunda, "16:20", "17:10"), new Horario(Dia.Terça, "09:40", "10:30") }, "68", Semestre.Anual, "208d"));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Segunda, "15:30", "16:20"), new Horario(Dia.Segunda, "16:20", "17:10"), new Horario(Dia.Terça, "10:30", "11:20") }, "68", Semestre.Anual, "208e"));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Segunda, "15:30", "16:20"), new Horario(Dia.Segunda, "16:20", "17:10"), new Horario(Dia.Terça, "11:20", "12:10") }, "68", Semestre.Anual, "208f"));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Quarta, "09:40", "10:30"), new Horario(Dia.Quarta, "10:30", "11:20"), new Horario(Dia.Quarta, "11:20", "12:10") }, "68", Semestre.Anual, "1512b"));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Quarta, "08:35", "09:25"), new Horario(Dia.Quarta, "09:40", "10:30"), new Horario(Dia.Quarta, "10:30", "11:20") }, "68", Semestre.Anual, "1512c"));


            turmas.Add(new Turma(matematicaComputacional, new List<Horario>() { new Horario(Dia.Segunda, "15:30", "16:20"), new Horario(Dia.Segunda, "16:20", "17:10"), new Horario(Dia.Quinta, "13:30", "14:20"), new Horario(Dia.Quinta, "14:20", "15:10") }, "68", Semestre.Semestre_2, "6900b"));
            turmas.Add(new Turma(progInterfaceHardSoft, new List<Horario>() { new Horario(Dia.Terça, "13:30", "14:20"), new Horario(Dia.Terça, "14:20", "15:10"), new Horario(Dia.Sexta, "07:45", "08:35"), new Horario(Dia.Sexta, "08:35", "09:25") }, "68", Semestre.Semestre_1,  "6894b"));
            turmas.Add(new Turma(estrutura, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Sabado, "13:30", "14:20"), new Horario(Dia.Sabado, "14:20", "15:10") }, "68", Semestre.Semestre_2, 7256));
            turmas.Add(new Turma(estrutura, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Sabado, "13:30", "14:20"), new Horario(Dia.Sabado, "14:20", "15:10") }, "68", Semestre.Semestre_2, "7256b"));
            turmas.Add(new Turma(estrutura, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Sabado, "13:30", "14:20"), new Horario(Dia.Sabado, "14:20", "15:10") }, "68", Semestre.Semestre_2, "7256c"));
            turmas.Add(new Turma(estrutura, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Sabado, "13:30", "14:20"), new Horario(Dia.Sabado, "14:20", "15:10") }, "68", Semestre.Semestre_2, "7256d"));
            turmas.Add(new Turma(bd1, new List<Horario>() { new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10"), new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00") }, "68", Semestre.Semestre_2, 7331));
            turmas.Add(new Turma(fundProgramacao, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00"), new Horario(Dia.Sabado, "13:30", "14:20"), new Horario(Dia.Sabado, "14:20", "15:10") }, "102", Semestre.Semestre_1, "7239b"));
            turmas.Add(new Turma(fundProgramacao, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00"), new Horario(Dia.Sabado, "13:30", "14:20"), new Horario(Dia.Sabado, "14:20", "15:10") }, "102", Semestre.Semestre_1, "7239c"));
            turmas.Add(new Turma(fundProgramacao, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00"), new Horario(Dia.Sabado, "13:30", "14:20"), new Horario(Dia.Sabado, "14:20", "15:10") }, "102", Semestre.Semestre_1, "7239d"));
            turmas.Add(new Turma(progSistemas, new List<Horario>() { new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00"), new Horario(Dia.Sabado, "15:30", "16:20"), new Horario(Dia.Sabado, "16:20", "17:10") }, "102", Semestre.Semestre_1, 7328));
            turmas.Add(new Turma(so, new List<Horario>() { new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10"), new Horario(Dia.Sabado, "13:30", "14:20"), new Horario(Dia.Sabado, "14:20", "15:10") }, "68", Semestre.Semestre_1, 7329));
            turmas.Add(new Turma(arq, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00") }, "68", Semestre.Semestre_1, "5178b"));
            turmas.Add(new Turma(ihc, new List<Horario>() { new Horario(Dia.Terça, "19:30", "20:20"), new Horario(Dia.Terça, "20:20", "21:10"), new Horario(Dia.Quinta, "21:20", "22:10"), new Horario(Dia.Quinta, "22:10", "23:00") }, "68", Semestre.Semestre_2, "5183b"));
            turmas.Add(new Turma(logica, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00") }, "68", Semestre.Semestre_1, "5200b"));
            turmas.Add(new Turma(assembly, new List<Horario>() { new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00") }, "68", Semestre.Semestre_2, "5193b"));
            turmas.Add(new Turma(concorrente, new List<Horario>() { new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00"), new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Sexta, "20:20", "21:10") }, "68", Semestre.Semestre_2, "5205b"));
            turmas.Add(new Turma(paa, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Semestre_2, "5184b"));
            turmas.Add(new Turma(paa, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Semestre_2, "5184c"));
            turmas.Add(new Turma(algoritmosEProg, new List<Horario>() { new Horario(Dia.Segunda, "13:30", "14:20"), new Horario(Dia.Segunda, "14:20", "15:10"), new Horario(Dia.Terça, "15:30", "16:20") }, "136", Semestre.Anual, 5259));
            turmas.Add(new Turma(algoritmosEProg, new List<Horario>() { new Horario(Dia.Segunda, "13:30", "14:20"), new Horario(Dia.Segunda, "14:20", "15:10"), new Horario(Dia.Terça, "16:20", "17:10") }, "136", Semestre.Anual, "5259b"));
            turmas.Add(new Turma(algoritmosEProg, new List<Horario>() { new Horario(Dia.Quarta, "13:30", "14:20"), new Horario(Dia.Quarta, "14:20", "15:10"), new Horario(Dia.Quarta, "15:30", "16:20") }, "136", Semestre.Anual, "5259c"));
            turmas.Add(new Turma(algoritmosEProg, new List<Horario>() { new Horario(Dia.Quarta, "13:30", "14:20"), new Horario(Dia.Quarta, "14:20", "15:10"), new Horario(Dia.Quarta, "16:20", "17:10") }, "136", Semestre.Anual, "5259d"));
            turmas.Add(new Turma(algoritmosEProg, new List<Horario>() { new Horario(Dia.Quinta, "15:30", "16:20"), new Horario(Dia.Quinta, "16:20", "17:10"), new Horario(Dia.Sexta, "15:30", "16:20") }, "136", Semestre.Anual, "5259e"));
            turmas.Add(new Turma(algoritmosEProg, new List<Horario>() { new Horario(Dia.Quinta, "15:30", "16:20"), new Horario(Dia.Quinta, "16:20", "17:10"), new Horario(Dia.Sexta, "16:20", "17:10") }, "136", Semestre.Anual, "5259f"));
            turmas.Add(new Turma(algoritmosEProg, new List<Horario>() { new Horario(Dia.Segunda, "15:30", "16:20"), new Horario(Dia.Segunda, "16:20", "17:10"), new Horario(Dia.Quarta, "15:30", "16:20"), new Horario(Dia.Quarta, "16:20", "17:10"), new Horario(Dia.Quinta, "15:30", "16:20"), new Horario(Dia.Quinta, "16:20", "17:10") }, "102", Semestre.Semestre_1, 4549));
            turmas.Add(new Turma(algoritmosEProg, new List<Horario>() { new Horario(Dia.Segunda, "15:30", "16:20"), new Horario(Dia.Segunda, "16:20", "17:10"), new Horario(Dia.Quarta, "13:30", "14:20"), new Horario(Dia.Quarta, "14:20", "15:10"), new Horario(Dia.Quarta, "15:30", "16:20"), new Horario(Dia.Quarta, "16:20", "17:10") }, "102", Semestre.Semestre_1, "4549b"));
            turmas.Add(new Turma(bd1, new List<Horario>() { new Horario(Dia.Segunda, "15:30", "16:20"), new Horario(Dia.Segunda, "16:20", "17:10"), new Horario(Dia.Quarta, "13:30", "14:20"), new Horario(Dia.Quarta, "14:20", "15:10"), new Horario(Dia.Quarta, "15:30", "16:20"), new Horario(Dia.Quarta, "16:20", "17:10") }, "68", Semestre.Semestre_2, 4573));
            turmas.Add(new Turma(estrutura, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00"), new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Semestre_2, 4553));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00"), new Horario(Dia.Sabado, "13:30", "14:20"), new Horario(Dia.Sabado, "14:20", "15:10") }, "102", Semestre.Anual, 4309));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00"), new Horario(Dia.Sabado, "13:30", "14:20"), new Horario(Dia.Sabado, "14:20", "15:10") }, "102", Semestre.Anual, "4309b"));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00"), new Horario(Dia.Sabado, "13:30", "14:20"), new Horario(Dia.Sabado, "14:20", "15:10") }, "102", Semestre.Anual, "4309c"));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Segunda, "15:30", "16:20"), new Horario(Dia.Segunda, "16:20", "17:10"), new Horario(Dia.Segunda, "17:10", "18:00") }, "51", Semestre.Semestre_2, "2536c"));
            turmas.Add(new Turma(fundProgramacao, new List<Horario>() { new Horario(Dia.Sexta, "09:40", "10:30"), new Horario(Dia.Sexta, "10:30", "11:20"), new Horario(Dia.Sexta, "11:20", "12:10") }, "51", Semestre.Semestre_1, 6633));
            turmas.Add(new Turma(fundProgramacao, new List<Horario>() { new Horario(Dia.Quinta, "15:30", "16:20"), new Horario(Dia.Quinta, "16:20", "17:10"), new Horario(Dia.Quinta, "17:10", "18:00") }, "51", Semestre.Semestre_1, "6633b"));
            turmas.Add(new Turma(infoAgro, new List<Horario>() { new Horario(Dia.Terça, "13:30", "14:20"), new Horario(Dia.Terça, "14:20", "15:10"), new Horario(Dia.Quinta, "13:30", "14:20"), new Horario(Dia.Quinta, "14:20", "15:10") }, "68", Semestre.Semestre_2, 4460));
            turmas.Add(new Turma(infoAgro, new List<Horario>() { new Horario(Dia.Terça, "13:30", "14:20"), new Horario(Dia.Terça, "14:20", "15:10"), new Horario(Dia.Quinta, "13:30", "14:20"), new Horario(Dia.Quinta, "14:20", "15:10") }, "68", Semestre.Semestre_2, "4460b"));
            turmas.Add(new Turma(infoAgro, new List<Horario>() { new Horario(Dia.Terça, "13:30", "14:20"), new Horario(Dia.Terça, "14:20", "15:10"), new Horario(Dia.Quinta, "13:30", "14:20"), new Horario(Dia.Quinta, "14:20", "15:10") }, "68", Semestre.Semestre_2, "4460c"));
            turmas.Add(new Turma(infoAgro, new List<Horario>() { new Horario(Dia.Terça, "13:30", "14:20"), new Horario(Dia.Terça, "14:20", "15:10"), new Horario(Dia.Quinta, "13:30", "14:20"), new Horario(Dia.Quinta, "14:20", "15:10") }, "68", Semestre.Semestre_2, "4460d"));
            turmas.Add(new Turma(secretariado, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00") }, "68", Semestre.Semestre_2, 3411));
            turmas.Add(new Turma(secretariado, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00") }, "68", Semestre.Semestre_2, "3411b"));
            turmas.Add(new Turma(cd2, new List<Horario>() { new Horario(Dia.Segunda, "07:45", "08:35"), new Horario(Dia.Segunda, "08:35", "09:25"), new Horario(Dia.Quarta, "07:45", "08:35"), new Horario(Dia.Quarta, "08:35", "09:25") }, "68", Semestre.Semestre_1, "6882c"));
            turmas.Add(new Turma(cd2, new List<Horario>() { new Horario(Dia.Segunda, "07:45", "08:35"), new Horario(Dia.Segunda, "08:35", "09:25"), new Horario(Dia.Quarta, "07:45", "08:35"), new Horario(Dia.Quarta, "08:35", "09:25") }, "68", Semestre.Semestre_1, "6882d"));
            turmas.Add(new Turma(pss, new List<Horario>() { new Horario(Dia.Terça, "07:45", "08:35"), new Horario(Dia.Terça, "08:35", "09:25"), new Horario(Dia.Quinta, "13:30", "14:20"), new Horario(Dia.Quinta, "14:20", "15:10") }, "68", Semestre.Semestre_1, 6885));
            turmas.Add(new Turma(pss, new List<Horario>() { new Horario(Dia.Terça, "07:45", "08:35"), new Horario(Dia.Terça, "08:35", "09:25"), new Horario(Dia.Quinta, "13:30", "14:20"), new Horario(Dia.Quinta, "14:20", "15:10") }, "68", Semestre.Semestre_1, "6885b"));
            turmas.Add(new Turma(cd1, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Semestre_2, "5174d"));


            #region Professores

            var constantino = new Professor(new List<Turma>(getTurmasDadoDisciplina(turmas, new List<Disciplina>() { modelagem, metodosModelagem })), new List<Turma>(getTurmasDadoDisciplina(turmas, new List<Disciplina>() { })), "Ademir Aparecido Constantino", new List<Turma>(), "8");
            var carniel = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Ademir Carniel", new List<Turma>(), "8");
            var anderson = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Anderson Faustino da Silva", new List<Turma>(), "8");
            var airton = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Airton Marco Polidório", new List<Turma>(), "8");
            var sica = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Carlos Benedito Sica de Toledo", new List<Turma>(), "8");
            var clelia = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Clélia Franco", new List<Turma>(), "8");
            var dante = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Dante Alves Medeiros Filho", new List<Turma>(), "8");
            var donizete = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Donizete Carlos Bruzarosco", new List<Turma>(), "8");
            var elisa = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Elisa Hatsue Moriya Huzita", new List<Turma>(), "8");
            var elvio = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Elvio João Leonardo", new List<Turma>(), "8");
            var flavio = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Flávio Arnaldo Braga da Silva", new List<Turma>(), "8");
            var itana = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Itana Maria de Souza Gimenes", new List<Turma>(), "8");
            var angelo = new Professor(new List<Turma>() { }, new List<Turma>() { }, "João Angelo Martini", new List<Turma>(), "8");
            var ze = new Professor(new List<Turma>() { }, new List<Turma>() { }, "José Roberto Vasconcelos", new List<Turma>(), "8");
            var josiane = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Josiane Melchiori Pinheiro", new List<Turma>(), "8");
            var jucelia = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Jucélia Geni Pereira Fregoneis", new List<Turma>(), "8");
            var linnyer = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Linnyer Beatrys Ruiz", new List<Turma>(), "8");
            var luciana = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Luciana Andréia Fondazzi Martimiano", new List<Turma>(), "8");
            var wigarashi = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Wagner Igarashi", new List<Turma>(), "8");
            var malbarbo = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Marco Aurélio Lopes Barbosa", new List<Turma>(), "8");
            var madalena = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Maria Madalena Dias", new List<Turma>(), "8");
            var yandre = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Yandre Maldonado e Gomes da Costa", new List<Turma>(), "8");
            var valeria = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Valéria Delisandra Feltrim", new List<Turma>(), "8");
            var thelma = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Thelma Elita Colanzi Lopes", new List<Turma>(), "8");
            var raqueline = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Raqueline Ritter de Moura", new List<Turma>(), "8");
            var ronaldo = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Ronaldo Augusto de Lara Gonçalves", new List<Turma>(), "8");
            var sergio = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Sérgio Roberto P. da Silva", new List<Turma>(), "8");
            var tania = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Tânia Fátima Calvi Tait", new List<Turma>(), "8");
            var wesley = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Wesley Romão", new List<Turma>(), "8");
            var andre = new Professor(new List<Turma>() { }, new List<Turma>() { }, "André Barbosa Verona", new List<Turma>(), "12");
            var franklin = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Franklin César Flores", new List<Turma>(), "8");
            var nardenio = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Nardênio Almeida Martins", new List<Turma>(), "8");
            var uber = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Flávio Uber", new List<Turma>(), "8");
            var heloise = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Heloise  Manica", new List<Turma>(), "16");
            var munif = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Munif Gebara Júnior", new List<Turma>(), "16");
            var balancieri = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Renato Balancieri", new List<Turma>(), "8");
            var juliana = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Juliana Keiko Yamaguchi", new List<Turma>(), "12");
            var rodrigo = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Rodrigo Lankaites Pinheiro", new List<Turma>(), "16");
            var edson = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Edson Alves de Oliveira Júniuor", new List<Turma>(), "8");
            var carlosFransley = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Carlos Fransley", new List<Turma>(), "8");

            professores.Add(constantino);
            professores.Add(carniel);
            professores.Add(anderson);
            professores.Add(airton);
            professores.Add(sica);
            professores.Add(clelia);
            professores.Add(dante);
            professores.Add(donizete);
            professores.Add(elisa);
            professores.Add(elvio);
            professores.Add(flavio);
            professores.Add(itana);
            professores.Add(angelo);
            professores.Add(ze);
            professores.Add(josiane);
            professores.Add(jucelia);
            professores.Add(linnyer);
            professores.Add(luciana);
            professores.Add(wigarashi);
            professores.Add(malbarbo);
            professores.Add(madalena);
            professores.Add(yandre);
            professores.Add(valeria);
            professores.Add(thelma);
            professores.Add(raqueline);
            professores.Add(ronaldo);
            professores.Add(sergio);
            professores.Add(wesley);
            professores.Add(franklin);
            professores.Add(nardenio);
            professores.Add(uber);
            professores.Add(heloise);
            professores.Add(munif);
            professores.Add(balancieri);
            professores.Add(juliana);
            professores.Add(rodrigo);
            professores.Add(edson);
            professores.Add(carlosFransley);
            #endregion

            #region Turmas Consolidadas
            turmas.Add(new Turma(modelagem, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20"), new Horario(Dia.Terça, "09:40", "10:30"), new Horario(Dia.Terça, "10:30", "11:20"), new Horario(Dia.Sexta, "07:45", "08:35"), new Horario(Dia.Sexta, "08:45", "09:25") }, "102", Semestre.Semestre_2, constantino, 6903));
            turmas.Add(new Turma(topicosPesquisa, new List<Horario>() { new Horario(Dia.Segunda, "13:30", "14:20"), new Horario(Dia.Segunda, "14:20", "15:10"), new Horario(Dia.Quarta, "13:30", "14:20"), new Horario(Dia.Quarta, "14:20", "15:10"), new Horario(Dia.Quinta, "09:40", "10:30"), new Horario(Dia.Quinta, "10:30", "11:20") }, "102", Semestre.Semestre_2, constantino, 1760));
            turmas.Add(new Turma(modelagem, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00") }, "102", Semestre.Semestre_2, constantino, 5199));

            turmas.Add(new Turma(matematicaComputacional, new List<Horario>() { new Horario(Dia.Segunda, "15:30", "16:20"), new Horario(Dia.Segunda, "16:20", "17:10"), new Horario(Dia.Quinta, "13:30", "14:20"), new Horario(Dia.Quinta, "14:20", "15:10") }, "68", Semestre.Semestre_2, airton, 6900));
            turmas.Add(new Turma(mf1, new List<Horario>() { new Horario(Dia.Terça, "15:30", "16:20"), new Horario(Dia.Terça, "16:20", "17:10"), new Horario(Dia.Quinta, "15:30", "16:20"), new Horario(Dia.Quinta, "16:20", "17:10") }, "68", Semestre.Semestre_2, airton, 1744));
            turmas.Add(new Turma(algoritmos, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00"), new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Sexta, "20:20", "21:10") }, "102", Semestre.Semestre_2, airton, 5175));
            turmas.Add(new Turma(mf1, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Sexta, "20:20", "21:10") }, "68", Semestre.Semestre_1, airton, 5237));

            turmas.Add(new Turma(arq, new List<Horario>() { new Horario(Dia.Segunda, "07:45", "08:35"), new Horario(Dia.Segunda, "08:35", "09:25"), new Horario(Dia.Quinta, "15:30", "16:20"), new Horario(Dia.Quinta, "16:20", "17:10"), new Horario(Dia.Sexta, "07:45", "08:35"), new Horario(Dia.Sexta, "08:35", "09:25") }, "102", Semestre.Semestre_2, anderson, 6887));
            turmas.Add(new Turma(arq2, new List<Horario>() { new Horario(Dia.Terça, "07:45", "08:35"), new Horario(Dia.Terça, "08:35", "09:25"), new Horario(Dia.Quinta, "07:45", "08:35"), new Horario(Dia.Quinta, "08:35", "09:25") }, "68", Semestre.Semestre_1, anderson, 6895));
            turmas.Add(new Turma(compiladores, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Semestre_2, anderson, 5204));
            turmas.Add(new Turma(concorrente, new List<Horario>() { new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00"), new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Sexta, "20:20", "21:10") }, "68", Semestre.Semestre_2, anderson, 5205));

            turmas.Add(new Turma(automacao, new List<Horario>() { new Horario(Dia.Quarta, "09:40", "10:30"), new Horario(Dia.Quarta, "10:30", "11:20"), new Horario(Dia.Quarta, "11:20", "12:10") }, "102", Semestre.Anual, andre, 1034));
            turmas.Add(new Turma(cd2, new List<Horario>() { new Horario(Dia.Segunda, "07:45", "08:35"), new Horario(Dia.Segunda, "08:35", "09:25"), new Horario(Dia.Quarta, "07:45", "08:35"), new Horario(Dia.Quarta, "08:35", "09:25") }, "68", Semestre.Semestre_1, andre, 6882));
            turmas.Add(new Turma(fundamentosEletronica, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20"), new Horario(Dia.Terça, "07:45", "08:35"), new Horario(Dia.Terça, "08:35", "09:25"), new Horario(Dia.Sexta, "09:40", "10:30"), new Horario(Dia.Sexta, "10:30", "11:20") }, "102", Semestre.Semestre_1, andre, 6872));
            turmas.Add(new Turma(arq, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Sabado, "15:30", "16:20"), new Horario(Dia.Sabado, "16:20", "17:10")}, "68", Semestre.Semestre_2, andre, 7330));
            turmas.Add(new Turma(arq, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00") }, "68", Semestre.Semestre_1, andre, 5178));

            turmas.Add(new Turma(fundamentosEletronica, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20"), new Horario(Dia.Terça, "07:45", "08:35"), new Horario(Dia.Terça, "08:35", "09:25"), new Horario(Dia.Sexta, "09:40", "10:30"), new Horario(Dia.Sexta, "10:30", "11:20") }, "102", Semestre.Semestre_1, sica, "6872d"));
            turmas.Add(new Turma(simulacaoSistemas, new List<Horario>() { new Horario(Dia.Terça, "13:30", "14:20"), new Horario(Dia.Terça, "14:20", "15:10"), new Horario(Dia.Quinta, "07:45", "08:35"), new Horario(Dia.Quinta, "08:35", "09:25") }, "68", Semestre.Semestre_2, sica, 1762));
            turmas.Add(new Turma(sd, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Sexta, "20:20", "21:10") }, "68", Semestre.Semestre_1, sica, 5197));

            turmas.Add(new Turma(automacao, new List<Horario>() { new Horario(Dia.Quarta, "15:30", "16:20"), new Horario(Dia.Quarta, "16:20" , "17:10"), new Horario(Dia.Quarta, "17:10", "18:00") }, "102", Semestre.Anual, carlosFransley, 1034));
            turmas.Add(new Turma(ia, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Sexta, "20:20", "21:10") }, "68", Semestre.Semestre_2, carlosFransley, "5236b"));

            turmas.Add(new Turma(fundProgramacao, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00"), new Horario(Dia.Sabado, "13:30", "14:20"), new Horario(Dia.Sabado, "14:20", "15:10") }, "102", Semestre.Semestre_1, clelia, 7239));

            turmas.Add(new Turma(cg, new List<Horario>() { new Horario(Dia.Quinta, "09:40", "10:30"), new Horario(Dia.Quinta, "10:30", "11:20"), new Horario(Dia.Quinta, "11:20", "12:10"), new Horario(Dia.Sexta, "09:40", "10:30"), new Horario(Dia.Sexta, "10:30", "11:20"), new Horario(Dia.Sexta, "11:20", "12:10") }, "102", Semestre.Semestre_1, dante, 1759));
            turmas.Add(new Turma(cg, new List<Horario>() { new Horario(Dia.Terça, "19:30", "20:20"), new Horario(Dia.Terça, "20:20", "21:10"), new Horario(Dia.Quinta, "21:20", "22:10"), new Horario(Dia.Quinta, "22:10", "23:00") }, "68", Semestre.Semestre_2, dante, 5202));
            turmas.Add(new Turma(pesquisa, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Semestre_1, dante, 5171));

            turmas.Add(new Turma(iss, new List<Horario>() { new Horario(Dia.Segunda, "07:45", "08:35"), new Horario(Dia.Segunda, "08:35", "09:25"), new Horario(Dia.Quinta, "09:40", "10:30"), new Horario(Dia.Quinta, "10:30", "11:20") }, "68", Semestre.Semestre_2, donizete, 6904));
            turmas.Add(new Turma(pss, new List<Horario>() { new Horario(Dia.Segunda, "07:45", "08:35"), new Horario(Dia.Segunda, "08:35", "09:25"), new Horario(Dia.Quinta, "09:40", "10:30"), new Horario(Dia.Quinta, "10:30", "11:20") }, "68", Semestre.Semestre_1, donizete, 6899));
            turmas.Add(new Turma(iss, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10") }, "68", Semestre.Semestre_2, donizete, 5191));
            turmas.Add(new Turma(iss, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00") }, "68", Semestre.Semestre_1, donizete, 5186));
            turmas.Add(new Turma(tcc, new List<Horario>() { new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00") }, "68", Semestre.Anual, donizete, 3513));

            turmas.Add(new Turma(ambienteDesenv, new List<Horario>() { new Horario(Dia.Terça, "13:30", "14:20"), new Horario(Dia.Terça, "14:20", "15:10"), new Horario(Dia.Quinta, "15:30", "16:20"), new Horario(Dia.Quinta, "16:20", "17:10") }, "68", Semestre.Semestre_2, edson, 1753));
            turmas.Add(new Turma(progSistWeb, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Sexta, "20:20", "21:10") }, "68", Semestre.Semestre_1, edson, 5188));
            turmas.Add(new Turma(poo, new List<Horario>() { new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "22:20", "21:10"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00") }, "68", Semestre.Semestre_1, edson, 5190));
            turmas.Add(new Turma(ass, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20"), new Horario(Dia.Quinta, "09:40", "10:30"), new Horario(Dia.Quinta, "10:30", "11:20") }, "68", Semestre.Semestre_2, edson, 6891));

            turmas.Add(new Turma(ti, new List<Horario>() { new Horario(Dia.Segunda, "13:30", "14:20"), new Horario(Dia.Segunda, "14:20", "15:10"), new Horario(Dia.Quinta, "09:40", "10:30"), new Horario(Dia.Quinta, "10:30", "11:20") }, "68", Semestre.Semestre_1, elisa, 6874));
            turmas.Add(new Turma(projetoSoftware, new List<Horario>() { new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Sexta, "20:20", "21:10") }, "68", Semestre.Anual, elisa, 1498));
            turmas.Add(new Turma(pss, new List<Horario>() { new Horario(Dia.Terça, "19:30", "20:20"), new Horario(Dia.Terça, "20:20", "21:10"), new Horario(Dia.Quinta, "21:20", "22:10"), new Horario(Dia.Quinta, "22:10", "23:00") }, "68", Semestre.Semestre_1, elisa, 5176));

            turmas.Add(new Turma(cd1, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20"), new Horario(Dia.Terça, "07:45", "08:35"), new Horario(Dia.Terça, "08:35", "09:25"), new Horario(Dia.Sexta, "09:40", "10:30"), new Horario(Dia.Sexta, "10:30", "11:20") }, "102", Semestre.Semestre_2, elvio, 6878));
            turmas.Add(new Turma(redes, new List<Horario>() { new Horario(Dia.Quarta, "09:40", "10:30"), new Horario(Dia.Quarta, "10:30", "11:20") }, "68", Semestre.Anual, elvio, 1023));
            turmas.Add(new Turma(cd1, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Semestre_2, elvio, 5174));
            turmas.Add(new Turma(redes, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10") }, "68", Semestre.Semestre_2, elvio, 5203));

            turmas.Add(new Turma(so, new List<Horario>() { new Horario(Dia.Segunda, "13:30", "14:20"), new Horario(Dia.Segunda, "14:20", "15:10"), new Horario(Dia.Quarta, "07:45", "08:35"), new Horario(Dia.Quarta, "08:35", "09:25"), new Horario(Dia.Sexta, "09:40", "10:30"), new Horario(Dia.Sexta, "10:30", "11:20") }, "102", Semestre.Semestre_2, flavio, 6901));
            turmas.Add(new Turma(so, new List<Horario>() { new Horario(Dia.Terça, "19:30", "20:20"), new Horario(Dia.Terça, "20:20", "21:10"), new Horario(Dia.Quinta, "21:20", "22:10"), new Horario(Dia.Quinta, "22:10", "23:00") }, "68", Semestre.Semestre_1, flavio, 5196));


            turmas.Add(new Turma(arq2, new List<Horario>() { new Horario(Dia.Terça, "13:30", "14:20"), new Horario(Dia.Terça, "14:20", "15:10"), new Horario(Dia.Quinta, "09:40", "10:30"), new Horario(Dia.Quinta, "10:30", "11:20") }, "68", Semestre.Semestre_1, uber, 1757));
            turmas.Add(new Turma(mf2, new List<Horario>() { new Horario(Dia.Segunda, "13:30", "14:20"), new Horario(Dia.Segunda, "14:20", "15:10"), new Horario(Dia.Quarta, "15:30", "16:20"), new Horario(Dia.Quarta, "16:20", "17:10") }, "68", Semestre.Semestre_2, uber, 1754));
            turmas.Add(new Turma(algoritmos, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00"), new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Sexta, "20:20", "21:10") }, "102", Semestre.Semestre_2, uber, "5175b"));
            turmas.Add(new Turma(mf2, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00") }, "68", Semestre.Semestre_1, uber, 1754));

            turmas.Add(new Turma(estrutura, new List<Horario>() { new Horario(Dia.Segunda, "13:30", "14:20"), new Horario(Dia.Segunda, "14:20", "15:10"), new Horario(Dia.Terça, "09:40", "10:30"), new Horario(Dia.Terça, "10:30", "11:20"), new Horario(Dia.Sexta, "07:45", "08:35"), new Horario(Dia.Sexta, "08:35", "09:25") }, "102", Semestre.Semestre_1, franklin, 6884));
            turmas.Add(new Turma(algoritmos, new List<Horario>() { new Horario(Dia.Segunda, "07:45", "08:35"), new Horario(Dia.Segunda, "08:35", "09:25"), new Horario(Dia.Terça, "15:30", "16:20"), new Horario(Dia.Terça, "16:20", "17:10"), new Horario(Dia.Quinta, "07:45", "08:35"), new Horario(Dia.Quinta, "08:35", "09:25") }, "102", Semestre.Semestre_2, franklin, 6879));
            turmas.Add(new Turma(estrutura, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00") }, "102", Semestre.Semestre_1, franklin, 5180));

            turmas.Add(new Turma(pesquisa, new List<Horario>() { new Horario(Dia.Terça, "15:30", "16:20"), new Horario(Dia.Terça, "16:20", "17:10") }, "34", Semestre.Semestre_1, heloise, 6875));
            turmas.Add(new Turma(pesquisa, new List<Horario>() { new Horario(Dia.Quinta, "15:30", "16:20"), new Horario(Dia.Quinta, "16:20", "17:10") }, "34", Semestre.Semestre_1, heloise, "6875b"));
            turmas.Add(new Turma(bd1, new List<Horario>() { new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00") }, "68", Semestre.Semestre_2, heloise, 5182));
            turmas.Add(new Turma(bd2, new List<Horario>() { new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00"), new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Sexta, "20:20", "21:10"), }, "68", Semestre.Semestre_2, heloise, 5192));
            turmas.Add(new Turma(pesquisa, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "22:20", "21:10") }, "68", Semestre.Semestre_1,heloise, "5171b"));

            turmas.Add(new Turma(pesquisa, new List<Horario>() { new Horario(Dia.Terça, "07:45", "08:35"), new Horario(Dia.Terça, "08:35", "09:25") }, "68", Semestre.Anual, itana, 1029));
            turmas.Add(new Turma(ass, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00") }, "68", Semestre.Semestre_2, itana, 5181));
            turmas.Add(new Turma(eng4, new List<Horario>() { new Horario(Dia.Terça, "19:30", "20:20"), new Horario(Dia.Terça, "20:20", "21:10"), new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10"), new Horario(Dia.Quinta, "21:20", "22:10"), new Horario(Dia.Quinta, "22:10", "23:00") }, "136", Semestre.Semestre_1, itana, 1837));
            
            turmas.Add(new Turma(cd1, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20"), new Horario(Dia.Terça, "07:45", "08:35"), new Horario(Dia.Terça, "08:35", "09:25"), new Horario(Dia.Sexta, "09:40", "10:30"), new Horario(Dia.Sexta, "10:30", "11:20") }, "102", Semestre.Semestre_2, angelo, "6878b"));
            turmas.Add(new Turma(cd1, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Semestre_2, angelo, "5174b"));

            turmas.Add(new Turma(computabilidade, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20"), new Horario(Dia.Terça, "09:40", "10:30"), new Horario(Dia.Terça, "10:30", "11:20") }, "68", Semestre.Semestre_1, ze, 6896));
            turmas.Add(new Turma(programacaoLinear, new List<Horario>() { new Horario(Dia.Quinta, "07:45", "08:35"), new Horario(Dia.Quinta, "08:35", "09:25"), new Horario(Dia.Sexta, "07:45", "08:35"), new Horario(Dia.Sexta, "08:35", "09:25") }, "68", Semestre.Semestre_1, ze, 1761));
            turmas.Add(new Turma(paa, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20"), new Horario(Dia.Segunda, "11:20", "12:10"), new Horario(Dia.Quarta, "09:40", "10:30"), new Horario(Dia.Quarta, "10:30", "11:20"), new Horario(Dia.Quarta, "11:20", "12:10") }, "102", Semestre.Semestre_2, ze, 6889));
            turmas.Add(new Turma(paa, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Semestre_2, ze, 5184));

            turmas.Add(new Turma(paradigmaImpOO, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Semestre_2, juliana, "5185b"));
            turmas.Add(new Turma(pss, new List<Horario>() { new Horario(Dia.Terça, "19:30", "20:20"), new Horario(Dia.Terça, "20:20", "21:10"), new Horario(Dia.Quinta, "21:20", "22:10"), new Horario(Dia.Quinta, "22:10", "23:00") }, "68", Semestre.Semestre_1, juliana, "5176b"));

            turmas.Add(new Turma(computabilidade, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20"), new Horario(Dia.Terça, "09:40", "10:30"), new Horario(Dia.Terça, "10:30", "11:20") }, "68", Semestre.Semestre_1, linnyer, "6896b"));
            turmas.Add(new Turma(lingFormaisAutomatos, new List<Horario>() { new Horario(Dia.Segunda, "15:30", "16:20"), new Horario(Dia.Segunda, "16:20", "17:10"), new Horario(Dia.Segunda, "17:10", "18:00"), new Horario(Dia.Terça, "15:30", "16:20"), new Horario(Dia.Terça, "16:20", "17:10"), new Horario(Dia.Terça, "17:10", "18:00") }, "102", Semestre.Semestre_1, linnyer, 6883));
            turmas.Add(new Turma(lingFormaisAutomatos, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Terça, "19:30", "20:20"), new Horario(Dia.Terça, "20:20", "21:10"), new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00") }, "102", Semestre.Semestre_1, linnyer, 5195));

            turmas.Add(new Turma(redes, new List<Horario>() { new Horario(Dia.Quarta, "09:40", "10:30"), new Horario(Dia.Quarta, "10:30", "11:20") }, "68", Semestre.Anual, luciana, "1023b"));
            turmas.Add(new Turma(sd, new List<Horario>() { new Horario(Dia.Terça, "15:30", "16:20"), new Horario(Dia.Terça, "16:20", "17:10"), new Horario(Dia.Terça, "17:10", "18:00"), new Horario(Dia.Quinta, "15:30", "16:20"), new Horario(Dia.Quinta, "16:20", "17:10"), new Horario(Dia.Quinta, "17:10", "18:00") }, "102", Semestre.Semestre_1, luciana, 1756));
            turmas.Add(new Turma(redes, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10") }, "68", Semestre.Semestre_2, luciana, "5203b"));
            turmas.Add(new Turma(sd, new List<Horario>() { new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10"), new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Sexta, "20:20", "21:10") }, "68", Semestre.Semestre_1, luciana, 1838));

            turmas.Add(new Turma(grafos, new List<Horario>() { new Horario(Dia.Quarta, "09:40", "10:30"), new Horario(Dia.Quarta, "10:30", "11:20"), new Horario(Dia.Sexta, "09:40", "10:30"), new Horario(Dia.Sexta, "10:30", "11:20") }, "68", Semestre.Semestre_1, malbarbo, 6898));
            turmas.Add(new Turma(lp, new List<Horario>() { new Horario(Dia.Quinta, "13:30", "14:20"), new Horario(Dia.Quinta, "14:20", "15:10") }, "68", Semestre.Anual, malbarbo, 1028));
            turmas.Add(new Turma(logica, new List<Horario>() { new Horario(Dia.Terça, "07:45", "08:35"), new Horario(Dia.Terça, "08:35", "09:25"), new Horario(Dia.Quinta, "07:45", "08:35"), new Horario(Dia.Quinta, "08:35", "09:25") }, "68", Semestre.Semestre_2, malbarbo, 6902));
            turmas.Add(new Turma(grafos, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10") }, "68", Semestre.Semestre_1, malbarbo, 5189));
            turmas.Add(new Turma(logica, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00") }, "68", Semestre.Semestre_1, malbarbo, 5200));

            turmas.Add(new Turma(bd1, new List<Horario>() { new Horario(Dia.Terça, "09:40", "10:30"), new Horario(Dia.Terça, "10:30", "11:20"), new Horario(Dia.Quinta, "07:45", "08:35"), new Horario(Dia.Quinta, "08:35", "09:25") }, "68", Semestre.Semestre_2, madalena, 6892));
            turmas.Add(new Turma(bd2, new List<Horario>() { new Horario(Dia.Terça, "09:40", "10:30"), new Horario(Dia.Terça, "10:30", "11:20"), new Horario(Dia.Terça, "11:20", "12:10"), new Horario(Dia.Quinta, "15:30", "16:20"), new Horario(Dia.Quinta, "16:20", "17:10"), new Horario(Dia.Quinta, "17:10", "18:00") }, "102", Semestre.Semestre_2, madalena, 1752));
            turmas.Add(new Turma(ti, new List<Horario>() { new Horario(Dia.Segunda, "13:30", "14:20"), new Horario(Dia.Segunda, "14:20", "15:10"), new Horario(Dia.Quinta, "09:40", "10:30"), new Horario(Dia.Quinta, "10:30", "11:20") }, "68", Semestre.Semestre_1, madalena, "6874b"));
            turmas.Add(new Turma(ti, new List<Horario>() { new Horario(Dia.Terça, "19:30", "20:20"), new Horario(Dia.Terça, "20:20", "21:10"), new Horario(Dia.Quinta, "21:20", "22:10"), new Horario(Dia.Quinta, "22:10", "23:00") }, "68", Semestre.Semestre_1, madalena, 5167));


            turmas.Add(new Turma(bd1, new List<Horario>() { new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10"), new Horario(Dia.Quarta, "21:20", "22:10") }, "102", Semestre.Anual, munif, 2495));

            turmas.Add(new Turma(cd1, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20"), new Horario(Dia.Terça, "07:45", "08:35"), new Horario(Dia.Terça, "08:35", "09:25"), new Horario(Dia.Sexta, "09:40", "10:30"), new Horario(Dia.Sexta, "10:30", "11:20") }, "102", Semestre.Semestre_2, nardenio, "6878c"));
            turmas.Add(new Turma(cd2, new List<Horario>() { new Horario(Dia.Segunda, "07:45", "08:35"), new Horario(Dia.Segunda, "08:35", "09:25"), new Horario(Dia.Quarta, "07:45", "08:35"), new Horario(Dia.Quarta, "08:35", "09:25") }, "68", Semestre.Semestre_1, nardenio, "6882b"));
            turmas.Add(new Turma(cd1, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Semestre_2, nardenio, "5174c"));

            turmas.Add(new Turma(bd1, new List<Horario>() { new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00") }, "68", Semestre.Semestre_2, balancieri, "5182b"));
            turmas.Add(new Turma(ti, new List<Horario>() { new Horario(Dia.Terça, "19:30", "20:20"), new Horario(Dia.Terça, "20:20", "21:10"), new Horario(Dia.Quinta, "21:20", "22:10"), new Horario(Dia.Quinta, "22:10", "23:00") }, "68", Semestre.Semestre_1, balancieri, "5167b"));

            turmas.Add(new Turma(algoritmos, new List<Horario>() { new Horario(Dia.Segunda, "07:45", "08:35"), new Horario(Dia.Segunda, "08:35", "09:25"), new Horario(Dia.Terça, "15:30", "16:20"), new Horario(Dia.Terça, "16:20", "17:10"), new Horario(Dia.Quinta, "07:45", "08:35"), new Horario(Dia.Quinta, "08:35", "09:25") }, "102", Semestre.Semestre_2, ronaldo, "6879b"));
            turmas.Add(new Turma(progInterfaceHardSoft, new List<Horario>() { new Horario(Dia.Terça, "13:30", "14:20"), new Horario(Dia.Terça, "14:20", "15:10"), new Horario(Dia.Sexta, "07:45", "08:35"), new Horario(Dia.Sexta, "08:35", "09:25") }, "68", Semestre.Semestre_1, ronaldo, 6894));
            turmas.Add(new Turma(estrutura, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00") }, "102", Semestre.Semestre_1, ronaldo, "5180b"));
            turmas.Add(new Turma(assembly, new List<Horario>() { new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00") }, "68", Semestre.Semestre_2, ronaldo, 5193));

            turmas.Add(new Turma(ia, new List<Horario>() { new Horario(Dia.Segunda, "15:30", "16:20"), new Horario(Dia.Segunda, "16:20", "17:10"), new Horario(Dia.Quarta, "13:30", "14:20"), new Horario(Dia.Quarta, "14:20", "15:10"), new Horario(Dia.Quinta, "13:30", "14:20"), new Horario(Dia.Quinta, "14:20", "15:10") }, "102", Semestre.Semestre_1, sergio, 1743));
            turmas.Add(new Turma(ihc, new List<Horario>() { new Horario(Dia.Terça, "13:30", "14:20"), new Horario(Dia.Terça, "14:20", "15:10"), new Horario(Dia.Quinta, "13:30", "14:20"), new Horario(Dia.Quinta, "14:20", "15:10") }, "68", Semestre.Semestre_2, sergio, 6890));
            turmas.Add(new Turma(ihc, new List<Horario>() { new Horario(Dia.Terça, "19:30", "20:20"), new Horario(Dia.Terça, "20:20", "21:10"), new Horario(Dia.Quinta, "21:20", "22:10"), new Horario(Dia.Quinta, "22:10", "23:00") }, "68", Semestre.Semestre_2, sergio, 5183));
            turmas.Add(new Turma(ia, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Sexta, "20:20", "21:10") }, "68", Semestre.Semestre_2, sergio, 5236));

            turmas.Add(new Turma(aspectosPsicologicos, new List<Horario>() { new Horario(Dia.Segunda, "15:30", "16:20"), new Horario(Dia.Segunda, "16:20", "17:10"), new Horario(Dia.Segunda, "17:10", "18:00") }, "68", Semestre.Anual, tania, 1027));
            turmas.Add(new Turma(eng3, new List<Horario>() { new Horario(Dia.Quinta, "09:40", "10:30"), new Horario(Dia.Quarta, "10:30", "11:20"), new Horario(Dia.Quinta, "11:20", "12:10") }, "102", Semestre.Anual, tania, 1030));
            turmas.Add(new Turma(qualidade, new List<Horario>() { new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Anual, tania, 1485));
            turmas.Add(new Turma(tecAvancadasSI, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00") }, "68", Semestre.Anual, tania, 1499));
            turmas.Add(new Turma(eng3, new List<Horario>() { new Horario(Dia.Terça, "19:30", "20:20"), new Horario(Dia.Terça, "20:20", "21:10"), new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10") }, "136", Semestre.Anual, tania, 1227));
            turmas.Add(new Turma(informaticaESociedade, new List<Horario>() { new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10") }, "34", Semestre.Anual, tania, 5198));

            turmas.Add(new Turma(ord, new List<Horario>() { new Horario(Dia.Segunda, "13:30", "14:20"), new Horario(Dia.Segunda, "14:20", "15:10"), new Horario(Dia.Quarta, "13:30", "14:20"), new Horario(Dia.Quarta, "14:20", "15:10") }, "68", Semestre.Semestre_1, valeria, 6897));
            turmas.Add(new Turma(paradigmaImpOO, new List<Horario>() { new Horario(Dia.Segunda, "13:30", "14:20"), new Horario(Dia.Segunda, "14:20", "15:10"), new Horario(Dia.Quarta, "13:30", "14:20"), new Horario(Dia.Quarta, "14:20", "15:10") }, "68", Semestre.Semestre_2, valeria, 6888));
            turmas.Add(new Turma(ord, new List<Horario>() { new Horario(Dia.Terça, "19:30", "20:20"), new Horario(Dia.Terça, "20:20", "21:10"), new Horario(Dia.Quinta, "21:20", "22:10"), new Horario(Dia.Quinta, "22:10", "23:00") }, "68", Semestre.Semestre_1, valeria, 5187));
            turmas.Add(new Turma(paradigmaImpOO, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Semestre_2, valeria, 5185));

            turmas.Add(new Turma(compiladores, new List<Horario>() { new Horario(Dia.Quinta, "07:45", "08:35"), new Horario(Dia.Quinta, "08:35", "09:25") }, "68", Semestre.Anual, wigarashi, 1020));
            turmas.Add(new Turma(so2, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20") }, "68", Semestre.Anual, wigarashi, 1037));
            turmas.Add(new Turma(compiladores, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Semestre_2, wigarashi, "5204b"));
            turmas.Add(new Turma(progSistWeb, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Sexta, "20:20", "21:10") }, "68", Semestre.Semestre_1, wigarashi, "5188b"));
            turmas.Add(new Turma(poo, new List<Horario>() { new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "22:20", "21:10"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00") }, "68", Semestre.Semestre_1, wigarashi, "5190b"));

            turmas.Add(new Turma(estrutura, new List<Horario>() { new Horario(Dia.Segunda, "13:30", "14:20"), new Horario(Dia.Segunda, "14:20", "15:10"), new Horario(Dia.Terça, "09:40", "10:30"), new Horario(Dia.Terça, "10:30", "11:20"), new Horario(Dia.Sexta, "07:45", "08:35"), new Horario(Dia.Sexta, "08:35", "09:25") }, "102", Semestre.Semestre_1, wesley, "6884b"));
            turmas.Add(new Turma(algoritmos, new List<Horario>() { new Horario(Dia.Segunda, "07:45", "08:35"), new Horario(Dia.Segunda, "08:35", "09:25"), new Horario(Dia.Terça, "15:30", "16:20"), new Horario(Dia.Terça, "16:20", "17:10"), new Horario(Dia.Quinta, "07:45", "08:35"), new Horario(Dia.Quinta, "08:35", "09:25") }, "102", Semestre.Semestre_2, wesley, "6879c"));
            turmas.Add(new Turma(algoritmos, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00"), new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Sexta, "20:20", "21:10") }, "102", Semestre.Semestre_2, wesley, "5175c"));

            #endregion


            constantino.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>(){ modelagem, metodosModelagem });
            carniel.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { bd1, bd2 });
            carniel.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { ti, pesquisa });
            airton.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { algoritmos, algoritmosEProg, mf1, mf2 });
            airton.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { estrutura, fundProgramacao });
            anderson.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { compiladores, concorrente });
            anderson.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { arq, arq2, assembly });
            sica.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() {cd1,cd2});
            sica.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() {fundamentosEletronica});
            clelia.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() {algoritmos,algoritmosEProg,fundProgramacao});
            clelia.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() {estrutura});
            dante.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() {cg, pesquisa});
            dante.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { topicosPesquisa });
            donizete.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() {eng1, eng2, eng3, eng4,pss,iss});
            donizete.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { qualidade, ass});
            elisa.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() {ass, qualidade,pss});
            elisa.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { eng1, eng2, eng3, eng4, iss});
            elvio.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { redes});
            elvio.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() {fundamentosEletronica, sd});
            flavio.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() {so, so2, sd});
            flavio.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { compiladores});
            itana.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { ass, pss,projetoSoftware, gerenciamentoProjeto});
            itana.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { eng1, eng2, eng3, eng4});
            angelo.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { cd1, cd2});
            angelo.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { fundamentosEletronica });
            ze.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { paa});
            ze.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { grafos, teoriaDosGrafos });
            jucelia.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() {informaticaESociedade});
            jucelia.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() {programacaoLinear });
            linnyer.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { lingFormaisAutomatos});
            linnyer.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { informaticaESociedade });
            luciana.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { redes, sd});
            luciana.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { fundamentosEletronica });
            wigarashi.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() {ia,poo, progSistWeb});
            wigarashi.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { compiladores, eng1});
            malbarbo.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { grafos,lp,paradigmaImpOO});
            malbarbo.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { paa});
            madalena.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() {bd1, bd2});
            madalena.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { ti,pesquisa});
            yandre.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { lingFormaisAutomatos});
            yandre.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() {informaticaESociedade });
            valeria.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() {ord,paradigmaImpOO,lp});
            valeria.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() {bd1,bd2 });
            thelma.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() {informaticaESociedade,pesquisaOperacional, pesquisa,topicosPesquisa});
            thelma.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { tecAvancadasSI});
            raqueline.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() {concorrente});
            raqueline.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() {eng3 });
            ronaldo.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { assembly, algoritmos,algoritmosEProg,estrutura});
            ronaldo.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { fundProgramacao});
            sergio.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { ia,ihc});
            sergio.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() {lp });
            tania.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { eng1,eng2,eng3});
            tania.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { ass,pss});
            wesley.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { algoritmos,estrutura,algoritmosEProg});
            wesley.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { lp});
            andre.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() {arq,arq2});
            andre.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { fundamentosEletronica });
            franklin.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() {algoritmos,algoritmosEProg,estrutura});
            franklin.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { fundProgramacao});
            nardenio.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { cd1, cd2});
            nardenio.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { fundamentosEletronica });
            uber.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { algoritmos,algoritmosEProg,estrutura,fundProgramacao});
            uber.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { fundComputacao});
            heloise.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { bd1,bd2,ti});
            heloise.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { pesquisa });
            munif.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { bd1,bd2,poo});
            munif.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { fundComputacao});
            balancieri.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { bd1,bd2,poo,algoritmos,progSistWeb});
            balancieri.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { qualidade});
            juliana.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { paa,pesquisa});
            juliana.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() {organizacaoComputadores});
            rodrigo.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { cd1, cd2,paa });
            rodrigo.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { fundamentosEletronica });
            edson.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { poo,progSistWeb});
            edson.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { bd1,bd2 });
            carlosFransley.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() {paa,grafos,teoriaDosGrafos});
            carlosFransley.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { paradigmaImpOO});
            
            string texto = GeradorArquivo.getTexto(professores,
                        turmas);

            var textoEsperado =
                "a";

            Assert.AreEqual(textoEsperado.ToString(), texto);
        }


        [TestMethod]
        public void testaGeracaoArquivoCasoRealDIN2()
        {
            var turmas = new List<Turma>();
            var professores = new List<Professor>();

            var compiladores = new Disciplina("Compiladores");
            var redes = new Disciplina("Redes de Computadores");
            var aspectosPsicologicos = new Disciplina("Aspectos Psicológicos e Sociais da Informática");
            var lp = new Disciplina("Linguagens de Programação");
            var tccCordenacao = new Disciplina("Trabalho de Graduação (Coordenação)");
            var eng3 = new Disciplina("Processo de Engenharia de Software III");
            var eng4 = new Disciplina("Processo de Engenharia de Software IV");
            var tcc = new Disciplina("Trabalho de Conclusão de Curso (Coordenação)");
            var automacao = new Disciplina("Automação");
            var progSistWeb = new Disciplina("Programação de sistemas WEB");
            var so2 = new Disciplina("Sistemas Operacionais II");
            var ia = new Disciplina("Inteligência Artificial");
            var mf1 = new Disciplina("Métodos Formais I");
            var teoriaDosGrafos = new Disciplina("Teoria dos Grafos e Análise de Algoritmos");
            var bd2 = new Disciplina("Banco de Dados II");
            var ambienteDesenv = new Disciplina("Ambientes de Desenvolvimento de Software");
            var mf2 = new Disciplina("Métodos Formais II");
            var sd = new Disciplina("Sistemas Distribuídos");
            var arq2 = new Disciplina("Arquitetura de Computadores II");
            var cg = new Disciplina("Computação Gráfica");
            var topicosPesquisa = new Disciplina("Tópicos em Pesquisa Operacional");
            var programacaoLinear = new Disciplina("Programação Linear");
            var simulacaoSistemas = new Disciplina("Simulação de Sistemas");
            var fundamentosEletronica = new Disciplina("Fundamentos de Eletrônica");
            var ti = new Disciplina("Fundamentos de Tecnologia da Informação");
            var pesquisa = new Disciplina("Fundamentos de Pesquisa");
            var cd1 = new Disciplina("Circuitos Digitais I");
            var algoritmos = new Disciplina("Fundamentos de Algoritmos");
            var cd2 = new Disciplina("Circuitos Digitais II");
            var lingFormaisAutomatos = new Disciplina("Linguagens Formais e Automatos");
            var estrutura = new Disciplina("Estruturas de Dados");
            var eng1 = new Disciplina("Proc. de Soft. e Eng. de Requisitos");
            var arq = new Disciplina("Arq. e Organiz. de Computadores");
            var paa = new Disciplina("Projeto e Análise de Algoritmos");
            var poo = new Disciplina("Par.de Prog.Imp.e Orient.a Objetos");
            var ihc = new Disciplina("Interação ser humano computador");
            var ass = new Disciplina("Análise de Sistemas de Software");
            var bd1 = new Disciplina("Banco de Dados I");
            var progInterfaceHardSoft = new Disciplina("Progr. Interfaceamento de Hard e Soft");
            var computabilidade = new Disciplina("Computabilidade");
            var ord = new Disciplina("Organização e Recuperação de Dados");
            var grafos = new Disciplina("Algortimos em Grafos");
            var pss = new Disciplina("Projeto de Sistemas de Software");
            var matematicaComputacional = new Disciplina("Matemática Computacional");
            var so = new Disciplina("Sistemas Operacionais");
            var logica = new Disciplina("Paradigma Prog. Lógica e Funcional");
            var modelagem = new Disciplina("Modelagem e Otimização Algorítmica");
            var iss = new Disciplina("Implementação de Sistemas de Software");
            var informaticaESociedade = new Disciplina("Informática e Sociedade");
            var concorrente = new Disciplina("Programação Concorrentes");
            var aprendizagemMaquina = new Disciplina("Aprendizagem de Máquina");
            var estagio = new Disciplina("Estágio Docência");
            var gerenciamentoProjeto = new Disciplina("Gerenciamento de Projetos de Software");
            var metodosModelagem = new Disciplina("Métodos de Modelagem de Software");
            var pesquisaOperacional = new Disciplina("Pesquisa Operacional");
            var seminario = new Disciplina("Seminários I");
            var seminarios2 = new Disciplina("Seminários II");
            var tc = new Disciplina("Teoria da Computação");
            var sistemasComputacao = new Disciplina("Tópicos em Sistemas de Computacão I");
            var sistemasComputacao2 = new Disciplina("Tópicos em Sistemas de Computacão II");
            var sistemasComputacao3 = new Disciplina("Tópicos em Sistemas de Computacão III");
            var sistemaInformacao = new Disciplina("Tópicos em Sistemas de Informação I");
            var sistemaInformacao2 = new Disciplina("Tópicos em Sistemas de Informação II");
            var trabIndividual = new Disciplina("Trabalho Individual");
            var organizacaoComputadores = new Disciplina("Organização de Computadores e Sistemas Operacionais");
            var eng2 = new Disciplina("Engenharia de Software II");
            var qualidade = new Disciplina("Qualidade de Software");
            var projetoSoftware = new Disciplina("Projeto de Software");
            var fundProgramacao = new Disciplina("Fundamentos de Programação");
            var tecAvancadasSI = new Disciplina("Tecnologias Avançadas para Sistemas de Informação");
            var progSistemas = new Disciplina("Programação de Sistemas");
            var algoritmosEProg = new Disciplina("Algoritmos e Prog Comp");
            var assembly = new Disciplina("Programação em Ling de Montagem");
            var paradigmaImpOO = new Disciplina("Par.de Prog.Imp.e Orient.a Objetos");
            var infoAgro = new Disciplina("Informática Aplicada a Agronomia");
            var fundComputacao = new Disciplina("Fundamentos da Computação (MEC)");
            var secretariado = new Disciplina("Introdução à Informática Secreatiado");
            var labProgramacao = new Disciplina("Lab. Fund de Programação");

            //turmas.Add(new Turma(redes, new List<Horario>() { new Horario(Dia.Quarta, "09:40", "10:30"), new Horario(Dia.Quarta, "10:30", "11:20") }, "68", Semestre.Anual, 1023));
            turmas.Add(new Turma(tccCordenacao, new List<Horario>() { new Horario(Dia.Terça, "13:30", "14:20"), new Horario(Dia.Terça, "14:20", "15:10") }, "68", Semestre.Anual, "1029b"));
            //new Turma(automacao, new List<Horario>() { new Horario(Dia.Quarta, "15:30", "16:20"), new Horario(Dia.Quarta, "16:20", "17:10"), new Horario(Dia.Quarta, "17:10", "18:00") }, "102", Semestre.Anual, 1034));
            //new Turma(mf1, new List<Horario>() { new Horario(Dia.Segunda, "13:30", "14:20"), new Horario(Dia.Segunda, "14:20", "15:10"), new Horario(Dia.Terça, "15:30", "16:20"), new Horario(Dia.Terça, "16:20", "17:10") }, "68", Semestre.Semestre_2, 1744));
            turmas.Add(new Turma(teoriaDosGrafos, new List<Horario>() { new Horario(Dia.Segunda, "13:30", "14:20"), new Horario(Dia.Segunda, "14:20", "15:10"), new Horario(Dia.Terça, "15:30", "16:20"), new Horario(Dia.Terça, "16:20", "17:10"), new Horario(Dia.Quinta, "13:30", "14:20"), new Horario(Dia.Quinta, "14:20", "13:10") }, "102", Semestre.Semestre_1, 1747));
            turmas.Add(new Turma(mf2, new List<Horario>() { new Horario(Dia.Terça, "15:30", "16:20"), new Horario(Dia.Terça, "16:20", "17:10"), new Horario(Dia.Quarta, "07:45", "08:35"), new Horario(Dia.Quarta, "08:35", "09:25") }, "68", Semestre.Semestre_2, 1754));
            //new Turma(topicosPesquisa, new List<Horario>() { new Horario(Dia.Segunda, "13:30", "14:20"), new Horario(Dia.Segunda, "14:20", "15:10"), new Horario(Dia.Quarta, "13:30", "14:20"), new Horario(Dia.Quarta, "14:20", "15:10"), new Horario(Dia.Quinta, "09:40", "10:30"), new Horario(Dia.Quinta, "10:30", "11:20") }, "102", Semestre.Semestre_2, 1760));
            //turmas.Add(new Turma(programacaoLinear, new List<Horario>() { new Horario(Dia.Quinta, "07:45", "08:35"), new Horario(Dia.Quinta, "08:35", "09:25"), new Horario(Dia.Sexta, "07:45", "08:35"), new Horario(Dia.Sexta, "08:35", "09:25")}, "68", Semestre.Semestre_1, 1761));
            turmas.Add(new Turma(simulacaoSistemas, new List<Horario>() { new Horario(Dia.Terça, "13:30", "14:20"), new Horario(Dia.Terça, "14:20", "15:10"), new Horario(Dia.Quinta, "07:45", "08:35"), new Horario(Dia.Quinta, "08:35", "09:25") }, "68", Semestre.Semestre_2, 1762));
            turmas.Add(new Turma(arq, new List<Horario>() { new Horario(Dia.Quarta, "09:40", "10:30"), new Horario(Dia.Quarta, "10:30", "11:20"), new Horario(Dia.Quinta, "15:30", "16:20"), new Horario(Dia.Quinta, "16:20", "17:10"), new Horario(Dia.Sexta, "07:45", "08:35"), new Horario(Dia.Sexta, "08:35", "09:25") }, "102", Semestre.Semestre_2, "6887b"));
            turmas.Add(new Turma(fundamentosEletronica, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20"), new Horario(Dia.Terça, "07:45", "08:35"), new Horario(Dia.Terça, "08:35", "09:25"), new Horario(Dia.Sexta, "09:40", "10:30"), new Horario(Dia.Sexta, "10:30", "11:20") }, "102", Semestre.Semestre_1, "6872b"));
            turmas.Add(new Turma(fundamentosEletronica, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20"), new Horario(Dia.Terça, "07:45", "08:35"), new Horario(Dia.Terça, "08:35", "09:25"), new Horario(Dia.Sexta, "09:40", "10:30"), new Horario(Dia.Sexta, "10:30", "11:20") }, "102", Semestre.Semestre_1, "6872c"));
            turmas.Add(new Turma(ass, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20"), new Horario(Dia.Quinta, "09:40", "10:30"), new Horario(Dia.Quinta, "10:30", "11:20") }, "68", Semestre.Semestre_2, "6891b"));
            turmas.Add(new Turma(programacaoLinear, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20"), new Horario(Dia.Segunda, "11:20", "12:10"), new Horario(Dia.Quarta, "09:40", "10:30"), new Horario(Dia.Quarta, "10:30", "11:20"), new Horario(Dia.Quarta, "11:20", "12:10") }, "102", Semestre.Semestre_2, "6889b"));

            turmas.Add(new Turma(mf2, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00") }, "68", Semestre.Semestre_1, 1684));
            //turmas.Add(new Turma(eng4, new List<Horario>() { new Horario(Dia.Terça, "19:30", "20:20"), new Horario(Dia.Terça, "20:20", "21:10"), new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10"), new Horario(Dia.Quinta, "21:20", "22:10"), new Horario(Dia.Quinta, "22:10", "23:00") }, "136", Semestre.Semestre_1, 1837));
            turmas.Add(new Turma(eng4, new List<Horario>() { new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10"), new Horario(Dia.Quinta, "21:20", "22:10"), new Horario(Dia.Quinta, "22:10", "23:00") }, "136", Semestre.Semestre_1, "1837b"));
            turmas.Add(new Turma(ass, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00") }, "68", Semestre.Semestre_2, "5181b"));

            turmas.Add(new Turma(organizacaoComputadores, new List<Horario>() { new Horario(Dia.Sabado, "13:30", "14:20"), new Horario(Dia.Sabado, "14:20", "15:10"), new Horario(Dia.Sabado, "15:30", "16:20") }, "102", Semestre.Anual, 1452));
            turmas.Add(new Turma(sd, new List<Horario>() { new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00") }, "68", Semestre.Anual, 1487));
            turmas.Add(new Turma(eng2, new List<Horario>() { new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Anual, 1470));

            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Terça, "07:45", "08:35"), new Horario(Dia.Quarta, "07:45", "08:35"), new Horario(Dia.Quarta, "08:35", "09:25") }, "136", Semestre.Anual, 1590));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Terça, "08:35", "09:25"), new Horario(Dia.Quarta, "07:45", "08:35"), new Horario(Dia.Quarta, "08:35", "09:25") }, "136", Semestre.Anual, "1590b"));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10") }, "68", Semestre.Semestre_2, 1640));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10") }, "68", Semestre.Semestre_2, "1640b"));
            turmas.Add(new Turma(fundProgramacao, new List<Horario>() { new Horario(Dia.Quarta, "09:40", "10:30"), new Horario(Dia.Quarta, "10:30", "11:20"), new Horario(Dia.Quarta, "11:20", "12:10") }, "51", Semestre.Semestre_2, 2536));
            turmas.Add(new Turma(fundProgramacao, new List<Horario>() { new Horario(Dia.Quarta, "15:30", "16:20"), new Horario(Dia.Quarta, "16:20", "17:10"), new Horario(Dia.Quarta, "17:10", "18:00") }, "51", Semestre.Semestre_2, "2536b"));
            turmas.Add(new Turma(labProgramacao, new List<Horario>() { new Horario(Dia.Quarta, "13:30", "14:20") }, "17", Semestre.Semestre_2, 2541));
            turmas.Add(new Turma(labProgramacao, new List<Horario>() { new Horario(Dia.Quarta, "07:45", "08:35") }, "17", Semestre.Semestre_2, "2541b"));
            turmas.Add(new Turma(labProgramacao, new List<Horario>() { new Horario(Dia.Quinta, "13:30", "14:20") }, "17", Semestre.Semestre_2, "2541c"));
            turmas.Add(new Turma(labProgramacao, new List<Horario>() { new Horario(Dia.Quinta, "07:45", "08:35") }, "17", Semestre.Semestre_2, "2541d"));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Sexta, "09:40", "10:30"), new Horario(Dia.Sexta, "10:30", "11:20"), new Horario(Dia.Quarta, "15:30", "16:20") }, "68", Semestre.Anual, "208"));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Quarta, "16:20", "17:10"), new Horario(Dia.Sexta, "09:40", "10:30"), new Horario(Dia.Sexta, "10:30", "11:20") }, "68", Semestre.Anual, "208c"));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Quarta, "17:10", "18:00"), new Horario(Dia.Sexta, "09:40", "10:30"), new Horario(Dia.Sexta, "10:30", "11:20") }, "68", Semestre.Anual, "208d"));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Segunda, "15:30", "16:20"), new Horario(Dia.Segunda, "16:20", "17:10"), new Horario(Dia.Terça, "09:40", "10:30") }, "68", Semestre.Anual, "208d"));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Segunda, "15:30", "16:20"), new Horario(Dia.Segunda, "16:20", "17:10"), new Horario(Dia.Terça, "10:30", "11:20") }, "68", Semestre.Anual, "208e"));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Segunda, "15:30", "16:20"), new Horario(Dia.Segunda, "16:20", "17:10"), new Horario(Dia.Terça, "11:20", "12:10") }, "68", Semestre.Anual, "208f"));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Quarta, "09:40", "10:30"), new Horario(Dia.Quarta, "10:30", "11:20"), new Horario(Dia.Quarta, "11:20", "12:10") }, "68", Semestre.Anual, "1512b"));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Quarta, "08:35", "09:25"), new Horario(Dia.Quarta, "09:40", "10:30"), new Horario(Dia.Quarta, "10:30", "11:20") }, "68", Semestre.Anual, "1512c"));


            turmas.Add(new Turma(matematicaComputacional, new List<Horario>() { new Horario(Dia.Segunda, "15:30", "16:20"), new Horario(Dia.Segunda, "16:20", "17:10"), new Horario(Dia.Quinta, "13:30", "14:20"), new Horario(Dia.Quinta, "14:20", "15:10") }, "68", Semestre.Semestre_2, "6900b"));
            turmas.Add(new Turma(progInterfaceHardSoft, new List<Horario>() { new Horario(Dia.Terça, "13:30", "14:20"), new Horario(Dia.Terça, "14:20", "15:10"), new Horario(Dia.Sexta, "07:45", "08:35"), new Horario(Dia.Sexta, "08:35", "09:25") }, "68", Semestre.Semestre_1, "6894b"));
            turmas.Add(new Turma(estrutura, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Sabado, "13:30", "14:20"), new Horario(Dia.Sabado, "14:20", "15:10") }, "68", Semestre.Semestre_2, 7256));
            turmas.Add(new Turma(estrutura, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Sabado, "13:30", "14:20"), new Horario(Dia.Sabado, "14:20", "15:10") }, "68", Semestre.Semestre_2, "7256b"));
            turmas.Add(new Turma(estrutura, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Sabado, "13:30", "14:20"), new Horario(Dia.Sabado, "14:20", "15:10") }, "68", Semestre.Semestre_2, "7256c"));
            turmas.Add(new Turma(estrutura, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Sabado, "13:30", "14:20"), new Horario(Dia.Sabado, "14:20", "15:10") }, "68", Semestre.Semestre_2, "7256d"));
            turmas.Add(new Turma(bd1, new List<Horario>() { new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10"), new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00") }, "68", Semestre.Semestre_2, 7331));
            turmas.Add(new Turma(fundProgramacao, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00"), new Horario(Dia.Sabado, "13:30", "14:20"), new Horario(Dia.Sabado, "14:20", "15:10") }, "102", Semestre.Semestre_1, "7239b"));
            turmas.Add(new Turma(fundProgramacao, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00"), new Horario(Dia.Sabado, "13:30", "14:20"), new Horario(Dia.Sabado, "14:20", "15:10") }, "102", Semestre.Semestre_1, "7239c"));
            turmas.Add(new Turma(fundProgramacao, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00"), new Horario(Dia.Sabado, "13:30", "14:20"), new Horario(Dia.Sabado, "14:20", "15:10") }, "102", Semestre.Semestre_1, "7239d"));
            turmas.Add(new Turma(progSistemas, new List<Horario>() { new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00"), new Horario(Dia.Sabado, "15:30", "16:20"), new Horario(Dia.Sabado, "16:20", "17:10") }, "102", Semestre.Semestre_1, 7328));
            turmas.Add(new Turma(so, new List<Horario>() { new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10"), new Horario(Dia.Sabado, "13:30", "14:20"), new Horario(Dia.Sabado, "14:20", "15:10") }, "68", Semestre.Semestre_1, 7329));
            turmas.Add(new Turma(arq, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00") }, "68", Semestre.Semestre_1, "5178b"));
            turmas.Add(new Turma(ihc, new List<Horario>() { new Horario(Dia.Terça, "19:30", "20:20"), new Horario(Dia.Terça, "20:20", "21:10"), new Horario(Dia.Quinta, "21:20", "22:10"), new Horario(Dia.Quinta, "22:10", "23:00") }, "68", Semestre.Semestre_2, "5183b"));
            turmas.Add(new Turma(logica, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00") }, "68", Semestre.Semestre_1, "5200b"));
            turmas.Add(new Turma(assembly, new List<Horario>() { new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00") }, "68", Semestre.Semestre_2, "5193b"));
            turmas.Add(new Turma(concorrente, new List<Horario>() { new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00"), new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Sexta, "20:20", "21:10") }, "68", Semestre.Semestre_2, "5205b"));
            turmas.Add(new Turma(paa, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Semestre_2, "5184b"));
            turmas.Add(new Turma(paa, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Semestre_2, "5184c"));
            turmas.Add(new Turma(algoritmosEProg, new List<Horario>() { new Horario(Dia.Segunda, "13:30", "14:20"), new Horario(Dia.Segunda, "14:20", "15:10"), new Horario(Dia.Terça, "15:30", "16:20") }, "136", Semestre.Anual, 5259));
            turmas.Add(new Turma(algoritmosEProg, new List<Horario>() { new Horario(Dia.Segunda, "13:30", "14:20"), new Horario(Dia.Segunda, "14:20", "15:10"), new Horario(Dia.Terça, "16:20", "17:10") }, "136", Semestre.Anual, "5259b"));
            turmas.Add(new Turma(algoritmosEProg, new List<Horario>() { new Horario(Dia.Quarta, "13:30", "14:20"), new Horario(Dia.Quarta, "14:20", "15:10"), new Horario(Dia.Quarta, "15:30", "16:20") }, "136", Semestre.Anual, "5259c"));
            turmas.Add(new Turma(algoritmosEProg, new List<Horario>() { new Horario(Dia.Quarta, "13:30", "14:20"), new Horario(Dia.Quarta, "14:20", "15:10"), new Horario(Dia.Quarta, "16:20", "17:10") }, "136", Semestre.Anual, "5259d"));
            turmas.Add(new Turma(algoritmosEProg, new List<Horario>() { new Horario(Dia.Quinta, "15:30", "16:20"), new Horario(Dia.Quinta, "16:20", "17:10"), new Horario(Dia.Sexta, "15:30", "16:20") }, "136", Semestre.Anual, "5259e"));
            turmas.Add(new Turma(algoritmosEProg, new List<Horario>() { new Horario(Dia.Quinta, "15:30", "16:20"), new Horario(Dia.Quinta, "16:20", "17:10"), new Horario(Dia.Sexta, "16:20", "17:10") }, "136", Semestre.Anual, "5259f"));
            turmas.Add(new Turma(algoritmosEProg, new List<Horario>() { new Horario(Dia.Segunda, "15:30", "16:20"), new Horario(Dia.Segunda, "16:20", "17:10"), new Horario(Dia.Quarta, "15:30", "16:20"), new Horario(Dia.Quarta, "16:20", "17:10"), new Horario(Dia.Quinta, "15:30", "16:20"), new Horario(Dia.Quinta, "16:20", "17:10") }, "102", Semestre.Semestre_1, 4549));
            turmas.Add(new Turma(algoritmosEProg, new List<Horario>() { new Horario(Dia.Segunda, "15:30", "16:20"), new Horario(Dia.Segunda, "16:20", "17:10"), new Horario(Dia.Quarta, "13:30", "14:20"), new Horario(Dia.Quarta, "14:20", "15:10"), new Horario(Dia.Quarta, "15:30", "16:20"), new Horario(Dia.Quarta, "16:20", "17:10") }, "102", Semestre.Semestre_1, "4549b"));
            turmas.Add(new Turma(bd1, new List<Horario>() { new Horario(Dia.Segunda, "15:30", "16:20"), new Horario(Dia.Segunda, "16:20", "17:10"), new Horario(Dia.Quarta, "13:30", "14:20"), new Horario(Dia.Quarta, "14:20", "15:10"), new Horario(Dia.Quarta, "15:30", "16:20"), new Horario(Dia.Quarta, "16:20", "17:10") }, "68", Semestre.Semestre_2, 4573));
            turmas.Add(new Turma(estrutura, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00"), new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Semestre_2, 4553));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00"), new Horario(Dia.Sabado, "13:30", "14:20"), new Horario(Dia.Sabado, "14:20", "15:10") }, "102", Semestre.Anual, 4309));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00"), new Horario(Dia.Sabado, "13:30", "14:20"), new Horario(Dia.Sabado, "14:20", "15:10") }, "102", Semestre.Anual, "4309b"));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00"), new Horario(Dia.Sabado, "13:30", "14:20"), new Horario(Dia.Sabado, "14:20", "15:10") }, "102", Semestre.Anual, "4309c"));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Segunda, "15:30", "16:20"), new Horario(Dia.Segunda, "16:20", "17:10"), new Horario(Dia.Segunda, "17:10", "18:00") }, "51", Semestre.Semestre_2, "2536c"));
            turmas.Add(new Turma(fundProgramacao, new List<Horario>() { new Horario(Dia.Sexta, "09:40", "10:30"), new Horario(Dia.Sexta, "10:30", "11:20"), new Horario(Dia.Sexta, "11:20", "12:10") }, "51", Semestre.Semestre_1, 6633));
            turmas.Add(new Turma(fundProgramacao, new List<Horario>() { new Horario(Dia.Quinta, "15:30", "16:20"), new Horario(Dia.Quinta, "16:20", "17:10"), new Horario(Dia.Quinta, "17:10", "18:00") }, "51", Semestre.Semestre_1, "6633b"));
            turmas.Add(new Turma(infoAgro, new List<Horario>() { new Horario(Dia.Terça, "13:30", "14:20"), new Horario(Dia.Terça, "14:20", "15:10"), new Horario(Dia.Quinta, "13:30", "14:20"), new Horario(Dia.Quinta, "14:20", "15:10") }, "68", Semestre.Semestre_2, 4460));
            turmas.Add(new Turma(infoAgro, new List<Horario>() { new Horario(Dia.Terça, "13:30", "14:20"), new Horario(Dia.Terça, "14:20", "15:10"), new Horario(Dia.Quinta, "13:30", "14:20"), new Horario(Dia.Quinta, "14:20", "15:10") }, "68", Semestre.Semestre_2, "4460b"));
            turmas.Add(new Turma(infoAgro, new List<Horario>() { new Horario(Dia.Terça, "13:30", "14:20"), new Horario(Dia.Terça, "14:20", "15:10"), new Horario(Dia.Quinta, "13:30", "14:20"), new Horario(Dia.Quinta, "14:20", "15:10") }, "68", Semestre.Semestre_2, "4460c"));
            turmas.Add(new Turma(infoAgro, new List<Horario>() { new Horario(Dia.Terça, "13:30", "14:20"), new Horario(Dia.Terça, "14:20", "15:10"), new Horario(Dia.Quinta, "13:30", "14:20"), new Horario(Dia.Quinta, "14:20", "15:10") }, "68", Semestre.Semestre_2, "4460d"));
            turmas.Add(new Turma(secretariado, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00") }, "68", Semestre.Semestre_2, 3411));
            turmas.Add(new Turma(secretariado, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00") }, "68", Semestre.Semestre_2, "3411b"));
            turmas.Add(new Turma(cd2, new List<Horario>() { new Horario(Dia.Segunda, "07:45", "08:35"), new Horario(Dia.Segunda, "08:35", "09:25"), new Horario(Dia.Quarta, "07:45", "08:35"), new Horario(Dia.Quarta, "08:35", "09:25") }, "68", Semestre.Semestre_1, "6882c"));
            turmas.Add(new Turma(cd2, new List<Horario>() { new Horario(Dia.Segunda, "07:45", "08:35"), new Horario(Dia.Segunda, "08:35", "09:25"), new Horario(Dia.Quarta, "07:45", "08:35"), new Horario(Dia.Quarta, "08:35", "09:25") }, "68", Semestre.Semestre_1, "6882d"));
            turmas.Add(new Turma(pss, new List<Horario>() { new Horario(Dia.Terça, "07:45", "08:35"), new Horario(Dia.Terça, "08:35", "09:25"), new Horario(Dia.Quinta, "13:30", "14:20"), new Horario(Dia.Quinta, "14:20", "15:10") }, "68", Semestre.Semestre_1, 6885));
            turmas.Add(new Turma(pss, new List<Horario>() { new Horario(Dia.Terça, "07:45", "08:35"), new Horario(Dia.Terça, "08:35", "09:25"), new Horario(Dia.Quinta, "13:30", "14:20"), new Horario(Dia.Quinta, "14:20", "15:10") }, "68", Semestre.Semestre_1, "6885b"));
            turmas.Add(new Turma(cd1, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Semestre_2, "5174d"));


            #region Professores

            var constantino = new Professor(new List<Turma>(getTurmasDadoDisciplina(turmas, new List<Disciplina>() { modelagem, metodosModelagem })), new List<Turma>(getTurmasDadoDisciplina(turmas, new List<Disciplina>() { })), "Ademir Aparecido Constantino", new List<Turma>(), "8");
            var carniel = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Ademir Carniel", new List<Turma>(), "8");
            var anderson = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Anderson Faustino da Silva", new List<Turma>(), "8");
            var airton = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Airton Marco Polidório", new List<Turma>(), "8");
            var sica = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Carlos Benedito Sica de Toledo", new List<Turma>(), "8");
            var clelia = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Clélia Franco", new List<Turma>(), "8");
            var dante = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Dante Alves Medeiros Filho", new List<Turma>(), "8");
            var donizete = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Donizete Carlos Bruzarosco", new List<Turma>(), "8");
            var elisa = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Elisa Hatsue Moriya Huzita", new List<Turma>(), "8");
            var elvio = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Elvio João Leonardo", new List<Turma>(), "8");
            var flavio = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Flávio Arnaldo Braga da Silva", new List<Turma>(), "8");
            var itana = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Itana Maria de Souza Gimenes", new List<Turma>(), "8");
            var angelo = new Professor(new List<Turma>() { }, new List<Turma>() { }, "João Angelo Martini", new List<Turma>(), "8");
            var ze = new Professor(new List<Turma>() { }, new List<Turma>() { }, "José Roberto Vasconcelos", new List<Turma>(), "8");
            var josiane = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Josiane Melchiori Pinheiro", new List<Turma>(), "8");
            var jucelia = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Jucélia Geni Pereira Fregoneis", new List<Turma>(), "8");
            var linnyer = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Linnyer Beatrys Ruiz", new List<Turma>(), "8");
            var luciana = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Luciana Andréia Fondazzi Martimiano", new List<Turma>(), "8");
            var wigarashi = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Wagner Igarashi", new List<Turma>(), "8");
            var malbarbo = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Marco Aurélio Lopes Barbosa", new List<Turma>(), "8");
            var madalena = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Maria Madalena Dias", new List<Turma>(), "8");
            var yandre = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Yandre Maldonado e Gomes da Costa", new List<Turma>(), "8");
            var valeria = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Valéria Delisandra Feltrim", new List<Turma>(), "8");
            var thelma = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Thelma Elita Colanzi Lopes", new List<Turma>(), "8");
            var raqueline = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Raqueline Ritter de Moura", new List<Turma>(), "8");
            var ronaldo = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Ronaldo Augusto de Lara Gonçalves", new List<Turma>(), "8");
            var sergio = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Sérgio Roberto P. da Silva", new List<Turma>(), "8");
            var tania = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Tânia Fátima Calvi Tait", new List<Turma>(), "8");
            var wesley = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Wesley Romão", new List<Turma>(), "8");
            var andre = new Professor(new List<Turma>() { }, new List<Turma>() { }, "André Barbosa Verona", new List<Turma>(), "12");
            var franklin = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Franklin César Flores", new List<Turma>(), "8");
            var leandro = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Leandro Magno", new List<Turma>(), "16");
            var priscila = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Priscila Aparecida de Moraes", new List<Turma>(), "16");
            var nardenio = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Nardênio Almeida Martins", new List<Turma>(), "8");
            var uber = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Flávio Uber", new List<Turma>(), "8");
            var heloise = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Heloise  Manica", new List<Turma>(), "16");
            var munif = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Munif Gebara Júnior", new List<Turma>(), "16");
            var balancieri = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Renato Balancieri", new List<Turma>(), "8");
            var juliana = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Juliana Keiko Yamaguchi", new List<Turma>(), "12");
            var rodrigo = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Rodrigo Lankaites Pinheiro", new List<Turma>(), "16");
            var edson = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Edson Alves de Oliveira Júniuor", new List<Turma>(), "8");
            var carlosFransley = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Carlos Fransley", new List<Turma>(), "8");

            professores.Add(constantino);
            professores.Add(carniel);
            professores.Add(anderson);
            professores.Add(airton);
            professores.Add(sica);
            professores.Add(clelia);
            professores.Add(dante);
            professores.Add(donizete);
            professores.Add(elisa);
            professores.Add(elvio);
            professores.Add(flavio);
            professores.Add(itana);
            professores.Add(angelo);
            professores.Add(ze);
            professores.Add(josiane);
            professores.Add(jucelia);
            professores.Add(linnyer);
            professores.Add(luciana);
            professores.Add(wigarashi);
            professores.Add(malbarbo);
            professores.Add(madalena);
            professores.Add(yandre);
            professores.Add(valeria);
            professores.Add(thelma);
            professores.Add(raqueline);
            professores.Add(ronaldo);
            professores.Add(sergio);
            professores.Add(wesley);
            professores.Add(franklin);
            professores.Add(nardenio);
            professores.Add(uber);
            professores.Add(heloise);
            professores.Add(munif);
            professores.Add(balancieri);
            professores.Add(juliana);
            professores.Add(rodrigo);
            professores.Add(edson);
            professores.Add(carlosFransley);
            professores.Add(priscila);
            professores.Add(leandro);
            #endregion

            #region Turmas Consolidadas
            turmas.Add(new Turma(modelagem, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20"), new Horario(Dia.Terça, "09:40", "10:30"), new Horario(Dia.Terça, "10:30", "11:20"), new Horario(Dia.Sexta, "07:45", "08:35"), new Horario(Dia.Sexta, "08:45", "09:25") }, "102", Semestre.Semestre_2, 6903));
            turmas.Add(new Turma(topicosPesquisa, new List<Horario>() { new Horario(Dia.Segunda, "13:30", "14:20"), new Horario(Dia.Segunda, "14:20", "15:10"), new Horario(Dia.Quarta, "13:30", "14:20"), new Horario(Dia.Quarta, "14:20", "15:10"), new Horario(Dia.Quinta, "09:40", "10:30"), new Horario(Dia.Quinta, "10:30", "11:20") }, "102", Semestre.Semestre_2,1760));
            turmas.Add(new Turma(modelagem, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00") }, "102", Semestre.Semestre_2, 5199));

            turmas.Add(new Turma(matematicaComputacional, new List<Horario>() { new Horario(Dia.Segunda, "15:30", "16:20"), new Horario(Dia.Segunda, "16:20", "17:10"), new Horario(Dia.Quinta, "13:30", "14:20"), new Horario(Dia.Quinta, "14:20", "15:10") }, "68", Semestre.Semestre_2,  6900));
            turmas.Add(new Turma(mf1, new List<Horario>() { new Horario(Dia.Terça, "15:30", "16:20"), new Horario(Dia.Terça, "16:20", "17:10"), new Horario(Dia.Quinta, "15:30", "16:20"), new Horario(Dia.Quinta, "16:20", "17:10") }, "68", Semestre.Semestre_2, 1744));
            turmas.Add(new Turma(algoritmos, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00"), new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Sexta, "20:20", "21:10") }, "102", Semestre.Semestre_2,  5175));
            turmas.Add(new Turma(mf1, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Sexta, "20:20", "21:10") }, "68", Semestre.Semestre_1, 5237));

            turmas.Add(new Turma(arq, new List<Horario>() { new Horario(Dia.Segunda, "07:45", "08:35"), new Horario(Dia.Segunda, "08:35", "09:25"), new Horario(Dia.Quinta, "15:30", "16:20"), new Horario(Dia.Quinta, "16:20", "17:10"), new Horario(Dia.Sexta, "07:45", "08:35"), new Horario(Dia.Sexta, "08:35", "09:25") }, "102", Semestre.Semestre_2,  6887));
            turmas.Add(new Turma(arq2, new List<Horario>() { new Horario(Dia.Terça, "07:45", "08:35"), new Horario(Dia.Terça, "08:35", "09:25"), new Horario(Dia.Quinta, "07:45", "08:35"), new Horario(Dia.Quinta, "08:35", "09:25") }, "68", Semestre.Semestre_1,  6895));
            turmas.Add(new Turma(compiladores, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Semestre_2, 5204));
            turmas.Add(new Turma(concorrente, new List<Horario>() { new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00"), new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Sexta, "20:20", "21:10") }, "68", Semestre.Semestre_2,  5205));

            turmas.Add(new Turma(automacao, new List<Horario>() { new Horario(Dia.Quarta, "09:40", "10:30"), new Horario(Dia.Quarta, "10:30", "11:20"), new Horario(Dia.Quarta, "11:20", "12:10") }, "102", Semestre.Anual,  1034));
            turmas.Add(new Turma(cd2, new List<Horario>() { new Horario(Dia.Segunda, "07:45", "08:35"), new Horario(Dia.Segunda, "08:35", "09:25"), new Horario(Dia.Quarta, "07:45", "08:35"), new Horario(Dia.Quarta, "08:35", "09:25") }, "68", Semestre.Semestre_1, 6882));
            turmas.Add(new Turma(fundamentosEletronica, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20"), new Horario(Dia.Terça, "07:45", "08:35"), new Horario(Dia.Terça, "08:35", "09:25"), new Horario(Dia.Sexta, "09:40", "10:30"), new Horario(Dia.Sexta, "10:30", "11:20") }, "102", Semestre.Semestre_1, 6872));
            turmas.Add(new Turma(arq, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Sabado, "15:30", "16:20"), new Horario(Dia.Sabado, "16:20", "17:10") }, "68", Semestre.Semestre_2, 7330));
            turmas.Add(new Turma(arq, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00") }, "68", Semestre.Semestre_1, 5178));

            turmas.Add(new Turma(fundamentosEletronica, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20"), new Horario(Dia.Terça, "07:45", "08:35"), new Horario(Dia.Terça, "08:35", "09:25"), new Horario(Dia.Sexta, "09:40", "10:30"), new Horario(Dia.Sexta, "10:30", "11:20") }, "102", Semestre.Semestre_1, "6872d"));
            turmas.Add(new Turma(simulacaoSistemas, new List<Horario>() { new Horario(Dia.Terça, "13:30", "14:20"), new Horario(Dia.Terça, "14:20", "15:10"), new Horario(Dia.Quinta, "07:45", "08:35"), new Horario(Dia.Quinta, "08:35", "09:25") }, "68", Semestre.Semestre_2,1762));
            turmas.Add(new Turma(sd, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Sexta, "20:20", "21:10") }, "68", Semestre.Semestre_1, 5197));

            turmas.Add(new Turma(automacao, new List<Horario>() { new Horario(Dia.Quarta, "15:30", "16:20"), new Horario(Dia.Quarta, "16:20", "17:10"), new Horario(Dia.Quarta, "17:10", "18:00") }, "102", Semestre.Anual, 1034));
            turmas.Add(new Turma(ia, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Sexta, "20:20", "21:10") }, "68", Semestre.Semestre_2, "5236b"));

            turmas.Add(new Turma(fundProgramacao, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00"), new Horario(Dia.Sabado, "13:30", "14:20"), new Horario(Dia.Sabado, "14:20", "15:10") }, "102", Semestre.Semestre_1,  7239));

            turmas.Add(new Turma(cg, new List<Horario>() { new Horario(Dia.Quinta, "09:40", "10:30"), new Horario(Dia.Quinta, "10:30", "11:20"), new Horario(Dia.Quinta, "11:20", "12:10"), new Horario(Dia.Sexta, "09:40", "10:30"), new Horario(Dia.Sexta, "10:30", "11:20"), new Horario(Dia.Sexta, "11:20", "12:10") }, "102", Semestre.Semestre_1, 1759));
            turmas.Add(new Turma(cg, new List<Horario>() { new Horario(Dia.Terça, "19:30", "20:20"), new Horario(Dia.Terça, "20:20", "21:10"), new Horario(Dia.Quinta, "21:20", "22:10"), new Horario(Dia.Quinta, "22:10", "23:00") }, "68", Semestre.Semestre_2, 5202));
            turmas.Add(new Turma(pesquisa, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Semestre_1,5171));

            turmas.Add(new Turma(iss, new List<Horario>() { new Horario(Dia.Segunda, "07:45", "08:35"), new Horario(Dia.Segunda, "08:35", "09:25"), new Horario(Dia.Quinta, "09:40", "10:30"), new Horario(Dia.Quinta, "10:30", "11:20") }, "68", Semestre.Semestre_2, 6904));
            turmas.Add(new Turma(pss, new List<Horario>() { new Horario(Dia.Segunda, "07:45", "08:35"), new Horario(Dia.Segunda, "08:35", "09:25"), new Horario(Dia.Quinta, "09:40", "10:30"), new Horario(Dia.Quinta, "10:30", "11:20") }, "68", Semestre.Semestre_1,  6899));
            turmas.Add(new Turma(iss, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10") }, "68", Semestre.Semestre_2, 5191));
            turmas.Add(new Turma(iss, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00") }, "68", Semestre.Semestre_1, 5186));
            turmas.Add(new Turma(tcc, new List<Horario>() { new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00") }, "68", Semestre.Anual, 3513));

            turmas.Add(new Turma(ambienteDesenv, new List<Horario>() { new Horario(Dia.Terça, "13:30", "14:20"), new Horario(Dia.Terça, "14:20", "15:10"), new Horario(Dia.Quinta, "15:30", "16:20"), new Horario(Dia.Quinta, "16:20", "17:10") }, "68", Semestre.Semestre_2, 1753));
            turmas.Add(new Turma(progSistWeb, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Sexta, "20:20", "21:10") }, "68", Semestre.Semestre_1,  5188));
            turmas.Add(new Turma(poo, new List<Horario>() { new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "22:20", "21:10"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00") }, "68", Semestre.Semestre_1,  5190));
            turmas.Add(new Turma(ass, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20"), new Horario(Dia.Quinta, "09:40", "10:30"), new Horario(Dia.Quinta, "10:30", "11:20") }, "68", Semestre.Semestre_2,  6891));

            turmas.Add(new Turma(ti, new List<Horario>() { new Horario(Dia.Segunda, "13:30", "14:20"), new Horario(Dia.Segunda, "14:20", "15:10"), new Horario(Dia.Quinta, "09:40", "10:30"), new Horario(Dia.Quinta, "10:30", "11:20") }, "68", Semestre.Semestre_1,  6874));
            turmas.Add(new Turma(projetoSoftware, new List<Horario>() { new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Sexta, "20:20", "21:10") }, "68", Semestre.Anual, 1498));
            turmas.Add(new Turma(pss, new List<Horario>() { new Horario(Dia.Terça, "19:30", "20:20"), new Horario(Dia.Terça, "20:20", "21:10"), new Horario(Dia.Quinta, "21:20", "22:10"), new Horario(Dia.Quinta, "22:10", "23:00") }, "68", Semestre.Semestre_1, 5176));

            turmas.Add(new Turma(cd1, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20"), new Horario(Dia.Terça, "07:45", "08:35"), new Horario(Dia.Terça, "08:35", "09:25"), new Horario(Dia.Sexta, "09:40", "10:30"), new Horario(Dia.Sexta, "10:30", "11:20") }, "102", Semestre.Semestre_2, 6878));
            turmas.Add(new Turma(redes, new List<Horario>() { new Horario(Dia.Quarta, "09:40", "10:30"), new Horario(Dia.Quarta, "10:30", "11:20") }, "68", Semestre.Anual, 1023));
            turmas.Add(new Turma(cd1, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Semestre_2,5174));
            turmas.Add(new Turma(redes, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10") }, "68", Semestre.Semestre_2, 5203));

            turmas.Add(new Turma(so, new List<Horario>() { new Horario(Dia.Segunda, "13:30", "14:20"), new Horario(Dia.Segunda, "14:20", "15:10"), new Horario(Dia.Quarta, "07:45", "08:35"), new Horario(Dia.Quarta, "08:35", "09:25"), new Horario(Dia.Sexta, "09:40", "10:30"), new Horario(Dia.Sexta, "10:30", "11:20") }, "102", Semestre.Semestre_2, 6901));
            turmas.Add(new Turma(so, new List<Horario>() { new Horario(Dia.Terça, "19:30", "20:20"), new Horario(Dia.Terça, "20:20", "21:10"), new Horario(Dia.Quinta, "21:20", "22:10"), new Horario(Dia.Quinta, "22:10", "23:00") }, "68", Semestre.Semestre_1, 5196));


            turmas.Add(new Turma(arq2, new List<Horario>() { new Horario(Dia.Terça, "13:30", "14:20"), new Horario(Dia.Terça, "14:20", "15:10"), new Horario(Dia.Quinta, "09:40", "10:30"), new Horario(Dia.Quinta, "10:30", "11:20") }, "68", Semestre.Semestre_1,  1757));
            turmas.Add(new Turma(mf2, new List<Horario>() { new Horario(Dia.Segunda, "13:30", "14:20"), new Horario(Dia.Segunda, "14:20", "15:10"), new Horario(Dia.Quarta, "15:30", "16:20"), new Horario(Dia.Quarta, "16:20", "17:10") }, "68", Semestre.Semestre_2, 1754));
            turmas.Add(new Turma(algoritmos, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00"), new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Sexta, "20:20", "21:10") }, "102", Semestre.Semestre_2,  "5175b"));
            turmas.Add(new Turma(mf2, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00") }, "68", Semestre.Semestre_1, 1754));

            turmas.Add(new Turma(estrutura, new List<Horario>() { new Horario(Dia.Segunda, "13:30", "14:20"), new Horario(Dia.Segunda, "14:20", "15:10"), new Horario(Dia.Terça, "09:40", "10:30"), new Horario(Dia.Terça, "10:30", "11:20"), new Horario(Dia.Sexta, "07:45", "08:35"), new Horario(Dia.Sexta, "08:35", "09:25") }, "102", Semestre.Semestre_1,  6884));
            turmas.Add(new Turma(algoritmos, new List<Horario>() { new Horario(Dia.Segunda, "07:45", "08:35"), new Horario(Dia.Segunda, "08:35", "09:25"), new Horario(Dia.Terça, "15:30", "16:20"), new Horario(Dia.Terça, "16:20", "17:10"), new Horario(Dia.Quinta, "07:45", "08:35"), new Horario(Dia.Quinta, "08:35", "09:25") }, "102", Semestre.Semestre_2, 6879));
            turmas.Add(new Turma(estrutura, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00") }, "102", Semestre.Semestre_1,  5180));

            turmas.Add(new Turma(pesquisa, new List<Horario>() { new Horario(Dia.Terça, "15:30", "16:20"), new Horario(Dia.Terça, "16:20", "17:10") }, "34", Semestre.Semestre_1, 6875));
            turmas.Add(new Turma(pesquisa, new List<Horario>() { new Horario(Dia.Quinta, "15:30", "16:20"), new Horario(Dia.Quinta, "16:20", "17:10") }, "34", Semestre.Semestre_1,  "6875b"));
            turmas.Add(new Turma(bd1, new List<Horario>() { new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00") }, "68", Semestre.Semestre_2, 5182));
            turmas.Add(new Turma(bd2, new List<Horario>() { new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00"), new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Sexta, "20:20", "21:10"), }, "68", Semestre.Semestre_2, 5192));
            turmas.Add(new Turma(pesquisa, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "22:20", "21:10") }, "68", Semestre.Semestre_1, "5171b"));

            turmas.Add(new Turma(pesquisa, new List<Horario>() { new Horario(Dia.Terça, "07:45", "08:35"), new Horario(Dia.Terça, "08:35", "09:25") }, "68", Semestre.Anual, 1029));
            turmas.Add(new Turma(ass, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00") }, "68", Semestre.Semestre_2, 5181));
            turmas.Add(new Turma(eng4, new List<Horario>() { new Horario(Dia.Terça, "19:30", "20:20"), new Horario(Dia.Terça, "20:20", "21:10"), new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10"), new Horario(Dia.Quinta, "21:20", "22:10"), new Horario(Dia.Quinta, "22:10", "23:00") }, "136", Semestre.Semestre_1, 1837));

            turmas.Add(new Turma(cd1, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20"), new Horario(Dia.Terça, "07:45", "08:35"), new Horario(Dia.Terça, "08:35", "09:25"), new Horario(Dia.Sexta, "09:40", "10:30"), new Horario(Dia.Sexta, "10:30", "11:20") }, "102", Semestre.Semestre_2,  "6878b"));
            turmas.Add(new Turma(cd1, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Semestre_2, "5174b"));

            turmas.Add(new Turma(computabilidade, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20"), new Horario(Dia.Terça, "09:40", "10:30"), new Horario(Dia.Terça, "10:30", "11:20") }, "68", Semestre.Semestre_1,  6896));
            turmas.Add(new Turma(programacaoLinear, new List<Horario>() { new Horario(Dia.Quinta, "07:45", "08:35"), new Horario(Dia.Quinta, "08:35", "09:25"), new Horario(Dia.Sexta, "07:45", "08:35"), new Horario(Dia.Sexta, "08:35", "09:25") }, "68", Semestre.Semestre_1,  1761));
            turmas.Add(new Turma(paa, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20"), new Horario(Dia.Segunda, "11:20", "12:10"), new Horario(Dia.Quarta, "09:40", "10:30"), new Horario(Dia.Quarta, "10:30", "11:20"), new Horario(Dia.Quarta, "11:20", "12:10") }, "102", Semestre.Semestre_2, 6889));
            turmas.Add(new Turma(paa, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Semestre_2,  5184));

            turmas.Add(new Turma(paradigmaImpOO, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Semestre_2, "5185b"));
            turmas.Add(new Turma(pss, new List<Horario>() { new Horario(Dia.Terça, "19:30", "20:20"), new Horario(Dia.Terça, "20:20", "21:10"), new Horario(Dia.Quinta, "21:20", "22:10"), new Horario(Dia.Quinta, "22:10", "23:00") }, "68", Semestre.Semestre_1,  "5176b"));

            turmas.Add(new Turma(computabilidade, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20"), new Horario(Dia.Terça, "09:40", "10:30"), new Horario(Dia.Terça, "10:30", "11:20") }, "68", Semestre.Semestre_1, "6896b"));
            turmas.Add(new Turma(lingFormaisAutomatos, new List<Horario>() { new Horario(Dia.Segunda, "15:30", "16:20"), new Horario(Dia.Segunda, "16:20", "17:10"), new Horario(Dia.Segunda, "17:10", "18:00"), new Horario(Dia.Terça, "15:30", "16:20"), new Horario(Dia.Terça, "16:20", "17:10"), new Horario(Dia.Terça, "17:10", "18:00") }, "102", Semestre.Semestre_1,  6883));
            turmas.Add(new Turma(lingFormaisAutomatos, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Terça, "19:30", "20:20"), new Horario(Dia.Terça, "20:20", "21:10"), new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00") }, "102", Semestre.Semestre_1, 5195));

            turmas.Add(new Turma(redes, new List<Horario>() { new Horario(Dia.Quarta, "09:40", "10:30"), new Horario(Dia.Quarta, "10:30", "11:20") }, "68", Semestre.Anual, "1023b"));
            turmas.Add(new Turma(sd, new List<Horario>() { new Horario(Dia.Terça, "15:30", "16:20"), new Horario(Dia.Terça, "16:20", "17:10"), new Horario(Dia.Terça, "17:10", "18:00"), new Horario(Dia.Quinta, "15:30", "16:20"), new Horario(Dia.Quinta, "16:20", "17:10"), new Horario(Dia.Quinta, "17:10", "18:00") }, "102", Semestre.Semestre_1,  1756));
            turmas.Add(new Turma(redes, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10") }, "68", Semestre.Semestre_2, "5203b"));
            turmas.Add(new Turma(sd, new List<Horario>() { new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10"), new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Sexta, "20:20", "21:10") }, "68", Semestre.Semestre_1,  1838));

            turmas.Add(new Turma(grafos, new List<Horario>() { new Horario(Dia.Quarta, "09:40", "10:30"), new Horario(Dia.Quarta, "10:30", "11:20"), new Horario(Dia.Sexta, "09:40", "10:30"), new Horario(Dia.Sexta, "10:30", "11:20") }, "68", Semestre.Semestre_1,  6898));
            turmas.Add(new Turma(lp, new List<Horario>() { new Horario(Dia.Quinta, "13:30", "14:20"), new Horario(Dia.Quinta, "14:20", "15:10") }, "68", Semestre.Anual,  1028));
            turmas.Add(new Turma(logica, new List<Horario>() { new Horario(Dia.Terça, "07:45", "08:35"), new Horario(Dia.Terça, "08:35", "09:25"), new Horario(Dia.Quinta, "07:45", "08:35"), new Horario(Dia.Quinta, "08:35", "09:25") }, "68", Semestre.Semestre_2,  6902));
            turmas.Add(new Turma(grafos, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10") }, "68", Semestre.Semestre_1, 5189));
            turmas.Add(new Turma(logica, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00") }, "68", Semestre.Semestre_1,  5200));

            turmas.Add(new Turma(bd1, new List<Horario>() { new Horario(Dia.Terça, "09:40", "10:30"), new Horario(Dia.Terça, "10:30", "11:20"), new Horario(Dia.Quinta, "07:45", "08:35"), new Horario(Dia.Quinta, "08:35", "09:25") }, "68", Semestre.Semestre_2,  6892));
            turmas.Add(new Turma(bd2, new List<Horario>() { new Horario(Dia.Terça, "09:40", "10:30"), new Horario(Dia.Terça, "10:30", "11:20"), new Horario(Dia.Terça, "11:20", "12:10"), new Horario(Dia.Quinta, "15:30", "16:20"), new Horario(Dia.Quinta, "16:20", "17:10"), new Horario(Dia.Quinta, "17:10", "18:00") }, "102", Semestre.Semestre_2, 1752));
            turmas.Add(new Turma(ti, new List<Horario>() { new Horario(Dia.Segunda, "13:30", "14:20"), new Horario(Dia.Segunda, "14:20", "15:10"), new Horario(Dia.Quinta, "09:40", "10:30"), new Horario(Dia.Quinta, "10:30", "11:20") }, "68", Semestre.Semestre_1, "6874b"));
            turmas.Add(new Turma(ti, new List<Horario>() { new Horario(Dia.Terça, "19:30", "20:20"), new Horario(Dia.Terça, "20:20", "21:10"), new Horario(Dia.Quinta, "21:20", "22:10"), new Horario(Dia.Quinta, "22:10", "23:00") }, "68", Semestre.Semestre_1,  5167));


            turmas.Add(new Turma(bd1, new List<Horario>() { new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10"), new Horario(Dia.Quarta, "21:20", "22:10") }, "102", Semestre.Anual, 2495));

            turmas.Add(new Turma(cd1, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20"), new Horario(Dia.Terça, "07:45", "08:35"), new Horario(Dia.Terça, "08:35", "09:25"), new Horario(Dia.Sexta, "09:40", "10:30"), new Horario(Dia.Sexta, "10:30", "11:20") }, "102", Semestre.Semestre_2, "6878c"));
            turmas.Add(new Turma(cd2, new List<Horario>() { new Horario(Dia.Segunda, "07:45", "08:35"), new Horario(Dia.Segunda, "08:35", "09:25"), new Horario(Dia.Quarta, "07:45", "08:35"), new Horario(Dia.Quarta, "08:35", "09:25") }, "68", Semestre.Semestre_1, "6882b"));
            turmas.Add(new Turma(cd1, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Semestre_2,  "5174c"));

            turmas.Add(new Turma(bd1, new List<Horario>() { new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00") }, "68", Semestre.Semestre_2, "5182b"));
            turmas.Add(new Turma(ti, new List<Horario>() { new Horario(Dia.Terça, "19:30", "20:20"), new Horario(Dia.Terça, "20:20", "21:10"), new Horario(Dia.Quinta, "21:20", "22:10"), new Horario(Dia.Quinta, "22:10", "23:00") }, "68", Semestre.Semestre_1,  "5167b"));

            turmas.Add(new Turma(algoritmos, new List<Horario>() { new Horario(Dia.Segunda, "07:45", "08:35"), new Horario(Dia.Segunda, "08:35", "09:25"), new Horario(Dia.Terça, "15:30", "16:20"), new Horario(Dia.Terça, "16:20", "17:10"), new Horario(Dia.Quinta, "07:45", "08:35"), new Horario(Dia.Quinta, "08:35", "09:25") }, "102", Semestre.Semestre_2,"6879b"));
            turmas.Add(new Turma(progInterfaceHardSoft, new List<Horario>() { new Horario(Dia.Terça, "13:30", "14:20"), new Horario(Dia.Terça, "14:20", "15:10"), new Horario(Dia.Sexta, "07:45", "08:35"), new Horario(Dia.Sexta, "08:35", "09:25") }, "68", Semestre.Semestre_1,  6894));
            turmas.Add(new Turma(estrutura, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00") }, "102", Semestre.Semestre_1, "5180b"));
            turmas.Add(new Turma(assembly, new List<Horario>() { new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00") }, "68", Semestre.Semestre_2,  5193));

            turmas.Add(new Turma(ia, new List<Horario>() { new Horario(Dia.Segunda, "15:30", "16:20"), new Horario(Dia.Segunda, "16:20", "17:10"), new Horario(Dia.Quarta, "13:30", "14:20"), new Horario(Dia.Quarta, "14:20", "15:10"), new Horario(Dia.Quinta, "13:30", "14:20"), new Horario(Dia.Quinta, "14:20", "15:10") }, "102", Semestre.Semestre_1, 1743));
            turmas.Add(new Turma(ihc, new List<Horario>() { new Horario(Dia.Terça, "13:30", "14:20"), new Horario(Dia.Terça, "14:20", "15:10"), new Horario(Dia.Quinta, "13:30", "14:20"), new Horario(Dia.Quinta, "14:20", "15:10") }, "68", Semestre.Semestre_2, 6890));
            turmas.Add(new Turma(ihc, new List<Horario>() { new Horario(Dia.Terça, "19:30", "20:20"), new Horario(Dia.Terça, "20:20", "21:10"), new Horario(Dia.Quinta, "21:20", "22:10"), new Horario(Dia.Quinta, "22:10", "23:00") }, "68", Semestre.Semestre_2, 5183));
            turmas.Add(new Turma(ia, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Sexta, "20:20", "21:10") }, "68", Semestre.Semestre_2, 5236));

            turmas.Add(new Turma(aspectosPsicologicos, new List<Horario>() { new Horario(Dia.Segunda, "15:30", "16:20"), new Horario(Dia.Segunda, "16:20", "17:10"), new Horario(Dia.Segunda, "17:10", "18:00") }, "68", Semestre.Anual,  1027));
            turmas.Add(new Turma(eng3, new List<Horario>() { new Horario(Dia.Quinta, "09:40", "10:30"), new Horario(Dia.Quarta, "10:30", "11:20"), new Horario(Dia.Quinta, "11:20", "12:10") }, "102", Semestre.Anual,  1030));
            turmas.Add(new Turma(qualidade, new List<Horario>() { new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Anual,  1485));
            turmas.Add(new Turma(tecAvancadasSI, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00") }, "68", Semestre.Anual, 1499));
            turmas.Add(new Turma(eng3, new List<Horario>() { new Horario(Dia.Terça, "19:30", "20:20"), new Horario(Dia.Terça, "20:20", "21:10"), new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10") }, "136", Semestre.Anual,  1227));
            turmas.Add(new Turma(informaticaESociedade, new List<Horario>() { new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10") }, "34", Semestre.Anual, 5198));

            turmas.Add(new Turma(ord, new List<Horario>() { new Horario(Dia.Segunda, "13:30", "14:20"), new Horario(Dia.Segunda, "14:20", "15:10"), new Horario(Dia.Quarta, "13:30", "14:20"), new Horario(Dia.Quarta, "14:20", "15:10") }, "68", Semestre.Semestre_1, 6897));
            turmas.Add(new Turma(paradigmaImpOO, new List<Horario>() { new Horario(Dia.Segunda, "13:30", "14:20"), new Horario(Dia.Segunda, "14:20", "15:10"), new Horario(Dia.Quarta, "13:30", "14:20"), new Horario(Dia.Quarta, "14:20", "15:10") }, "68", Semestre.Semestre_2,  6888));
            turmas.Add(new Turma(ord, new List<Horario>() { new Horario(Dia.Terça, "19:30", "20:20"), new Horario(Dia.Terça, "20:20", "21:10"), new Horario(Dia.Quinta, "21:20", "22:10"), new Horario(Dia.Quinta, "22:10", "23:00") }, "68", Semestre.Semestre_1,  5187));
            turmas.Add(new Turma(paradigmaImpOO, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Semestre_2, 5185));

            turmas.Add(new Turma(compiladores, new List<Horario>() { new Horario(Dia.Quinta, "07:45", "08:35"), new Horario(Dia.Quinta, "08:35", "09:25") }, "68", Semestre.Anual, 1020));
            turmas.Add(new Turma(so2, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20") }, "68", Semestre.Anual, 1037));
            turmas.Add(new Turma(compiladores, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Semestre_2, "5204b"));
            turmas.Add(new Turma(progSistWeb, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Sexta, "20:20", "21:10") }, "68", Semestre.Semestre_1, "5188b"));
            turmas.Add(new Turma(poo, new List<Horario>() { new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "22:20", "21:10"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00") }, "68", Semestre.Semestre_1,  "5190b"));

            turmas.Add(new Turma(estrutura, new List<Horario>() { new Horario(Dia.Segunda, "13:30", "14:20"), new Horario(Dia.Segunda, "14:20", "15:10"), new Horario(Dia.Terça, "09:40", "10:30"), new Horario(Dia.Terça, "10:30", "11:20"), new Horario(Dia.Sexta, "07:45", "08:35"), new Horario(Dia.Sexta, "08:35", "09:25") }, "102", Semestre.Semestre_1,  "6884b"));
            turmas.Add(new Turma(algoritmos, new List<Horario>() { new Horario(Dia.Segunda, "07:45", "08:35"), new Horario(Dia.Segunda, "08:35", "09:25"), new Horario(Dia.Terça, "15:30", "16:20"), new Horario(Dia.Terça, "16:20", "17:10"), new Horario(Dia.Quinta, "07:45", "08:35"), new Horario(Dia.Quinta, "08:35", "09:25") }, "102", Semestre.Semestre_2,  "6879c"));
            turmas.Add(new Turma(algoritmos, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00"), new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Sexta, "20:20", "21:10") }, "102", Semestre.Semestre_2, "5175c"));

            #endregion


            constantino.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { modelagem, metodosModelagem });
            carniel.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { bd1, bd2 });
            carniel.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { ti, pesquisa });
            airton.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { algoritmos, algoritmosEProg, mf1, mf2 });
            airton.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { estrutura, fundProgramacao });
            anderson.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { compiladores, concorrente });
            anderson.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { arq, arq2, assembly });
            sica.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { cd1, cd2 });
            sica.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { fundamentosEletronica });
            clelia.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { algoritmos, algoritmosEProg, fundProgramacao });
            clelia.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { estrutura });
            dante.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { cg, pesquisa });
            dante.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { topicosPesquisa });
            donizete.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { eng1, eng2, eng3, eng4, pss, iss });
            donizete.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { qualidade, ass });
            elisa.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { ass, qualidade, pss });
            elisa.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { eng1, eng2, eng3, eng4, iss });
            elvio.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { redes });
            elvio.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { fundamentosEletronica, sd });
            flavio.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { so, so2, sd });
            flavio.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { compiladores });
            itana.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { ass, pss, projetoSoftware, gerenciamentoProjeto });
            itana.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { eng1, eng2, eng3, eng4 });
            angelo.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { cd1, cd2 });
            angelo.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { fundamentosEletronica });
            ze.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { paa });
            ze.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { grafos, teoriaDosGrafos });
            jucelia.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { informaticaESociedade });
            jucelia.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { programacaoLinear });
            linnyer.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { lingFormaisAutomatos });
            linnyer.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { informaticaESociedade });
            luciana.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { redes, sd });
            luciana.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { fundamentosEletronica });
            wigarashi.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { ia, poo, progSistWeb });
            wigarashi.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { compiladores, eng1 });
            malbarbo.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { grafos, lp, paradigmaImpOO });
            malbarbo.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { paa });
            madalena.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { bd1, bd2 });
            madalena.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { ti, pesquisa });
            yandre.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { lingFormaisAutomatos });
            yandre.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { informaticaESociedade });
            valeria.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { ord, paradigmaImpOO, lp });
            valeria.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { bd1, bd2 });
            thelma.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { informaticaESociedade, pesquisaOperacional, pesquisa, topicosPesquisa });
            thelma.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { tecAvancadasSI });
            raqueline.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { concorrente });
            raqueline.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { eng3 });
            ronaldo.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { assembly, algoritmos, algoritmosEProg, estrutura });
            ronaldo.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { fundProgramacao });
            sergio.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { ia, ihc });
            sergio.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { lp });
            tania.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { eng1, eng2, eng3 });
            tania.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { ass, pss });
            wesley.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { algoritmos, estrutura, algoritmosEProg });
            wesley.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { lp });
            andre.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { arq, arq2 });
            andre.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { fundamentosEletronica });
            franklin.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { algoritmos, algoritmosEProg, estrutura });
            franklin.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { fundProgramacao });
            nardenio.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { cd1, cd2 });
            nardenio.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { fundamentosEletronica });
            uber.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { algoritmos, algoritmosEProg, estrutura, fundProgramacao });
            uber.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { fundComputacao });
            heloise.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { bd1, bd2, ti });
            heloise.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { pesquisa });
            munif.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { bd1, bd2, poo });
            munif.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { fundComputacao });
            balancieri.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { bd1, bd2, poo, algoritmos, progSistWeb });
            balancieri.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { qualidade });
            juliana.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { paa, pesquisa });
            juliana.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { organizacaoComputadores });
            rodrigo.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { cd1, cd2, paa });
            rodrigo.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { fundamentosEletronica });
            edson.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { poo, progSistWeb });
            edson.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { bd1, bd2 });
            carlosFransley.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { paa, grafos, teoriaDosGrafos });
            carlosFransley.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { paradigmaImpOO });

            //var b = new MainWindow();
            //var viewModel = new ViewModel();
            //viewModel.Turmas = new ObservableCollection<Turma>(turmas);
            //viewModel.Professores = new ObservableCollection<Professor>(professores);
            //b.DataContext = viewModel;
            //b.Show();

            string texto = GeradorArquivo.getTexto(professores,
                        turmas);

            var textoEsperado =
                "a";

            Assert.AreEqual(textoEsperado.ToString(), texto);
        }



        [TestMethod]
        public void analisadorCenario1()
        {
            var turmas = new List<Turma>();
            var professores = new List<Professor>();

            var compiladores = new Disciplina("Compiladores");
            var redes = new Disciplina("Redes de Computadores");
            var aspectosPsicologicos = new Disciplina("Aspectos Psicológicos e Sociais da Informática");
            var lp = new Disciplina("Linguagens de Programação");
            var tccCordenacao = new Disciplina("Trabalho de Graduação (Coordenação)");
            var eng3 = new Disciplina("Processo de Engenharia de Software III");
            var eng4 = new Disciplina("Processo de Engenharia de Software IV");
            var tcc = new Disciplina("Trabalho de Conclusão de Curso (Coordenação)");
            var automacao = new Disciplina("Automação");
            var progSistWeb = new Disciplina("Programação de sistemas WEB");
            var so2 = new Disciplina("Sistemas Operacionais II");
            var ia = new Disciplina("Inteligência Artificial");
            var mf1 = new Disciplina("Métodos Formais I");
            var teoriaDosGrafos = new Disciplina("Teoria dos Grafos e Análise de Algoritmos");
            var bd2 = new Disciplina("Banco de Dados II");
            var ambienteDesenv = new Disciplina("Ambientes de Desenvolvimento de Software");
            var mf2 = new Disciplina("Métodos Formais II");
            var sd = new Disciplina("Sistemas Distribuídos");
            var arq2 = new Disciplina("Arquitetura de Computadores II");
            var cg = new Disciplina("Computação Gráfica");
            var topicosPesquisa = new Disciplina("Tópicos em Pesquisa Operacional");
            var programacaoLinear = new Disciplina("Programação Linear");
            var simulacaoSistemas = new Disciplina("Simulação de Sistemas");
            var fundamentosEletronica = new Disciplina("Fundamentos de Eletrônica");
            var ti = new Disciplina("Fundamentos de Tecnologia da Informação");
            var pesquisa = new Disciplina("Fundamentos de Pesquisa");
            var cd1 = new Disciplina("Circuitos Digitais I");
            var algoritmos = new Disciplina("Fundamentos de Algoritmos");
            var cd2 = new Disciplina("Circuitos Digitais II");
            var lingFormaisAutomatos = new Disciplina("Linguagens Formais e Automatos");
            var estrutura = new Disciplina("Estruturas de Dados");
            var eng1 = new Disciplina("Proc. de Soft. e Eng. de Requisitos");
            var arq = new Disciplina("Arq. e Organiz. de Computadores");
            var paa = new Disciplina("Projeto e Análise de Algoritmos");
            var poo = new Disciplina("Par.de Prog.Imp.e Orient.a Objetos");
            var ihc = new Disciplina("Interação ser humano computador");
            var ass = new Disciplina("Análise de Sistemas de Software");
            var bd1 = new Disciplina("Banco de Dados I");
            var progInterfaceHardSoft = new Disciplina("Progr. Interfaceamento de Hard e Soft");
            var computabilidade = new Disciplina("Computabilidade");
            var ord = new Disciplina("Organização e Recuperação de Dados");
            var grafos = new Disciplina("Algortimos em Grafos");
            var pss = new Disciplina("Projeto de Sistemas de Software");
            var matematicaComputacional = new Disciplina("Matemática Computacional");
            var so = new Disciplina("Sistemas Operacionais");
            var logica = new Disciplina("Paradigma Prog. Lógica e Funcional");
            var modelagem = new Disciplina("Modelagem e Otimização Algorítmica");
            var iss = new Disciplina("Implementação de Sistemas de Software");
            var informaticaESociedade = new Disciplina("Informática e Sociedade");
            var concorrente = new Disciplina("Programação Concorrentes");
            var aprendizagemMaquina = new Disciplina("Aprendizagem de Máquina");
            var estagio = new Disciplina("Estágio Docência");
            var gerenciamentoProjeto = new Disciplina("Gerenciamento de Projetos de Software");
            var metodosModelagem = new Disciplina("Métodos de Modelagem de Software");
            var pesquisaOperacional = new Disciplina("Pesquisa Operacional");
            var seminario = new Disciplina("Seminários I");
            var seminarios2 = new Disciplina("Seminários II");
            var tc = new Disciplina("Teoria da Computação");
            var sistemasComputacao = new Disciplina("Tópicos em Sistemas de Computacão I");
            var sistemasComputacao2 = new Disciplina("Tópicos em Sistemas de Computacão II");
            var sistemasComputacao3 = new Disciplina("Tópicos em Sistemas de Computacão III");
            var sistemaInformacao = new Disciplina("Tópicos em Sistemas de Informação I");
            var sistemaInformacao2 = new Disciplina("Tópicos em Sistemas de Informação II");
            var trabIndividual = new Disciplina("Trabalho Individual");
            var organizacaoComputadores = new Disciplina("Organização de Computadores e Sistemas Operacionais");
            var eng2 = new Disciplina("Engenharia de Software II");
            var qualidade = new Disciplina("Qualidade de Software");
            var projetoSoftware = new Disciplina("Projeto de Software");
            var fundProgramacao = new Disciplina("Fundamentos de Programação");
            var tecAvancadasSI = new Disciplina("Tecnologias Avançadas para Sistemas de Informação");
            var progSistemas = new Disciplina("Programação de Sistemas");
            var algoritmosEProg = new Disciplina("Algoritmos e Prog Comp");
            var assembly = new Disciplina("Programação em Ling de Montagem");
            var paradigmaImpOO = new Disciplina("Par.de Prog.Imp.e Orient.a Objetos");
            var infoAgro = new Disciplina("Informática Aplicada a Agronomia");
            var fundComputacao = new Disciplina("Fundamentos da Computação (MEC)");
            var secretariado = new Disciplina("Introdução à Informática Secreatiado");
            var labProgramacao = new Disciplina("Lab. Fund de Programação");

            //turmas.Add(new Turma(redes, new List<Horario>() { new Horario(Dia.Quarta, "09:40", "10:30"), new Horario(Dia.Quarta, "10:30", "11:20") }, "68", Semestre.Anual, 1023));
            turmas.Add(new Turma(tccCordenacao, new List<Horario>() { new Horario(Dia.Terça, "13:30", "14:20"), new Horario(Dia.Terça, "14:20", "15:10") }, "68", Semestre.Anual, "1029b"));
            //new Turma(automacao, new List<Horario>() { new Horario(Dia.Quarta, "15:30", "16:20"), new Horario(Dia.Quarta, "16:20", "17:10"), new Horario(Dia.Quarta, "17:10", "18:00") }, "102", Semestre.Anual, 1034));
            //new Turma(mf1, new List<Horario>() { new Horario(Dia.Segunda, "13:30", "14:20"), new Horario(Dia.Segunda, "14:20", "15:10"), new Horario(Dia.Terça, "15:30", "16:20"), new Horario(Dia.Terça, "16:20", "17:10") }, "68", Semestre.Semestre_2, 1744));
            turmas.Add(new Turma(teoriaDosGrafos, new List<Horario>() { new Horario(Dia.Segunda, "13:30", "14:20"), new Horario(Dia.Segunda, "14:20", "15:10"), new Horario(Dia.Terça, "15:30", "16:20"), new Horario(Dia.Terça, "16:20", "17:10"), new Horario(Dia.Quinta, "13:30", "14:20"), new Horario(Dia.Quinta, "14:20", "13:10") }, "102", Semestre.Semestre_1, 1747));
            turmas.Add(new Turma(mf2, new List<Horario>() { new Horario(Dia.Terça, "15:30", "16:20"), new Horario(Dia.Terça, "16:20", "17:10"), new Horario(Dia.Quarta, "07:45", "08:35"), new Horario(Dia.Quarta, "08:35", "09:25") }, "68", Semestre.Semestre_2, 1754));
            //new Turma(topicosPesquisa, new List<Horario>() { new Horario(Dia.Segunda, "13:30", "14:20"), new Horario(Dia.Segunda, "14:20", "15:10"), new Horario(Dia.Quarta, "13:30", "14:20"), new Horario(Dia.Quarta, "14:20", "15:10"), new Horario(Dia.Quinta, "09:40", "10:30"), new Horario(Dia.Quinta, "10:30", "11:20") }, "102", Semestre.Semestre_2, 1760));
            //turmas.Add(new Turma(programacaoLinear, new List<Horario>() { new Horario(Dia.Quinta, "07:45", "08:35"), new Horario(Dia.Quinta, "08:35", "09:25"), new Horario(Dia.Sexta, "07:45", "08:35"), new Horario(Dia.Sexta, "08:35", "09:25")}, "68", Semestre.Semestre_1, 1761));
            turmas.Add(new Turma(simulacaoSistemas, new List<Horario>() { new Horario(Dia.Terça, "13:30", "14:20"), new Horario(Dia.Terça, "14:20", "15:10"), new Horario(Dia.Quinta, "07:45", "08:35"), new Horario(Dia.Quinta, "08:35", "09:25") }, "68", Semestre.Semestre_2, 1762));
            turmas.Add(new Turma(arq, new List<Horario>() { new Horario(Dia.Quarta, "09:40", "10:30"), new Horario(Dia.Quarta, "10:30", "11:20"), new Horario(Dia.Quinta, "15:30", "16:20"), new Horario(Dia.Quinta, "16:20", "17:10"), new Horario(Dia.Sexta, "07:45", "08:35"), new Horario(Dia.Sexta, "08:35", "09:25") }, "102", Semestre.Semestre_2, "6887b"));
            turmas.Add(new Turma(fundamentosEletronica, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20"), new Horario(Dia.Terça, "07:45", "08:35"), new Horario(Dia.Terça, "08:35", "09:25"), new Horario(Dia.Sexta, "09:40", "10:30"), new Horario(Dia.Sexta, "10:30", "11:20") }, "102", Semestre.Semestre_1, "6872b"));
            turmas.Add(new Turma(fundamentosEletronica, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20"), new Horario(Dia.Terça, "07:45", "08:35"), new Horario(Dia.Terça, "08:35", "09:25"), new Horario(Dia.Sexta, "09:40", "10:30"), new Horario(Dia.Sexta, "10:30", "11:20") }, "102", Semestre.Semestre_1, "6872c"));
            turmas.Add(new Turma(ass, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20"), new Horario(Dia.Quinta, "09:40", "10:30"), new Horario(Dia.Quinta, "10:30", "11:20") }, "68", Semestre.Semestre_2, "6891b"));
            turmas.Add(new Turma(programacaoLinear, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20"), new Horario(Dia.Segunda, "11:20", "12:10"), new Horario(Dia.Quarta, "09:40", "10:30"), new Horario(Dia.Quarta, "10:30", "11:20"), new Horario(Dia.Quarta, "11:20", "12:10") }, "102", Semestre.Semestre_2, "6889b"));

            turmas.Add(new Turma(mf2, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00") }, "68", Semestre.Semestre_1, 1684));
            //turmas.Add(new Turma(eng4, new List<Horario>() { new Horario(Dia.Terça, "19:30", "20:20"), new Horario(Dia.Terça, "20:20", "21:10"), new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10"), new Horario(Dia.Quinta, "21:20", "22:10"), new Horario(Dia.Quinta, "22:10", "23:00") }, "136", Semestre.Semestre_1, 1837));
            turmas.Add(new Turma(eng4, new List<Horario>() { new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10"), new Horario(Dia.Quinta, "21:20", "22:10"), new Horario(Dia.Quinta, "22:10", "23:00") }, "136", Semestre.Semestre_1, "1837b"));
            turmas.Add(new Turma(ass, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00") }, "68", Semestre.Semestre_2, "5181b"));

            turmas.Add(new Turma(organizacaoComputadores, new List<Horario>() { new Horario(Dia.Sabado, "13:30", "14:20"), new Horario(Dia.Sabado, "14:20", "15:10"), new Horario(Dia.Sabado, "15:30", "16:20") }, "102", Semestre.Anual, 1452));
            turmas.Add(new Turma(sd, new List<Horario>() { new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00") }, "68", Semestre.Anual, 1487));
            turmas.Add(new Turma(eng2, new List<Horario>() { new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Anual, 1470));

            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Terça, "07:45", "08:35"), new Horario(Dia.Quarta, "07:45", "08:35"), new Horario(Dia.Quarta, "08:35", "09:25") }, "136", Semestre.Anual, 1590));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Terça, "08:35", "09:25"), new Horario(Dia.Quarta, "07:45", "08:35"), new Horario(Dia.Quarta, "08:35", "09:25") }, "136", Semestre.Anual, "1590b"));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10") }, "68", Semestre.Semestre_2, 1640));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10") }, "68", Semestre.Semestre_2, "1640b"));
            turmas.Add(new Turma(fundProgramacao, new List<Horario>() { new Horario(Dia.Quarta, "09:40", "10:30"), new Horario(Dia.Quarta, "10:30", "11:20"), new Horario(Dia.Quarta, "11:20", "12:10") }, "51", Semestre.Semestre_2, 2536));
            turmas.Add(new Turma(fundProgramacao, new List<Horario>() { new Horario(Dia.Quarta, "15:30", "16:20"), new Horario(Dia.Quarta, "16:20", "17:10"), new Horario(Dia.Quarta, "17:10", "18:00") }, "51", Semestre.Semestre_2, "2536b"));
            turmas.Add(new Turma(labProgramacao, new List<Horario>() { new Horario(Dia.Quarta, "13:30", "14:20") }, "17", Semestre.Semestre_2, 2541));
            turmas.Add(new Turma(labProgramacao, new List<Horario>() { new Horario(Dia.Quarta, "07:45", "08:35") }, "17", Semestre.Semestre_2, "2541b"));
            turmas.Add(new Turma(labProgramacao, new List<Horario>() { new Horario(Dia.Quinta, "13:30", "14:20") }, "17", Semestre.Semestre_2, "2541c"));
            turmas.Add(new Turma(labProgramacao, new List<Horario>() { new Horario(Dia.Quinta, "07:45", "08:35") }, "17", Semestre.Semestre_2, "2541d"));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Sexta, "09:40", "10:30"), new Horario(Dia.Sexta, "10:30", "11:20"), new Horario(Dia.Quarta, "15:30", "16:20") }, "68", Semestre.Anual, "208"));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Quarta, "16:20", "17:10"), new Horario(Dia.Sexta, "09:40", "10:30"), new Horario(Dia.Sexta, "10:30", "11:20") }, "68", Semestre.Anual, "208c"));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Quarta, "17:10", "18:00"), new Horario(Dia.Sexta, "09:40", "10:30"), new Horario(Dia.Sexta, "10:30", "11:20") }, "68", Semestre.Anual, "208d"));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Segunda, "15:30", "16:20"), new Horario(Dia.Segunda, "16:20", "17:10"), new Horario(Dia.Terça, "09:40", "10:30") }, "68", Semestre.Anual, "208d"));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Segunda, "15:30", "16:20"), new Horario(Dia.Segunda, "16:20", "17:10"), new Horario(Dia.Terça, "10:30", "11:20") }, "68", Semestre.Anual, "208e"));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Segunda, "15:30", "16:20"), new Horario(Dia.Segunda, "16:20", "17:10"), new Horario(Dia.Terça, "11:20", "12:10") }, "68", Semestre.Anual, "208f"));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Quarta, "09:40", "10:30"), new Horario(Dia.Quarta, "10:30", "11:20"), new Horario(Dia.Quarta, "11:20", "12:10") }, "68", Semestre.Anual, "1512b"));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Quarta, "08:35", "09:25"), new Horario(Dia.Quarta, "09:40", "10:30"), new Horario(Dia.Quarta, "10:30", "11:20") }, "68", Semestre.Anual, "1512c"));


            turmas.Add(new Turma(matematicaComputacional, new List<Horario>() { new Horario(Dia.Segunda, "15:30", "16:20"), new Horario(Dia.Segunda, "16:20", "17:10"), new Horario(Dia.Quinta, "13:30", "14:20"), new Horario(Dia.Quinta, "14:20", "15:10") }, "68", Semestre.Semestre_2, "6900b"));
            turmas.Add(new Turma(progInterfaceHardSoft, new List<Horario>() { new Horario(Dia.Terça, "13:30", "14:20"), new Horario(Dia.Terça, "14:20", "15:10"), new Horario(Dia.Sexta, "07:45", "08:35"), new Horario(Dia.Sexta, "08:35", "09:25") }, "68", Semestre.Semestre_1, "6894b"));
            turmas.Add(new Turma(estrutura, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Sabado, "13:30", "14:20"), new Horario(Dia.Sabado, "14:20", "15:10") }, "68", Semestre.Semestre_2, 7256));
            turmas.Add(new Turma(estrutura, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Sabado, "13:30", "14:20"), new Horario(Dia.Sabado, "14:20", "15:10") }, "68", Semestre.Semestre_2, "7256b"));
            turmas.Add(new Turma(estrutura, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Sabado, "13:30", "14:20"), new Horario(Dia.Sabado, "14:20", "15:10") }, "68", Semestre.Semestre_2, "7256c"));
            turmas.Add(new Turma(estrutura, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Sabado, "13:30", "14:20"), new Horario(Dia.Sabado, "14:20", "15:10") }, "68", Semestre.Semestre_2, "7256d"));
            turmas.Add(new Turma(bd1, new List<Horario>() { new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10"), new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00") }, "68", Semestre.Semestre_2, 7331));
            turmas.Add(new Turma(fundProgramacao, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00"), new Horario(Dia.Sabado, "13:30", "14:20"), new Horario(Dia.Sabado, "14:20", "15:10") }, "102", Semestre.Semestre_1, "7239b"));
            turmas.Add(new Turma(fundProgramacao, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00"), new Horario(Dia.Sabado, "13:30", "14:20"), new Horario(Dia.Sabado, "14:20", "15:10") }, "102", Semestre.Semestre_1, "7239c"));
            turmas.Add(new Turma(fundProgramacao, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00"), new Horario(Dia.Sabado, "13:30", "14:20"), new Horario(Dia.Sabado, "14:20", "15:10") }, "102", Semestre.Semestre_1, "7239d"));
            turmas.Add(new Turma(progSistemas, new List<Horario>() { new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00"), new Horario(Dia.Sabado, "15:30", "16:20"), new Horario(Dia.Sabado, "16:20", "17:10") }, "102", Semestre.Semestre_1, 7328));
            turmas.Add(new Turma(so, new List<Horario>() { new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10"), new Horario(Dia.Sabado, "13:30", "14:20"), new Horario(Dia.Sabado, "14:20", "15:10") }, "68", Semestre.Semestre_1, 7329));
            turmas.Add(new Turma(arq, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00") }, "68", Semestre.Semestre_1, "5178b"));
            turmas.Add(new Turma(ihc, new List<Horario>() { new Horario(Dia.Terça, "19:30", "20:20"), new Horario(Dia.Terça, "20:20", "21:10"), new Horario(Dia.Quinta, "21:20", "22:10"), new Horario(Dia.Quinta, "22:10", "23:00") }, "68", Semestre.Semestre_2, "5183b"));
            turmas.Add(new Turma(logica, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00") }, "68", Semestre.Semestre_1, "5200b"));
            turmas.Add(new Turma(assembly, new List<Horario>() { new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00") }, "68", Semestre.Semestre_2, "5193b"));
            turmas.Add(new Turma(concorrente, new List<Horario>() { new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00"), new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Sexta, "20:20", "21:10") }, "68", Semestre.Semestre_2, "5205b"));
            turmas.Add(new Turma(paa, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Semestre_2, "5184b"));
            turmas.Add(new Turma(paa, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Semestre_2, "5184c"));
            turmas.Add(new Turma(algoritmosEProg, new List<Horario>() { new Horario(Dia.Segunda, "13:30", "14:20"), new Horario(Dia.Segunda, "14:20", "15:10"), new Horario(Dia.Terça, "15:30", "16:20") }, "136", Semestre.Anual, 5259));
            turmas.Add(new Turma(algoritmosEProg, new List<Horario>() { new Horario(Dia.Segunda, "13:30", "14:20"), new Horario(Dia.Segunda, "14:20", "15:10"), new Horario(Dia.Terça, "16:20", "17:10") }, "136", Semestre.Anual, "5259b"));
            turmas.Add(new Turma(algoritmosEProg, new List<Horario>() { new Horario(Dia.Quarta, "13:30", "14:20"), new Horario(Dia.Quarta, "14:20", "15:10"), new Horario(Dia.Quarta, "15:30", "16:20") }, "136", Semestre.Anual, "5259c"));
            turmas.Add(new Turma(algoritmosEProg, new List<Horario>() { new Horario(Dia.Quarta, "13:30", "14:20"), new Horario(Dia.Quarta, "14:20", "15:10"), new Horario(Dia.Quarta, "16:20", "17:10") }, "136", Semestre.Anual, "5259d"));
            turmas.Add(new Turma(algoritmosEProg, new List<Horario>() { new Horario(Dia.Quinta, "15:30", "16:20"), new Horario(Dia.Quinta, "16:20", "17:10"), new Horario(Dia.Sexta, "15:30", "16:20") }, "136", Semestre.Anual, "5259e"));
            turmas.Add(new Turma(algoritmosEProg, new List<Horario>() { new Horario(Dia.Quinta, "15:30", "16:20"), new Horario(Dia.Quinta, "16:20", "17:10"), new Horario(Dia.Sexta, "16:20", "17:10") }, "136", Semestre.Anual, "5259f"));
            turmas.Add(new Turma(algoritmosEProg, new List<Horario>() { new Horario(Dia.Segunda, "15:30", "16:20"), new Horario(Dia.Segunda, "16:20", "17:10"), new Horario(Dia.Quarta, "15:30", "16:20"), new Horario(Dia.Quarta, "16:20", "17:10"), new Horario(Dia.Quinta, "15:30", "16:20"), new Horario(Dia.Quinta, "16:20", "17:10") }, "102", Semestre.Semestre_1, 4549));
            turmas.Add(new Turma(algoritmosEProg, new List<Horario>() { new Horario(Dia.Segunda, "15:30", "16:20"), new Horario(Dia.Segunda, "16:20", "17:10"), new Horario(Dia.Quarta, "13:30", "14:20"), new Horario(Dia.Quarta, "14:20", "15:10"), new Horario(Dia.Quarta, "15:30", "16:20"), new Horario(Dia.Quarta, "16:20", "17:10") }, "102", Semestre.Semestre_1, "4549b"));
            turmas.Add(new Turma(bd1, new List<Horario>() { new Horario(Dia.Segunda, "15:30", "16:20"), new Horario(Dia.Segunda, "16:20", "17:10"), new Horario(Dia.Quarta, "13:30", "14:20"), new Horario(Dia.Quarta, "14:20", "15:10"), new Horario(Dia.Quarta, "15:30", "16:20"), new Horario(Dia.Quarta, "16:20", "17:10") }, "68", Semestre.Semestre_2, 4573));
            turmas.Add(new Turma(estrutura, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00"), new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Semestre_2, 4553));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00"), new Horario(Dia.Sabado, "13:30", "14:20"), new Horario(Dia.Sabado, "14:20", "15:10") }, "102", Semestre.Anual, 4309));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00"), new Horario(Dia.Sabado, "13:30", "14:20"), new Horario(Dia.Sabado, "14:20", "15:10") }, "102", Semestre.Anual, "4309b"));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00"), new Horario(Dia.Sabado, "13:30", "14:20"), new Horario(Dia.Sabado, "14:20", "15:10") }, "102", Semestre.Anual, "4309c"));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Segunda, "15:30", "16:20"), new Horario(Dia.Segunda, "16:20", "17:10"), new Horario(Dia.Segunda, "17:10", "18:00") }, "51", Semestre.Semestre_2, "2536c"));
            turmas.Add(new Turma(fundProgramacao, new List<Horario>() { new Horario(Dia.Sexta, "09:40", "10:30"), new Horario(Dia.Sexta, "10:30", "11:20"), new Horario(Dia.Sexta, "11:20", "12:10") }, "51", Semestre.Semestre_1, 6633));
            turmas.Add(new Turma(fundProgramacao, new List<Horario>() { new Horario(Dia.Quinta, "15:30", "16:20"), new Horario(Dia.Quinta, "16:20", "17:10"), new Horario(Dia.Quinta, "17:10", "18:00") }, "51", Semestre.Semestre_1, "6633b"));
            turmas.Add(new Turma(infoAgro, new List<Horario>() { new Horario(Dia.Terça, "13:30", "14:20"), new Horario(Dia.Terça, "14:20", "15:10"), new Horario(Dia.Quinta, "13:30", "14:20"), new Horario(Dia.Quinta, "14:20", "15:10") }, "68", Semestre.Semestre_2, 4460));
            turmas.Add(new Turma(infoAgro, new List<Horario>() { new Horario(Dia.Terça, "13:30", "14:20"), new Horario(Dia.Terça, "14:20", "15:10"), new Horario(Dia.Quinta, "13:30", "14:20"), new Horario(Dia.Quinta, "14:20", "15:10") }, "68", Semestre.Semestre_2, "4460b"));
            turmas.Add(new Turma(infoAgro, new List<Horario>() { new Horario(Dia.Terça, "13:30", "14:20"), new Horario(Dia.Terça, "14:20", "15:10"), new Horario(Dia.Quinta, "13:30", "14:20"), new Horario(Dia.Quinta, "14:20", "15:10") }, "68", Semestre.Semestre_2, "4460c"));
            turmas.Add(new Turma(infoAgro, new List<Horario>() { new Horario(Dia.Terça, "13:30", "14:20"), new Horario(Dia.Terça, "14:20", "15:10"), new Horario(Dia.Quinta, "13:30", "14:20"), new Horario(Dia.Quinta, "14:20", "15:10") }, "68", Semestre.Semestre_2, "4460d"));
            turmas.Add(new Turma(secretariado, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00") }, "68", Semestre.Semestre_2, 3411));
            turmas.Add(new Turma(secretariado, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00") }, "68", Semestre.Semestre_2, "3411b"));
            turmas.Add(new Turma(cd2, new List<Horario>() { new Horario(Dia.Segunda, "07:45", "08:35"), new Horario(Dia.Segunda, "08:35", "09:25"), new Horario(Dia.Quarta, "07:45", "08:35"), new Horario(Dia.Quarta, "08:35", "09:25") }, "68", Semestre.Semestre_1, "6882c"));
            turmas.Add(new Turma(cd2, new List<Horario>() { new Horario(Dia.Segunda, "07:45", "08:35"), new Horario(Dia.Segunda, "08:35", "09:25"), new Horario(Dia.Quarta, "07:45", "08:35"), new Horario(Dia.Quarta, "08:35", "09:25") }, "68", Semestre.Semestre_1, "6882d"));
            turmas.Add(new Turma(pss, new List<Horario>() { new Horario(Dia.Terça, "07:45", "08:35"), new Horario(Dia.Terça, "08:35", "09:25"), new Horario(Dia.Quinta, "13:30", "14:20"), new Horario(Dia.Quinta, "14:20", "15:10") }, "68", Semestre.Semestre_1, 6885));
            turmas.Add(new Turma(pss, new List<Horario>() { new Horario(Dia.Terça, "07:45", "08:35"), new Horario(Dia.Terça, "08:35", "09:25"), new Horario(Dia.Quinta, "13:30", "14:20"), new Horario(Dia.Quinta, "14:20", "15:10") }, "68", Semestre.Semestre_1, "6885b"));
            turmas.Add(new Turma(cd1, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Semestre_2, "5174d"));


            #region Professores

            var constantino = new Professor(new List<Turma>(getTurmasDadoDisciplina(turmas, new List<Disciplina>() { modelagem, metodosModelagem })), new List<Turma>(getTurmasDadoDisciplina(turmas, new List<Disciplina>() { })), "Ademir Aparecido Constantino", new List<Turma>(), "8");
            var carniel = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Ademir Carniel", new List<Turma>(), "8");
            var anderson = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Anderson Faustino da Silva", new List<Turma>(), "8");
            var airton = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Airton Marco Polidório", new List<Turma>(), "8");
            var sica = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Carlos Benedito Sica de Toledo", new List<Turma>(), "8");
            var clelia = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Clélia Franco", new List<Turma>(), "8");
            var dante = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Dante Alves Medeiros Filho", new List<Turma>(), "8");
            var donizete = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Donizete Carlos Bruzarosco", new List<Turma>(), "8");
            var elisa = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Elisa Hatsue Moriya Huzita", new List<Turma>(), "8");
            var elvio = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Elvio João Leonardo", new List<Turma>(), "8");
            var flavio = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Flávio Arnaldo Braga da Silva", new List<Turma>(), "8");
            var itana = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Itana Maria de Souza Gimenes", new List<Turma>(), "8");
            var angelo = new Professor(new List<Turma>() { }, new List<Turma>() { }, "João Angelo Martini", new List<Turma>(), "8");
            var ze = new Professor(new List<Turma>() { }, new List<Turma>() { }, "José Roberto Vasconcelos", new List<Turma>(), "8");
            var josiane = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Josiane Melchiori Pinheiro", new List<Turma>(), "8");
            var jucelia = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Jucélia Geni Pereira Fregoneis", new List<Turma>(), "8");
            var linnyer = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Linnyer Beatrys Ruiz", new List<Turma>(), "8");
            var luciana = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Luciana Andréia Fondazzi Martimiano", new List<Turma>(), "8");
            var wigarashi = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Wagner Igarashi", new List<Turma>(), "8");
            var malbarbo = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Marco Aurélio Lopes Barbosa", new List<Turma>(), "8");
            var madalena = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Maria Madalena Dias", new List<Turma>(), "8");
            var yandre = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Yandre Maldonado e Gomes da Costa", new List<Turma>(), "8");
            var valeria = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Valéria Delisandra Feltrim", new List<Turma>(), "8");
            var thelma = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Thelma Elita Colanzi Lopes", new List<Turma>(), "8");
            var raqueline = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Raqueline Ritter de Moura", new List<Turma>(), "8");
            var ronaldo = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Ronaldo Augusto de Lara Gonçalves", new List<Turma>(), "8");
            var sergio = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Sérgio Roberto P. da Silva", new List<Turma>(), "8");
            var tania = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Tânia Fátima Calvi Tait", new List<Turma>(), "8");
            var wesley = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Wesley Romão", new List<Turma>(), "8");
            var andre = new Professor(new List<Turma>() { }, new List<Turma>() { }, "André Barbosa Verona", new List<Turma>(), "12");
            var franklin = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Franklin César Flores", new List<Turma>(), "8");
            var leandro = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Leandro Magno", new List<Turma>(), "16");
            var priscila = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Priscila Aparecida de Moraes", new List<Turma>(), "16");
            var nardenio = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Nardênio Almeida Martins", new List<Turma>(), "8");
            var uber = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Flávio Uber", new List<Turma>(), "8");
            var heloise = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Heloise  Manica", new List<Turma>(), "16");
            var munif = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Munif Gebara Júnior", new List<Turma>(), "16");
            var balancieri = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Renato Balancieri", new List<Turma>(), "8");
            var juliana = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Juliana Keiko Yamaguchi", new List<Turma>(), "12");
            var rodrigo = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Rodrigo Lankaites Pinheiro", new List<Turma>(), "16");
            var edson = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Edson Alves de Oliveira Júniuor", new List<Turma>(), "8");
            var carlosFransley = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Carlos Fransley", new List<Turma>(), "8");

            professores.Add(constantino);
            professores.Add(carniel);
            professores.Add(anderson);
            professores.Add(airton);
            professores.Add(sica);
            professores.Add(clelia);
            professores.Add(andre);
            professores.Add(tania);
            professores.Add(dante);
            professores.Add(donizete);
            professores.Add(elisa);
            professores.Add(elvio);
            professores.Add(flavio);
            professores.Add(itana);
            professores.Add(angelo);
            professores.Add(ze);
            professores.Add(josiane);
            professores.Add(jucelia);
            professores.Add(linnyer);
            professores.Add(luciana);
            professores.Add(wigarashi);
            professores.Add(malbarbo);
            professores.Add(madalena);
            professores.Add(yandre);
            professores.Add(valeria);
            professores.Add(thelma);
            professores.Add(raqueline);
            professores.Add(ronaldo);
            professores.Add(sergio);
            professores.Add(wesley);
            professores.Add(franklin);
            professores.Add(nardenio);
            professores.Add(uber);
            professores.Add(heloise);
            professores.Add(munif);
            professores.Add(balancieri);
            professores.Add(juliana);
            professores.Add(rodrigo);
            professores.Add(edson);
            professores.Add(carlosFransley);
            professores.Add(priscila);
            professores.Add(leandro);
            #endregion

            #region Turmas Consolidadas
            turmas.Add(new Turma(modelagem, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20"), new Horario(Dia.Terça, "09:40", "10:30"), new Horario(Dia.Terça, "10:30", "11:20"), new Horario(Dia.Sexta, "07:45", "08:35"), new Horario(Dia.Sexta, "08:45", "09:25") }, "102", Semestre.Semestre_2, 6903));
            turmas.Add(new Turma(topicosPesquisa, new List<Horario>() { new Horario(Dia.Segunda, "13:30", "14:20"), new Horario(Dia.Segunda, "14:20", "15:10"), new Horario(Dia.Quarta, "13:30", "14:20"), new Horario(Dia.Quarta, "14:20", "15:10"), new Horario(Dia.Quinta, "09:40", "10:30"), new Horario(Dia.Quinta, "10:30", "11:20") }, "102", Semestre.Semestre_2, 1760));
            turmas.Add(new Turma(modelagem, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00") }, "102", Semestre.Semestre_2, 5199));

            turmas.Add(new Turma(matematicaComputacional, new List<Horario>() { new Horario(Dia.Segunda, "15:30", "16:20"), new Horario(Dia.Segunda, "16:20", "17:10"), new Horario(Dia.Quinta, "13:30", "14:20"), new Horario(Dia.Quinta, "14:20", "15:10") }, "68", Semestre.Semestre_2, 6900));
            turmas.Add(new Turma(mf1, new List<Horario>() { new Horario(Dia.Terça, "15:30", "16:20"), new Horario(Dia.Terça, "16:20", "17:10"), new Horario(Dia.Quinta, "15:30", "16:20"), new Horario(Dia.Quinta, "16:20", "17:10") }, "68", Semestre.Semestre_2, 1744));
            turmas.Add(new Turma(algoritmos, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00"), new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Sexta, "20:20", "21:10") }, "102", Semestre.Semestre_2, 5175));
            turmas.Add(new Turma(mf1, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Sexta, "20:20", "21:10") }, "68", Semestre.Semestre_1, 5237));

            turmas.Add(new Turma(arq, new List<Horario>() { new Horario(Dia.Segunda, "07:45", "08:35"), new Horario(Dia.Segunda, "08:35", "09:25"), new Horario(Dia.Quinta, "15:30", "16:20"), new Horario(Dia.Quinta, "16:20", "17:10"), new Horario(Dia.Sexta, "07:45", "08:35"), new Horario(Dia.Sexta, "08:35", "09:25") }, "102", Semestre.Semestre_2, 6887));
            turmas.Add(new Turma(arq2, new List<Horario>() { new Horario(Dia.Terça, "07:45", "08:35"), new Horario(Dia.Terça, "08:35", "09:25"), new Horario(Dia.Quinta, "07:45", "08:35"), new Horario(Dia.Quinta, "08:35", "09:25") }, "68", Semestre.Semestre_1, 6895));
            turmas.Add(new Turma(compiladores, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Semestre_2, 5204));
            turmas.Add(new Turma(concorrente, new List<Horario>() { new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00"), new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Sexta, "20:20", "21:10") }, "68", Semestre.Semestre_2, 5205));

            turmas.Add(new Turma(automacao, new List<Horario>() { new Horario(Dia.Quarta, "09:40", "10:30"), new Horario(Dia.Quarta, "10:30", "11:20"), new Horario(Dia.Quarta, "11:20", "12:10") }, "102", Semestre.Anual, 1034));
            turmas.Add(new Turma(cd2, new List<Horario>() { new Horario(Dia.Segunda, "07:45", "08:35"), new Horario(Dia.Segunda, "08:35", "09:25"), new Horario(Dia.Quarta, "07:45", "08:35"), new Horario(Dia.Quarta, "08:35", "09:25") }, "68", Semestre.Semestre_1, 6882));
            turmas.Add(new Turma(fundamentosEletronica, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20"), new Horario(Dia.Terça, "07:45", "08:35"), new Horario(Dia.Terça, "08:35", "09:25"), new Horario(Dia.Sexta, "09:40", "10:30"), new Horario(Dia.Sexta, "10:30", "11:20") }, "102", Semestre.Semestre_1, 6872));
            turmas.Add(new Turma(arq, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Sabado, "15:30", "16:20"), new Horario(Dia.Sabado, "16:20", "17:10") }, "68", Semestre.Semestre_2, 7330));
            turmas.Add(new Turma(arq, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00") }, "68", Semestre.Semestre_1, 5178));

            turmas.Add(new Turma(fundamentosEletronica, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20"), new Horario(Dia.Terça, "07:45", "08:35"), new Horario(Dia.Terça, "08:35", "09:25"), new Horario(Dia.Sexta, "09:40", "10:30"), new Horario(Dia.Sexta, "10:30", "11:20") }, "102", Semestre.Semestre_1, "6872d"));
            turmas.Add(new Turma(simulacaoSistemas, new List<Horario>() { new Horario(Dia.Terça, "13:30", "14:20"), new Horario(Dia.Terça, "14:20", "15:10"), new Horario(Dia.Quinta, "07:45", "08:35"), new Horario(Dia.Quinta, "08:35", "09:25") }, "68", Semestre.Semestre_2, 1762));
            turmas.Add(new Turma(sd, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Sexta, "20:20", "21:10") }, "68", Semestre.Semestre_1, 5197));

            turmas.Add(new Turma(automacao, new List<Horario>() { new Horario(Dia.Quarta, "15:30", "16:20"), new Horario(Dia.Quarta, "16:20", "17:10"), new Horario(Dia.Quarta, "17:10", "18:00") }, "102", Semestre.Anual, 1034));
            turmas.Add(new Turma(ia, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Sexta, "20:20", "21:10") }, "68", Semestre.Semestre_2, "5236b"));

            turmas.Add(new Turma(fundProgramacao, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00"), new Horario(Dia.Sabado, "13:30", "14:20"), new Horario(Dia.Sabado, "14:20", "15:10") }, "102", Semestre.Semestre_1, 7239));

            turmas.Add(new Turma(cg, new List<Horario>() { new Horario(Dia.Quinta, "09:40", "10:30"), new Horario(Dia.Quinta, "10:30", "11:20"), new Horario(Dia.Quinta, "11:20", "12:10"), new Horario(Dia.Sexta, "09:40", "10:30"), new Horario(Dia.Sexta, "10:30", "11:20"), new Horario(Dia.Sexta, "11:20", "12:10") }, "102", Semestre.Semestre_1, 1759));
            turmas.Add(new Turma(cg, new List<Horario>() { new Horario(Dia.Terça, "19:30", "20:20"), new Horario(Dia.Terça, "20:20", "21:10"), new Horario(Dia.Quinta, "21:20", "22:10"), new Horario(Dia.Quinta, "22:10", "23:00") }, "68", Semestre.Semestre_2, 5202));
            turmas.Add(new Turma(pesquisa, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Semestre_1, 5171));

            turmas.Add(new Turma(iss, new List<Horario>() { new Horario(Dia.Segunda, "07:45", "08:35"), new Horario(Dia.Segunda, "08:35", "09:25"), new Horario(Dia.Quinta, "09:40", "10:30"), new Horario(Dia.Quinta, "10:30", "11:20") }, "68", Semestre.Semestre_2, 6904));
            turmas.Add(new Turma(pss, new List<Horario>() { new Horario(Dia.Segunda, "07:45", "08:35"), new Horario(Dia.Segunda, "08:35", "09:25"), new Horario(Dia.Quinta, "09:40", "10:30"), new Horario(Dia.Quinta, "10:30", "11:20") }, "68", Semestre.Semestre_1, 6899));
            turmas.Add(new Turma(iss, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10") }, "68", Semestre.Semestre_2, 5191));
            turmas.Add(new Turma(iss, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00") }, "68", Semestre.Semestre_1, 5186));
            turmas.Add(new Turma(tcc, new List<Horario>() { new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00") }, "68", Semestre.Anual, 3513));

            turmas.Add(new Turma(ambienteDesenv, new List<Horario>() { new Horario(Dia.Terça, "13:30", "14:20"), new Horario(Dia.Terça, "14:20", "15:10"), new Horario(Dia.Quinta, "15:30", "16:20"), new Horario(Dia.Quinta, "16:20", "17:10") }, "68", Semestre.Semestre_2, 1753));
            turmas.Add(new Turma(progSistWeb, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Sexta, "20:20", "21:10") }, "68", Semestre.Semestre_1, 5188));
            turmas.Add(new Turma(poo, new List<Horario>() { new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "22:20", "21:10"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00") }, "68", Semestre.Semestre_1, 5190));
            turmas.Add(new Turma(ass, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20"), new Horario(Dia.Quinta, "09:40", "10:30"), new Horario(Dia.Quinta, "10:30", "11:20") }, "68", Semestre.Semestre_2, 6891));

            turmas.Add(new Turma(ti, new List<Horario>() { new Horario(Dia.Segunda, "13:30", "14:20"), new Horario(Dia.Segunda, "14:20", "15:10"), new Horario(Dia.Quinta, "09:40", "10:30"), new Horario(Dia.Quinta, "10:30", "11:20") }, "68", Semestre.Semestre_1, 6874));
            turmas.Add(new Turma(projetoSoftware, new List<Horario>() { new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Sexta, "20:20", "21:10") }, "68", Semestre.Anual, 1498));
            turmas.Add(new Turma(pss, new List<Horario>() { new Horario(Dia.Terça, "19:30", "20:20"), new Horario(Dia.Terça, "20:20", "21:10"), new Horario(Dia.Quinta, "21:20", "22:10"), new Horario(Dia.Quinta, "22:10", "23:00") }, "68", Semestre.Semestre_1, 5176));

            turmas.Add(new Turma(cd1, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20"), new Horario(Dia.Terça, "07:45", "08:35"), new Horario(Dia.Terça, "08:35", "09:25"), new Horario(Dia.Sexta, "09:40", "10:30"), new Horario(Dia.Sexta, "10:30", "11:20") }, "102", Semestre.Semestre_2, 6878));
            turmas.Add(new Turma(redes, new List<Horario>() { new Horario(Dia.Quarta, "09:40", "10:30"), new Horario(Dia.Quarta, "10:30", "11:20") }, "68", Semestre.Anual, 1023));
            turmas.Add(new Turma(cd1, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Semestre_2, 5174));
            turmas.Add(new Turma(redes, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10") }, "68", Semestre.Semestre_2, 5203));

            turmas.Add(new Turma(so, new List<Horario>() { new Horario(Dia.Segunda, "13:30", "14:20"), new Horario(Dia.Segunda, "14:20", "15:10"), new Horario(Dia.Quarta, "07:45", "08:35"), new Horario(Dia.Quarta, "08:35", "09:25"), new Horario(Dia.Sexta, "09:40", "10:30"), new Horario(Dia.Sexta, "10:30", "11:20") }, "102", Semestre.Semestre_2, 6901));
            turmas.Add(new Turma(so, new List<Horario>() { new Horario(Dia.Terça, "19:30", "20:20"), new Horario(Dia.Terça, "20:20", "21:10"), new Horario(Dia.Quinta, "21:20", "22:10"), new Horario(Dia.Quinta, "22:10", "23:00") }, "68", Semestre.Semestre_1, 5196));


            turmas.Add(new Turma(arq2, new List<Horario>() { new Horario(Dia.Terça, "13:30", "14:20"), new Horario(Dia.Terça, "14:20", "15:10"), new Horario(Dia.Quinta, "09:40", "10:30"), new Horario(Dia.Quinta, "10:30", "11:20") }, "68", Semestre.Semestre_1, 1757));
            turmas.Add(new Turma(mf2, new List<Horario>() { new Horario(Dia.Segunda, "13:30", "14:20"), new Horario(Dia.Segunda, "14:20", "15:10"), new Horario(Dia.Quarta, "15:30", "16:20"), new Horario(Dia.Quarta, "16:20", "17:10") }, "68", Semestre.Semestre_2, 1754));
            turmas.Add(new Turma(algoritmos, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00"), new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Sexta, "20:20", "21:10") }, "102", Semestre.Semestre_2, "5175b"));
            turmas.Add(new Turma(mf2, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00") }, "68", Semestre.Semestre_1, 1754));

            turmas.Add(new Turma(estrutura, new List<Horario>() { new Horario(Dia.Segunda, "13:30", "14:20"), new Horario(Dia.Segunda, "14:20", "15:10"), new Horario(Dia.Terça, "09:40", "10:30"), new Horario(Dia.Terça, "10:30", "11:20"), new Horario(Dia.Sexta, "07:45", "08:35"), new Horario(Dia.Sexta, "08:35", "09:25") }, "102", Semestre.Semestre_1, 6884));
            turmas.Add(new Turma(algoritmos, new List<Horario>() { new Horario(Dia.Segunda, "07:45", "08:35"), new Horario(Dia.Segunda, "08:35", "09:25"), new Horario(Dia.Terça, "15:30", "16:20"), new Horario(Dia.Terça, "16:20", "17:10"), new Horario(Dia.Quinta, "07:45", "08:35"), new Horario(Dia.Quinta, "08:35", "09:25") }, "102", Semestre.Semestre_2, 6879));
            turmas.Add(new Turma(estrutura, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00") }, "102", Semestre.Semestre_1, 5180));

            turmas.Add(new Turma(pesquisa, new List<Horario>() { new Horario(Dia.Terça, "15:30", "16:20"), new Horario(Dia.Terça, "16:20", "17:10") }, "34", Semestre.Semestre_1, 6875));
            turmas.Add(new Turma(pesquisa, new List<Horario>() { new Horario(Dia.Quinta, "15:30", "16:20"), new Horario(Dia.Quinta, "16:20", "17:10") }, "34", Semestre.Semestre_1, "6875b"));
            turmas.Add(new Turma(bd1, new List<Horario>() { new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00") }, "68", Semestre.Semestre_2, 5182));
            turmas.Add(new Turma(bd2, new List<Horario>() { new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00"), new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Sexta, "20:20", "21:10"), }, "68", Semestre.Semestre_2, 5192));
            turmas.Add(new Turma(pesquisa, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "22:20", "21:10") }, "68", Semestre.Semestre_1, "5171b"));

            turmas.Add(new Turma(pesquisa, new List<Horario>() { new Horario(Dia.Terça, "07:45", "08:35"), new Horario(Dia.Terça, "08:35", "09:25") }, "68", Semestre.Anual, 1029));
            turmas.Add(new Turma(ass, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00") }, "68", Semestre.Semestre_2, 5181));
            turmas.Add(new Turma(eng4, new List<Horario>() { new Horario(Dia.Terça, "19:30", "20:20"), new Horario(Dia.Terça, "20:20", "21:10"), new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10"), new Horario(Dia.Quinta, "21:20", "22:10"), new Horario(Dia.Quinta, "22:10", "23:00") }, "136", Semestre.Semestre_1, 1837));

            turmas.Add(new Turma(cd1, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20"), new Horario(Dia.Terça, "07:45", "08:35"), new Horario(Dia.Terça, "08:35", "09:25"), new Horario(Dia.Sexta, "09:40", "10:30"), new Horario(Dia.Sexta, "10:30", "11:20") }, "102", Semestre.Semestre_2, "6878b"));
            turmas.Add(new Turma(cd1, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Semestre_2, "5174b"));

            turmas.Add(new Turma(computabilidade, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20"), new Horario(Dia.Terça, "09:40", "10:30"), new Horario(Dia.Terça, "10:30", "11:20") }, "68", Semestre.Semestre_1, 6896));
            turmas.Add(new Turma(programacaoLinear, new List<Horario>() { new Horario(Dia.Quinta, "07:45", "08:35"), new Horario(Dia.Quinta, "08:35", "09:25"), new Horario(Dia.Sexta, "07:45", "08:35"), new Horario(Dia.Sexta, "08:35", "09:25") }, "68", Semestre.Semestre_1, 1761));
            turmas.Add(new Turma(paa, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20"), new Horario(Dia.Segunda, "11:20", "12:10"), new Horario(Dia.Quarta, "09:40", "10:30"), new Horario(Dia.Quarta, "10:30", "11:20"), new Horario(Dia.Quarta, "11:20", "12:10") }, "102", Semestre.Semestre_2, 6889));
            turmas.Add(new Turma(paa, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Semestre_2, 5184));

            turmas.Add(new Turma(paradigmaImpOO, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Semestre_2, "5185b"));
            turmas.Add(new Turma(pss, new List<Horario>() { new Horario(Dia.Terça, "19:30", "20:20"), new Horario(Dia.Terça, "20:20", "21:10"), new Horario(Dia.Quinta, "21:20", "22:10"), new Horario(Dia.Quinta, "22:10", "23:00") }, "68", Semestre.Semestre_1, "5176b"));

            turmas.Add(new Turma(computabilidade, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20"), new Horario(Dia.Terça, "09:40", "10:30"), new Horario(Dia.Terça, "10:30", "11:20") }, "68", Semestre.Semestre_1, "6896b"));
            turmas.Add(new Turma(lingFormaisAutomatos, new List<Horario>() { new Horario(Dia.Segunda, "15:30", "16:20"), new Horario(Dia.Segunda, "16:20", "17:10"), new Horario(Dia.Segunda, "17:10", "18:00"), new Horario(Dia.Terça, "15:30", "16:20"), new Horario(Dia.Terça, "16:20", "17:10"), new Horario(Dia.Terça, "17:10", "18:00") }, "102", Semestre.Semestre_1, 6883));
            turmas.Add(new Turma(lingFormaisAutomatos, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Terça, "19:30", "20:20"), new Horario(Dia.Terça, "20:20", "21:10"), new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00") }, "102", Semestre.Semestre_1, 5195));

            turmas.Add(new Turma(redes, new List<Horario>() { new Horario(Dia.Quarta, "09:40", "10:30"), new Horario(Dia.Quarta, "10:30", "11:20") }, "68", Semestre.Anual, "1023b"));
            turmas.Add(new Turma(sd, new List<Horario>() { new Horario(Dia.Terça, "15:30", "16:20"), new Horario(Dia.Terça, "16:20", "17:10"), new Horario(Dia.Terça, "17:10", "18:00"), new Horario(Dia.Quinta, "15:30", "16:20"), new Horario(Dia.Quinta, "16:20", "17:10"), new Horario(Dia.Quinta, "17:10", "18:00") }, "102", Semestre.Semestre_1, 1756));
            turmas.Add(new Turma(redes, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10") }, "68", Semestre.Semestre_2, "5203b"));
            turmas.Add(new Turma(sd, new List<Horario>() { new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10"), new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Sexta, "20:20", "21:10") }, "68", Semestre.Semestre_1, 1838));

            turmas.Add(new Turma(grafos, new List<Horario>() { new Horario(Dia.Quarta, "09:40", "10:30"), new Horario(Dia.Quarta, "10:30", "11:20"), new Horario(Dia.Sexta, "09:40", "10:30"), new Horario(Dia.Sexta, "10:30", "11:20") }, "68", Semestre.Semestre_1, 6898));
            turmas.Add(new Turma(lp, new List<Horario>() { new Horario(Dia.Quinta, "13:30", "14:20"), new Horario(Dia.Quinta, "14:20", "15:10") }, "68", Semestre.Anual, 1028));
            turmas.Add(new Turma(logica, new List<Horario>() { new Horario(Dia.Terça, "07:45", "08:35"), new Horario(Dia.Terça, "08:35", "09:25"), new Horario(Dia.Quinta, "07:45", "08:35"), new Horario(Dia.Quinta, "08:35", "09:25") }, "68", Semestre.Semestre_2, 6902));
            turmas.Add(new Turma(grafos, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10") }, "68", Semestre.Semestre_1, 5189));
            turmas.Add(new Turma(logica, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00") }, "68", Semestre.Semestre_1, 5200));

            turmas.Add(new Turma(bd1, new List<Horario>() { new Horario(Dia.Terça, "09:40", "10:30"), new Horario(Dia.Terça, "10:30", "11:20"), new Horario(Dia.Quinta, "07:45", "08:35"), new Horario(Dia.Quinta, "08:35", "09:25") }, "68", Semestre.Semestre_2, 6892));
            turmas.Add(new Turma(bd2, new List<Horario>() { new Horario(Dia.Terça, "09:40", "10:30"), new Horario(Dia.Terça, "10:30", "11:20"), new Horario(Dia.Terça, "11:20", "12:10"), new Horario(Dia.Quinta, "15:30", "16:20"), new Horario(Dia.Quinta, "16:20", "17:10"), new Horario(Dia.Quinta, "17:10", "18:00") }, "102", Semestre.Semestre_2, 1752));
            turmas.Add(new Turma(ti, new List<Horario>() { new Horario(Dia.Segunda, "13:30", "14:20"), new Horario(Dia.Segunda, "14:20", "15:10"), new Horario(Dia.Quinta, "09:40", "10:30"), new Horario(Dia.Quinta, "10:30", "11:20") }, "68", Semestre.Semestre_1, "6874b"));
            turmas.Add(new Turma(ti, new List<Horario>() { new Horario(Dia.Terça, "19:30", "20:20"), new Horario(Dia.Terça, "20:20", "21:10"), new Horario(Dia.Quinta, "21:20", "22:10"), new Horario(Dia.Quinta, "22:10", "23:00") }, "68", Semestre.Semestre_1, 5167));


            turmas.Add(new Turma(bd1, new List<Horario>() { new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10"), new Horario(Dia.Quarta, "21:20", "22:10") }, "102", Semestre.Anual, 2495));

            turmas.Add(new Turma(cd1, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20"), new Horario(Dia.Terça, "07:45", "08:35"), new Horario(Dia.Terça, "08:35", "09:25"), new Horario(Dia.Sexta, "09:40", "10:30"), new Horario(Dia.Sexta, "10:30", "11:20") }, "102", Semestre.Semestre_2, "6878c"));
            turmas.Add(new Turma(cd2, new List<Horario>() { new Horario(Dia.Segunda, "07:45", "08:35"), new Horario(Dia.Segunda, "08:35", "09:25"), new Horario(Dia.Quarta, "07:45", "08:35"), new Horario(Dia.Quarta, "08:35", "09:25") }, "68", Semestre.Semestre_1, "6882b"));
            turmas.Add(new Turma(cd1, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Semestre_2, "5174c"));

            turmas.Add(new Turma(bd1, new List<Horario>() { new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00") }, "68", Semestre.Semestre_2, "5182b"));
            turmas.Add(new Turma(ti, new List<Horario>() { new Horario(Dia.Terça, "19:30", "20:20"), new Horario(Dia.Terça, "20:20", "21:10"), new Horario(Dia.Quinta, "21:20", "22:10"), new Horario(Dia.Quinta, "22:10", "23:00") }, "68", Semestre.Semestre_1, "5167b"));

            turmas.Add(new Turma(algoritmos, new List<Horario>() { new Horario(Dia.Segunda, "07:45", "08:35"), new Horario(Dia.Segunda, "08:35", "09:25"), new Horario(Dia.Terça, "15:30", "16:20"), new Horario(Dia.Terça, "16:20", "17:10"), new Horario(Dia.Quinta, "07:45", "08:35"), new Horario(Dia.Quinta, "08:35", "09:25") }, "102", Semestre.Semestre_2, "6879b"));
            turmas.Add(new Turma(progInterfaceHardSoft, new List<Horario>() { new Horario(Dia.Terça, "13:30", "14:20"), new Horario(Dia.Terça, "14:20", "15:10"), new Horario(Dia.Sexta, "07:45", "08:35"), new Horario(Dia.Sexta, "08:35", "09:25") }, "68", Semestre.Semestre_1, 6894));
            turmas.Add(new Turma(estrutura, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00") }, "102", Semestre.Semestre_1, "5180b"));
            turmas.Add(new Turma(assembly, new List<Horario>() { new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00") }, "68", Semestre.Semestre_2, 5193));

            turmas.Add(new Turma(ia, new List<Horario>() { new Horario(Dia.Segunda, "15:30", "16:20"), new Horario(Dia.Segunda, "16:20", "17:10"), new Horario(Dia.Quarta, "13:30", "14:20"), new Horario(Dia.Quarta, "14:20", "15:10"), new Horario(Dia.Quinta, "13:30", "14:20"), new Horario(Dia.Quinta, "14:20", "15:10") }, "102", Semestre.Semestre_1, 1743));
            turmas.Add(new Turma(ihc, new List<Horario>() { new Horario(Dia.Terça, "13:30", "14:20"), new Horario(Dia.Terça, "14:20", "15:10"), new Horario(Dia.Quinta, "13:30", "14:20"), new Horario(Dia.Quinta, "14:20", "15:10") }, "68", Semestre.Semestre_2, 6890));
            turmas.Add(new Turma(ihc, new List<Horario>() { new Horario(Dia.Terça, "19:30", "20:20"), new Horario(Dia.Terça, "20:20", "21:10"), new Horario(Dia.Quinta, "21:20", "22:10"), new Horario(Dia.Quinta, "22:10", "23:00") }, "68", Semestre.Semestre_2, 5183));
            turmas.Add(new Turma(ia, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Sexta, "20:20", "21:10") }, "68", Semestre.Semestre_2, 5236));

            turmas.Add(new Turma(aspectosPsicologicos, new List<Horario>() { new Horario(Dia.Segunda, "15:30", "16:20"), new Horario(Dia.Segunda, "16:20", "17:10"), new Horario(Dia.Segunda, "17:10", "18:00") }, "68", Semestre.Anual, 1027));
            turmas.Add(new Turma(eng3, new List<Horario>() { new Horario(Dia.Quinta, "09:40", "10:30"), new Horario(Dia.Quarta, "10:30", "11:20"), new Horario(Dia.Quinta, "11:20", "12:10") }, "102", Semestre.Anual, 1030));
            turmas.Add(new Turma(qualidade, new List<Horario>() { new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Anual, 1485));
            turmas.Add(new Turma(tecAvancadasSI, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00") }, "68", Semestre.Anual, 1499));
            turmas.Add(new Turma(eng3, new List<Horario>() { new Horario(Dia.Terça, "19:30", "20:20"), new Horario(Dia.Terça, "20:20", "21:10"), new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10") }, "136", Semestre.Anual, 1227));
            turmas.Add(new Turma(informaticaESociedade, new List<Horario>() { new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10") }, "34", Semestre.Anual, 5198));

            turmas.Add(new Turma(ord, new List<Horario>() { new Horario(Dia.Segunda, "13:30", "14:20"), new Horario(Dia.Segunda, "14:20", "15:10"), new Horario(Dia.Quarta, "13:30", "14:20"), new Horario(Dia.Quarta, "14:20", "15:10") }, "68", Semestre.Semestre_1, 6897));
            turmas.Add(new Turma(paradigmaImpOO, new List<Horario>() { new Horario(Dia.Segunda, "13:30", "14:20"), new Horario(Dia.Segunda, "14:20", "15:10"), new Horario(Dia.Quarta, "13:30", "14:20"), new Horario(Dia.Quarta, "14:20", "15:10") }, "68", Semestre.Semestre_2, 6888));
            turmas.Add(new Turma(ord, new List<Horario>() { new Horario(Dia.Terça, "19:30", "20:20"), new Horario(Dia.Terça, "20:20", "21:10"), new Horario(Dia.Quinta, "21:20", "22:10"), new Horario(Dia.Quinta, "22:10", "23:00") }, "68", Semestre.Semestre_1, 5187));
            turmas.Add(new Turma(paradigmaImpOO, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Semestre_2, 5185));

            turmas.Add(new Turma(compiladores, new List<Horario>() { new Horario(Dia.Quinta, "07:45", "08:35"), new Horario(Dia.Quinta, "08:35", "09:25") }, "68", Semestre.Anual, 1020));
            turmas.Add(new Turma(so2, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20") }, "68", Semestre.Anual, 1037));
            turmas.Add(new Turma(compiladores, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Semestre_2, "5204b"));
            turmas.Add(new Turma(progSistWeb, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Sexta, "20:20", "21:10") }, "68", Semestre.Semestre_1, "5188b"));
            turmas.Add(new Turma(poo, new List<Horario>() { new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "22:20", "21:10"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00") }, "68", Semestre.Semestre_1, "5190b"));

            turmas.Add(new Turma(estrutura, new List<Horario>() { new Horario(Dia.Segunda, "13:30", "14:20"), new Horario(Dia.Segunda, "14:20", "15:10"), new Horario(Dia.Terça, "09:40", "10:30"), new Horario(Dia.Terça, "10:30", "11:20"), new Horario(Dia.Sexta, "07:45", "08:35"), new Horario(Dia.Sexta, "08:35", "09:25") }, "102", Semestre.Semestre_1, "6884b"));
            turmas.Add(new Turma(algoritmos, new List<Horario>() { new Horario(Dia.Segunda, "07:45", "08:35"), new Horario(Dia.Segunda, "08:35", "09:25"), new Horario(Dia.Terça, "15:30", "16:20"), new Horario(Dia.Terça, "16:20", "17:10"), new Horario(Dia.Quinta, "07:45", "08:35"), new Horario(Dia.Quinta, "08:35", "09:25") }, "102", Semestre.Semestre_2, "6879c"));
            turmas.Add(new Turma(algoritmos, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00"), new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Sexta, "20:20", "21:10") }, "102", Semestre.Semestre_2, "5175c"));

            #endregion


            constantino.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { modelagem, metodosModelagem });
            carniel.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { bd1, bd2 });
            carniel.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { ti, pesquisa });
            airton.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { algoritmos, algoritmosEProg, mf1, mf2 });
            airton.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { estrutura, fundProgramacao });
            anderson.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { compiladores, concorrente });
            anderson.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { arq, arq2, assembly });
            sica.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { cd1, cd2 });
            sica.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { fundamentosEletronica });
            clelia.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { algoritmos, algoritmosEProg, fundProgramacao });
            clelia.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { estrutura });
            dante.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { cg, pesquisa });
            dante.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { topicosPesquisa });
            donizete.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { eng1, eng2, eng3, eng4, pss, iss });
            donizete.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { qualidade, ass });
            elisa.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { ass, qualidade, pss });
            elisa.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { eng1, eng2, eng3, eng4, iss });
            elvio.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { redes });
            elvio.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { fundamentosEletronica, sd });
            flavio.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { so, so2, sd });
            flavio.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { compiladores });
            itana.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { ass, pss, projetoSoftware, gerenciamentoProjeto });
            itana.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { eng1, eng2, eng3, eng4 });
            angelo.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { cd1, cd2 });
            angelo.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { fundamentosEletronica });
            josiane.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { infoAgro  });
            ze.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { paa });
            ze.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { grafos, teoriaDosGrafos });
            jucelia.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { informaticaESociedade });
            jucelia.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { programacaoLinear });
            linnyer.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { lingFormaisAutomatos });
            linnyer.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { informaticaESociedade });
            luciana.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { redes, sd });
            luciana.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { fundamentosEletronica });
            wigarashi.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { ia, poo, progSistWeb });
            wigarashi.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { compiladores, eng1 });
            malbarbo.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { grafos, lp, paradigmaImpOO });
            malbarbo.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { paa });
            madalena.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { bd1, bd2 });
            madalena.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { ti, pesquisa });
            yandre.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { lingFormaisAutomatos });
            yandre.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { informaticaESociedade });
            valeria.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { ord, paradigmaImpOO, lp });
            valeria.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { bd1, bd2 });
            thelma.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { informaticaESociedade, pesquisaOperacional, pesquisa, topicosPesquisa });
            thelma.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { tecAvancadasSI });
            raqueline.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { concorrente });
            raqueline.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { eng3 });
            ronaldo.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { assembly, algoritmos, algoritmosEProg, estrutura });
            ronaldo.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { fundProgramacao });
            sergio.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { ia, ihc });
            sergio.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { lp });
            tania.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { eng1, eng2, eng3 });
            tania.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { ass, pss });
            wesley.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { algoritmos, estrutura, algoritmosEProg });
            wesley.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { lp });
            andre.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { arq, arq2 });
            andre.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { fundamentosEletronica });
            franklin.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { algoritmos, algoritmosEProg, estrutura });
            franklin.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { fundProgramacao });
            nardenio.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { cd1, cd2 });
            nardenio.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { fundamentosEletronica });
            uber.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { algoritmos, algoritmosEProg, estrutura, fundProgramacao });
            uber.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { fundComputacao });
            heloise.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { bd1, bd2, ti });
            heloise.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { pesquisa });
            munif.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { bd1, bd2, poo });
            munif.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { fundComputacao });
            balancieri.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { bd1, bd2, poo, algoritmos, progSistWeb });
            balancieri.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { qualidade });
            juliana.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { paa, pesquisa });
            juliana.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { organizacaoComputadores });
            rodrigo.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { cd1, cd2, paa });
            rodrigo.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { fundamentosEletronica });
            edson.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { poo, progSistWeb });
            edson.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { bd1, bd2 });
            carlosFransley.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { paa, grafos, teoriaDosGrafos });
            carlosFransley.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { paradigmaImpOO });
            priscila.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { pesquisa, pesquisaOperacional});
            priscila.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { aspectosPsicologicos });
            leandro.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { cg, assembly });
            leandro.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { paradigmaImpOO });

            //var b = new MainWindow();
            //var viewModel = new ViewModel();
            //viewModel.Turmas = new ObservableCollection<Turma>(turmas);
            //viewModel.Professores = new ObservableCollection<Professor>(professores);
            //b.DataContext = viewModel;
            //b.Show();



            //string texto = GeradorArquivo.getTexto(professores,
            //        turmas);

            //var textoEsperado =
            //    "a";

            //Assert.AreEqual(textoEsperado.ToString(), texto);

            new AvaliadorDeResultado().avalia(
                @"P19T1029b
P41T1747
P3T1754
P0T1762
P29T6887b
P17T6872b
P4T6872c
P11T6891b
P15T6889b
P11T1684
P1T1837b
P11T5181b
P38T1452
P17T1487
P27T1470
P36T1590
P16T1590b
P36T1640
P34T1640b
P5T2536
P5T2536b
P40T2541
P21T2541b
P23T2541c
P33T2541d
P34T208
P2T208c
P36T208d
P34T208e
P12T208f
P36T1512b
P34T1512c
P21T6900b
P4T6894b
P30T7256
P25T7256b
P30T7256c
P30T7256d
P1T7331
P22T7239b
P14T7239c
P5T7239d
P4T7328
P10T7329
P29T5178b
P26T5183b
P38T5200b
P31T5193b
P24T5205b
P38T5184b
P38T5184c
P25T5259
P5T5259b
P25T5259c
P28T5259d
P25T5259e
P28T5259f
P5T4549
P3T4549b
P35T4573
P25T4553
P32T4309
P36T4309b
P41T4309c
P36T2536c
P5T6633
P34T6633b
P14T4460
P14T4460b
P14T4460c
P14T4460d
P33T3411
P31T3411b
P4T6882c
P12T6882d
P8T6885
P11T6885b
P39T5174d
P0T6903
P23T1760
P0T5199
P22T6900
P3T1744
P37T5175
P38T5237
P29T6887
P29T6895
P2T5204
P24T5205
P32T1034
P33T6882
P9T6872
P29T7330
P2T5178
P33T6872d
P10T5197
P26T5236b
P34T7239
P31T1759
P31T5202
P6T5171
P7T6904
P8T6899
P7T5191
P7T5186
P4T3513
P7T1753
P40T5188
P18T5190
P8T6891
P35T6874
P11T1498
P8T5176
P39T6878
P9T1023
P39T5174
P9T5203
P10T6901
P10T5196
P2T1757
P28T5175b
P30T6884
P28T6879
P30T5180
P6T6875
P6T6875b
P1T5182
P35T5192
P38T5171b
P6T1029
P8T5181
P7T1837
P39T6878b
P12T5174b
P21T6896
P15T1761
P13T6889
P13T5184
P19T5185b
P11T5176b
P0T6896b
P16T6883
P16T5195
P17T1023b
P17T1756
P9T5203b
P17T1838
P41T6898
P22T1028
P32T6902
P19T5189
P18T5200
P1T6892
P20T1752
P20T6874b
P20T5167
P1T2495
P33T6878c
P39T6882b
P12T5174c
P20T5182b
P35T5167b
P37T6879b
P13T6894
P25T5180b
P31T5193
P18T1743
P26T6890
P26T5183
P26T5236
P32T1027
P27T1030
P8T1485
P23T1499
P27T1227
P15T5198
P22T6897
P19T6888
P22T5187
P19T5185
P2T1020
P10T1037
P2T5204b
P18T5188b
P40T5190b
P28T6884b
P37T6879c
P37T5175c", professores, turmas);

        }




        [TestMethod]
        public void analisaCenario2()
        {
            var turmas = new List<Turma>();
            var professores = new List<Professor>();

            var compiladores = new Disciplina("Compiladores");
            var redes = new Disciplina("Redes de Computadores");
            var aspectosPsicologicos = new Disciplina("Aspectos Psicológicos e Sociais da Informática");
            var lp = new Disciplina("Linguagens de Programação");
            var tccCordenacao = new Disciplina("Trabalho de Graduação (Coordenação)");
            var eng3 = new Disciplina("Processo de Engenharia de Software III");
            var eng4 = new Disciplina("Processo de Engenharia de Software IV");
            var tcc = new Disciplina("Trabalho de Conclusão de Curso (Coordenação)");
            var automacao = new Disciplina("Automação");
            var progSistWeb = new Disciplina("Programação de sistemas WEB");
            var so2 = new Disciplina("Sistemas Operacionais II");
            var ia = new Disciplina("Inteligência Artificial");
            var mf1 = new Disciplina("Métodos Formais I");
            var teoriaDosGrafos = new Disciplina("Teoria dos Grafos e Análise de Algoritmos");
            var bd2 = new Disciplina("Banco de Dados II");
            var ambienteDesenv = new Disciplina("Ambientes de Desenvolvimento de Software");
            var mf2 = new Disciplina("Métodos Formais II");
            var sd = new Disciplina("Sistemas Distribuídos");
            var arq2 = new Disciplina("Arquitetura de Computadores II");
            var cg = new Disciplina("Computação Gráfica");
            var topicosPesquisa = new Disciplina("Tópicos em Pesquisa Operacional");
            var programacaoLinear = new Disciplina("Programação Linear");
            var simulacaoSistemas = new Disciplina("Simulação de Sistemas");
            var fundamentosEletronica = new Disciplina("Fundamentos de Eletrônica");
            var ti = new Disciplina("Fundamentos de Tecnologia da Informação");
            var pesquisa = new Disciplina("Fundamentos de Pesquisa");
            var cd1 = new Disciplina("Circuitos Digitais I");
            var algoritmos = new Disciplina("Fundamentos de Algoritmos");
            var cd2 = new Disciplina("Circuitos Digitais II");
            var lingFormaisAutomatos = new Disciplina("Linguagens Formais e Automatos");
            var estrutura = new Disciplina("Estruturas de Dados");
            var eng1 = new Disciplina("Proc. de Soft. e Eng. de Requisitos");
            var arq = new Disciplina("Arq. e Organiz. de Computadores");
            var paa = new Disciplina("Projeto e Análise de Algoritmos");
            var poo = new Disciplina("Par.de Prog.Imp.e Orient.a Objetos");
            var ihc = new Disciplina("Interação ser humano computador");
            var ass = new Disciplina("Análise de Sistemas de Software");
            var bd1 = new Disciplina("Banco de Dados I");
            var progInterfaceHardSoft = new Disciplina("Progr. Interfaceamento de Hard e Soft");
            var computabilidade = new Disciplina("Computabilidade");
            var ord = new Disciplina("Organização e Recuperação de Dados");
            var grafos = new Disciplina("Algortimos em Grafos");
            var pss = new Disciplina("Projeto de Sistemas de Software");
            var matematicaComputacional = new Disciplina("Matemática Computacional");
            var so = new Disciplina("Sistemas Operacionais");
            var logica = new Disciplina("Paradigma Prog. Lógica e Funcional");
            var modelagem = new Disciplina("Modelagem e Otimização Algorítmica");
            var iss = new Disciplina("Implementação de Sistemas de Software");
            var informaticaESociedade = new Disciplina("Informática e Sociedade");
            var concorrente = new Disciplina("Programação Concorrentes");
            var aprendizagemMaquina = new Disciplina("Aprendizagem de Máquina");
            var estagio = new Disciplina("Estágio Docência");
            var gerenciamentoProjeto = new Disciplina("Gerenciamento de Projetos de Software");
            var metodosModelagem = new Disciplina("Métodos de Modelagem de Software");
            var pesquisaOperacional = new Disciplina("Pesquisa Operacional");
            var seminario = new Disciplina("Seminários I");
            var seminarios2 = new Disciplina("Seminários II");
            var tc = new Disciplina("Teoria da Computação");
            var sistemasComputacao = new Disciplina("Tópicos em Sistemas de Computacão I");
            var sistemasComputacao2 = new Disciplina("Tópicos em Sistemas de Computacão II");
            var sistemasComputacao3 = new Disciplina("Tópicos em Sistemas de Computacão III");
            var sistemaInformacao = new Disciplina("Tópicos em Sistemas de Informação I");
            var sistemaInformacao2 = new Disciplina("Tópicos em Sistemas de Informação II");
            var trabIndividual = new Disciplina("Trabalho Individual");
            var organizacaoComputadores = new Disciplina("Organização de Computadores e Sistemas Operacionais");
            var eng2 = new Disciplina("Engenharia de Software II");
            var qualidade = new Disciplina("Qualidade de Software");
            var projetoSoftware = new Disciplina("Projeto de Software");
            var fundProgramacao = new Disciplina("Fundamentos de Programação");
            var tecAvancadasSI = new Disciplina("Tecnologias Avançadas para Sistemas de Informação");
            var progSistemas = new Disciplina("Programação de Sistemas");
            var algoritmosEProg = new Disciplina("Algoritmos e Prog Comp");
            var assembly = new Disciplina("Programação em Ling de Montagem");
            var paradigmaImpOO = new Disciplina("Par.de Prog.Imp.e Orient.a Objetos");
            var infoAgro = new Disciplina("Informática Aplicada a Agronomia");
            var fundComputacao = new Disciplina("Fundamentos da Computação (MEC)");
            var secretariado = new Disciplina("Introdução à Informática Secreatiado");
            var labProgramacao = new Disciplina("Lab. Fund de Programação");

            //turmas.Add(new Turma(redes, new List<Horario>() { new Horario(Dia.Quarta, "09:40", "10:30"), new Horario(Dia.Quarta, "10:30", "11:20") }, "68", Semestre.Anual, 1023));
            turmas.Add(new Turma(tccCordenacao, new List<Horario>() { new Horario(Dia.Terça, "13:30", "14:20"), new Horario(Dia.Terça, "14:20", "15:10") }, "68", Semestre.Anual, "1029b"));
            //new Turma(automacao, new List<Horario>() { new Horario(Dia.Quarta, "15:30", "16:20"), new Horario(Dia.Quarta, "16:20", "17:10"), new Horario(Dia.Quarta, "17:10", "18:00") }, "102", Semestre.Anual, 1034));
            //new Turma(mf1, new List<Horario>() { new Horario(Dia.Segunda, "13:30", "14:20"), new Horario(Dia.Segunda, "14:20", "15:10"), new Horario(Dia.Terça, "15:30", "16:20"), new Horario(Dia.Terça, "16:20", "17:10") }, "68", Semestre.Semestre_2, 1744));
            turmas.Add(new Turma(teoriaDosGrafos, new List<Horario>() { new Horario(Dia.Segunda, "13:30", "14:20"), new Horario(Dia.Segunda, "14:20", "15:10"), new Horario(Dia.Terça, "15:30", "16:20"), new Horario(Dia.Terça, "16:20", "17:10"), new Horario(Dia.Quinta, "13:30", "14:20"), new Horario(Dia.Quinta, "14:20", "13:10") }, "102", Semestre.Semestre_1, 1747));
            turmas.Add(new Turma(mf2, new List<Horario>() { new Horario(Dia.Terça, "15:30", "16:20"), new Horario(Dia.Terça, "16:20", "17:10"), new Horario(Dia.Quarta, "07:45", "08:35"), new Horario(Dia.Quarta, "08:35", "09:25") }, "68", Semestre.Semestre_2, 1754));
            //new Turma(topicosPesquisa, new List<Horario>() { new Horario(Dia.Segunda, "13:30", "14:20"), new Horario(Dia.Segunda, "14:20", "15:10"), new Horario(Dia.Quarta, "13:30", "14:20"), new Horario(Dia.Quarta, "14:20", "15:10"), new Horario(Dia.Quinta, "09:40", "10:30"), new Horario(Dia.Quinta, "10:30", "11:20") }, "102", Semestre.Semestre_2, 1760));
            //turmas.Add(new Turma(programacaoLinear, new List<Horario>() { new Horario(Dia.Quinta, "07:45", "08:35"), new Horario(Dia.Quinta, "08:35", "09:25"), new Horario(Dia.Sexta, "07:45", "08:35"), new Horario(Dia.Sexta, "08:35", "09:25")}, "68", Semestre.Semestre_1, 1761));
            turmas.Add(new Turma(simulacaoSistemas, new List<Horario>() { new Horario(Dia.Terça, "13:30", "14:20"), new Horario(Dia.Terça, "14:20", "15:10"), new Horario(Dia.Quinta, "07:45", "08:35"), new Horario(Dia.Quinta, "08:35", "09:25") }, "68", Semestre.Semestre_2, 1762));
            turmas.Add(new Turma(arq, new List<Horario>() { new Horario(Dia.Quarta, "09:40", "10:30"), new Horario(Dia.Quarta, "10:30", "11:20"), new Horario(Dia.Quinta, "15:30", "16:20"), new Horario(Dia.Quinta, "16:20", "17:10"), new Horario(Dia.Sexta, "07:45", "08:35"), new Horario(Dia.Sexta, "08:35", "09:25") }, "102", Semestre.Semestre_2, "6887b"));
            turmas.Add(new Turma(fundamentosEletronica, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20"), new Horario(Dia.Terça, "07:45", "08:35"), new Horario(Dia.Terça, "08:35", "09:25"), new Horario(Dia.Sexta, "09:40", "10:30"), new Horario(Dia.Sexta, "10:30", "11:20") }, "102", Semestre.Semestre_1, "6872b"));
            turmas.Add(new Turma(fundamentosEletronica, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20"), new Horario(Dia.Terça, "07:45", "08:35"), new Horario(Dia.Terça, "08:35", "09:25"), new Horario(Dia.Sexta, "09:40", "10:30"), new Horario(Dia.Sexta, "10:30", "11:20") }, "102", Semestre.Semestre_1, "6872c"));
            turmas.Add(new Turma(ass, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20"), new Horario(Dia.Quinta, "09:40", "10:30"), new Horario(Dia.Quinta, "10:30", "11:20") }, "68", Semestre.Semestre_2, "6891b"));
            turmas.Add(new Turma(programacaoLinear, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20"), new Horario(Dia.Segunda, "11:20", "12:10"), new Horario(Dia.Quarta, "09:40", "10:30"), new Horario(Dia.Quarta, "10:30", "11:20"), new Horario(Dia.Quarta, "11:20", "12:10") }, "102", Semestre.Semestre_2, "6889b"));

            turmas.Add(new Turma(mf2, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00") }, "68", Semestre.Semestre_1, 1684));
            //turmas.Add(new Turma(eng4, new List<Horario>() { new Horario(Dia.Terça, "19:30", "20:20"), new Horario(Dia.Terça, "20:20", "21:10"), new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10"), new Horario(Dia.Quinta, "21:20", "22:10"), new Horario(Dia.Quinta, "22:10", "23:00") }, "136", Semestre.Semestre_1, 1837));
            turmas.Add(new Turma(eng4, new List<Horario>() { new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10"), new Horario(Dia.Quinta, "21:20", "22:10"), new Horario(Dia.Quinta, "22:10", "23:00") }, "136", Semestre.Semestre_1, "1837b"));
            turmas.Add(new Turma(ass, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00") }, "68", Semestre.Semestre_2, "5181b"));

            turmas.Add(new Turma(organizacaoComputadores, new List<Horario>() { new Horario(Dia.Sabado, "13:30", "14:20"), new Horario(Dia.Sabado, "14:20", "15:10"), new Horario(Dia.Sabado, "15:30", "16:20") }, "102", Semestre.Anual, 1452));
            turmas.Add(new Turma(sd, new List<Horario>() { new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00") }, "68", Semestre.Anual, 1487));
            turmas.Add(new Turma(eng2, new List<Horario>() { new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Anual, 1470));

            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Terça, "07:45", "08:35"), new Horario(Dia.Quarta, "07:45", "08:35"), new Horario(Dia.Quarta, "08:35", "09:25") }, "136", Semestre.Anual, 1590));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Terça, "08:35", "09:25"), new Horario(Dia.Quarta, "07:45", "08:35"), new Horario(Dia.Quarta, "08:35", "09:25") }, "136", Semestre.Anual, "1590b"));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10") }, "68", Semestre.Semestre_2, 1640));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10") }, "68", Semestre.Semestre_2, "1640b"));
            turmas.Add(new Turma(fundProgramacao, new List<Horario>() { new Horario(Dia.Quarta, "09:40", "10:30"), new Horario(Dia.Quarta, "10:30", "11:20"), new Horario(Dia.Quarta, "11:20", "12:10") }, "51", Semestre.Semestre_2, 2536));
            turmas.Add(new Turma(fundProgramacao, new List<Horario>() { new Horario(Dia.Quarta, "15:30", "16:20"), new Horario(Dia.Quarta, "16:20", "17:10"), new Horario(Dia.Quarta, "17:10", "18:00") }, "51", Semestre.Semestre_2, "2536b"));
            turmas.Add(new Turma(labProgramacao, new List<Horario>() { new Horario(Dia.Quarta, "13:30", "14:20") }, "17", Semestre.Semestre_2, 2541));
            turmas.Add(new Turma(labProgramacao, new List<Horario>() { new Horario(Dia.Quarta, "07:45", "08:35") }, "17", Semestre.Semestre_2, "2541b"));
            turmas.Add(new Turma(labProgramacao, new List<Horario>() { new Horario(Dia.Quinta, "13:30", "14:20") }, "17", Semestre.Semestre_2, "2541c"));
            turmas.Add(new Turma(labProgramacao, new List<Horario>() { new Horario(Dia.Quinta, "07:45", "08:35") }, "17", Semestre.Semestre_2, "2541d"));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Sexta, "09:40", "10:30"), new Horario(Dia.Sexta, "10:30", "11:20"), new Horario(Dia.Quarta, "15:30", "16:20") }, "68", Semestre.Anual, "208"));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Quarta, "16:20", "17:10"), new Horario(Dia.Sexta, "09:40", "10:30"), new Horario(Dia.Sexta, "10:30", "11:20") }, "68", Semestre.Anual, "208c"));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Quarta, "17:10", "18:00"), new Horario(Dia.Sexta, "09:40", "10:30"), new Horario(Dia.Sexta, "10:30", "11:20") }, "68", Semestre.Anual, "208d"));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Segunda, "15:30", "16:20"), new Horario(Dia.Segunda, "16:20", "17:10"), new Horario(Dia.Terça, "09:40", "10:30") }, "68", Semestre.Anual, "208d"));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Segunda, "15:30", "16:20"), new Horario(Dia.Segunda, "16:20", "17:10"), new Horario(Dia.Terça, "10:30", "11:20") }, "68", Semestre.Anual, "208e"));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Segunda, "15:30", "16:20"), new Horario(Dia.Segunda, "16:20", "17:10"), new Horario(Dia.Terça, "11:20", "12:10") }, "68", Semestre.Anual, "208f"));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Quarta, "09:40", "10:30"), new Horario(Dia.Quarta, "10:30", "11:20"), new Horario(Dia.Quarta, "11:20", "12:10") }, "68", Semestre.Anual, "1512b"));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Quarta, "08:35", "09:25"), new Horario(Dia.Quarta, "09:40", "10:30"), new Horario(Dia.Quarta, "10:30", "11:20") }, "68", Semestre.Anual, "1512c"));


            turmas.Add(new Turma(matematicaComputacional, new List<Horario>() { new Horario(Dia.Segunda, "15:30", "16:20"), new Horario(Dia.Segunda, "16:20", "17:10"), new Horario(Dia.Quinta, "13:30", "14:20"), new Horario(Dia.Quinta, "14:20", "15:10") }, "68", Semestre.Semestre_2, "6900b"));
            turmas.Add(new Turma(progInterfaceHardSoft, new List<Horario>() { new Horario(Dia.Terça, "13:30", "14:20"), new Horario(Dia.Terça, "14:20", "15:10"), new Horario(Dia.Sexta, "07:45", "08:35"), new Horario(Dia.Sexta, "08:35", "09:25") }, "68", Semestre.Semestre_1, "6894b"));
            turmas.Add(new Turma(estrutura, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Sabado, "13:30", "14:20"), new Horario(Dia.Sabado, "14:20", "15:10") }, "68", Semestre.Semestre_2, 7256));
            turmas.Add(new Turma(estrutura, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Sabado, "13:30", "14:20"), new Horario(Dia.Sabado, "14:20", "15:10") }, "68", Semestre.Semestre_2, "7256b"));
            turmas.Add(new Turma(estrutura, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Sabado, "13:30", "14:20"), new Horario(Dia.Sabado, "14:20", "15:10") }, "68", Semestre.Semestre_2, "7256c"));
            turmas.Add(new Turma(estrutura, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Sabado, "13:30", "14:20"), new Horario(Dia.Sabado, "14:20", "15:10") }, "68", Semestre.Semestre_2, "7256d"));
            turmas.Add(new Turma(bd1, new List<Horario>() { new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10"), new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00") }, "68", Semestre.Semestre_2, 7331));
            turmas.Add(new Turma(fundProgramacao, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00"), new Horario(Dia.Sabado, "13:30", "14:20"), new Horario(Dia.Sabado, "14:20", "15:10") }, "102", Semestre.Semestre_1, "7239b"));
            turmas.Add(new Turma(fundProgramacao, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00"), new Horario(Dia.Sabado, "13:30", "14:20"), new Horario(Dia.Sabado, "14:20", "15:10") }, "102", Semestre.Semestre_1, "7239c"));
            turmas.Add(new Turma(fundProgramacao, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00"), new Horario(Dia.Sabado, "13:30", "14:20"), new Horario(Dia.Sabado, "14:20", "15:10") }, "102", Semestre.Semestre_1, "7239d"));
            turmas.Add(new Turma(progSistemas, new List<Horario>() { new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00"), new Horario(Dia.Sabado, "15:30", "16:20"), new Horario(Dia.Sabado, "16:20", "17:10") }, "102", Semestre.Semestre_1, 7328));
            turmas.Add(new Turma(so, new List<Horario>() { new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10"), new Horario(Dia.Sabado, "13:30", "14:20"), new Horario(Dia.Sabado, "14:20", "15:10") }, "68", Semestre.Semestre_1, 7329));
            turmas.Add(new Turma(arq, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00") }, "68", Semestre.Semestre_1, "5178b"));
            turmas.Add(new Turma(ihc, new List<Horario>() { new Horario(Dia.Terça, "19:30", "20:20"), new Horario(Dia.Terça, "20:20", "21:10"), new Horario(Dia.Quinta, "21:20", "22:10"), new Horario(Dia.Quinta, "22:10", "23:00") }, "68", Semestre.Semestre_2, "5183b"));
            turmas.Add(new Turma(logica, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00") }, "68", Semestre.Semestre_1, "5200b"));
            turmas.Add(new Turma(assembly, new List<Horario>() { new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00") }, "68", Semestre.Semestre_2, "5193b"));
            turmas.Add(new Turma(concorrente, new List<Horario>() { new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00"), new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Sexta, "20:20", "21:10") }, "68", Semestre.Semestre_2, "5205b"));
            turmas.Add(new Turma(paa, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Semestre_2, "5184b"));
            turmas.Add(new Turma(paa, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Semestre_2, "5184c"));
            turmas.Add(new Turma(algoritmosEProg, new List<Horario>() { new Horario(Dia.Segunda, "13:30", "14:20"), new Horario(Dia.Segunda, "14:20", "15:10"), new Horario(Dia.Terça, "15:30", "16:20") }, "136", Semestre.Anual, 5259));
            turmas.Add(new Turma(algoritmosEProg, new List<Horario>() { new Horario(Dia.Segunda, "13:30", "14:20"), new Horario(Dia.Segunda, "14:20", "15:10"), new Horario(Dia.Terça, "16:20", "17:10") }, "136", Semestre.Anual, "5259b"));
            turmas.Add(new Turma(algoritmosEProg, new List<Horario>() { new Horario(Dia.Quarta, "13:30", "14:20"), new Horario(Dia.Quarta, "14:20", "15:10"), new Horario(Dia.Quarta, "15:30", "16:20") }, "136", Semestre.Anual, "5259c"));
            turmas.Add(new Turma(algoritmosEProg, new List<Horario>() { new Horario(Dia.Quarta, "13:30", "14:20"), new Horario(Dia.Quarta, "14:20", "15:10"), new Horario(Dia.Quarta, "16:20", "17:10") }, "136", Semestre.Anual, "5259d"));
            turmas.Add(new Turma(algoritmosEProg, new List<Horario>() { new Horario(Dia.Quinta, "15:30", "16:20"), new Horario(Dia.Quinta, "16:20", "17:10"), new Horario(Dia.Sexta, "15:30", "16:20") }, "136", Semestre.Anual, "5259e"));
            turmas.Add(new Turma(algoritmosEProg, new List<Horario>() { new Horario(Dia.Quinta, "15:30", "16:20"), new Horario(Dia.Quinta, "16:20", "17:10"), new Horario(Dia.Sexta, "16:20", "17:10") }, "136", Semestre.Anual, "5259f"));
            turmas.Add(new Turma(algoritmosEProg, new List<Horario>() { new Horario(Dia.Segunda, "15:30", "16:20"), new Horario(Dia.Segunda, "16:20", "17:10"), new Horario(Dia.Quarta, "15:30", "16:20"), new Horario(Dia.Quarta, "16:20", "17:10"), new Horario(Dia.Quinta, "15:30", "16:20"), new Horario(Dia.Quinta, "16:20", "17:10") }, "102", Semestre.Semestre_1, 4549));
            turmas.Add(new Turma(algoritmosEProg, new List<Horario>() { new Horario(Dia.Segunda, "15:30", "16:20"), new Horario(Dia.Segunda, "16:20", "17:10"), new Horario(Dia.Quarta, "13:30", "14:20"), new Horario(Dia.Quarta, "14:20", "15:10"), new Horario(Dia.Quarta, "15:30", "16:20"), new Horario(Dia.Quarta, "16:20", "17:10") }, "102", Semestre.Semestre_1, "4549b"));
            turmas.Add(new Turma(bd1, new List<Horario>() { new Horario(Dia.Segunda, "15:30", "16:20"), new Horario(Dia.Segunda, "16:20", "17:10"), new Horario(Dia.Quarta, "13:30", "14:20"), new Horario(Dia.Quarta, "14:20", "15:10"), new Horario(Dia.Quarta, "15:30", "16:20"), new Horario(Dia.Quarta, "16:20", "17:10") }, "68", Semestre.Semestre_2, 4573));
            turmas.Add(new Turma(estrutura, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00"), new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Semestre_2, 4553));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00"), new Horario(Dia.Sabado, "13:30", "14:20"), new Horario(Dia.Sabado, "14:20", "15:10") }, "102", Semestre.Anual, 4309));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00"), new Horario(Dia.Sabado, "13:30", "14:20"), new Horario(Dia.Sabado, "14:20", "15:10") }, "102", Semestre.Anual, "4309b"));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00"), new Horario(Dia.Sabado, "13:30", "14:20"), new Horario(Dia.Sabado, "14:20", "15:10") }, "102", Semestre.Anual, "4309c"));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Segunda, "15:30", "16:20"), new Horario(Dia.Segunda, "16:20", "17:10"), new Horario(Dia.Segunda, "17:10", "18:00") }, "51", Semestre.Semestre_2, "2536c"));
            turmas.Add(new Turma(fundProgramacao, new List<Horario>() { new Horario(Dia.Sexta, "09:40", "10:30"), new Horario(Dia.Sexta, "10:30", "11:20"), new Horario(Dia.Sexta, "11:20", "12:10") }, "51", Semestre.Semestre_1, 6633));
            turmas.Add(new Turma(fundProgramacao, new List<Horario>() { new Horario(Dia.Quinta, "15:30", "16:20"), new Horario(Dia.Quinta, "16:20", "17:10"), new Horario(Dia.Quinta, "17:10", "18:00") }, "51", Semestre.Semestre_1, "6633b"));
            turmas.Add(new Turma(infoAgro, new List<Horario>() { new Horario(Dia.Terça, "13:30", "14:20"), new Horario(Dia.Terça, "14:20", "15:10"), new Horario(Dia.Quinta, "13:30", "14:20"), new Horario(Dia.Quinta, "14:20", "15:10") }, "68", Semestre.Semestre_2, 4460));
            turmas.Add(new Turma(infoAgro, new List<Horario>() { new Horario(Dia.Terça, "13:30", "14:20"), new Horario(Dia.Terça, "14:20", "15:10"), new Horario(Dia.Quinta, "13:30", "14:20"), new Horario(Dia.Quinta, "14:20", "15:10") }, "68", Semestre.Semestre_2, "4460b"));
            turmas.Add(new Turma(infoAgro, new List<Horario>() { new Horario(Dia.Terça, "13:30", "14:20"), new Horario(Dia.Terça, "14:20", "15:10"), new Horario(Dia.Quinta, "13:30", "14:20"), new Horario(Dia.Quinta, "14:20", "15:10") }, "68", Semestre.Semestre_2, "4460c"));
            turmas.Add(new Turma(infoAgro, new List<Horario>() { new Horario(Dia.Terça, "13:30", "14:20"), new Horario(Dia.Terça, "14:20", "15:10"), new Horario(Dia.Quinta, "13:30", "14:20"), new Horario(Dia.Quinta, "14:20", "15:10") }, "68", Semestre.Semestre_2, "4460d"));
            turmas.Add(new Turma(secretariado, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00") }, "68", Semestre.Semestre_2, 3411));
            turmas.Add(new Turma(secretariado, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00") }, "68", Semestre.Semestre_2, "3411b"));
            turmas.Add(new Turma(cd2, new List<Horario>() { new Horario(Dia.Segunda, "07:45", "08:35"), new Horario(Dia.Segunda, "08:35", "09:25"), new Horario(Dia.Quarta, "07:45", "08:35"), new Horario(Dia.Quarta, "08:35", "09:25") }, "68", Semestre.Semestre_1, "6882c"));
            turmas.Add(new Turma(cd2, new List<Horario>() { new Horario(Dia.Segunda, "07:45", "08:35"), new Horario(Dia.Segunda, "08:35", "09:25"), new Horario(Dia.Quarta, "07:45", "08:35"), new Horario(Dia.Quarta, "08:35", "09:25") }, "68", Semestre.Semestre_1, "6882d"));
            turmas.Add(new Turma(pss, new List<Horario>() { new Horario(Dia.Terça, "07:45", "08:35"), new Horario(Dia.Terça, "08:35", "09:25"), new Horario(Dia.Quinta, "13:30", "14:20"), new Horario(Dia.Quinta, "14:20", "15:10") }, "68", Semestre.Semestre_1, 6885));
            turmas.Add(new Turma(pss, new List<Horario>() { new Horario(Dia.Terça, "07:45", "08:35"), new Horario(Dia.Terça, "08:35", "09:25"), new Horario(Dia.Quinta, "13:30", "14:20"), new Horario(Dia.Quinta, "14:20", "15:10") }, "68", Semestre.Semestre_1, "6885b"));
            turmas.Add(new Turma(cd1, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Semestre_2, "5174d"));
            turmas.Add(new Turma(informaticaESociedade, new List<Horario>() { new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10") }, "34", Semestre.Anual, 5198));

            #region Professores

            var constantino = new Professor(new List<Turma>(getTurmasDadoDisciplina(turmas, new List<Disciplina>() { modelagem, metodosModelagem })), new List<Turma>(getTurmasDadoDisciplina(turmas, new List<Disciplina>() { })), "Ademir Aparecido Constantino", new List<Turma>(), "8");
            var carniel = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Ademir Carniel", new List<Turma>(), "8");
            var anderson = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Anderson Faustino da Silva", new List<Turma>(), "8");
            var airton = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Airton Marco Polidório", new List<Turma>(), "8");
            var sica = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Carlos Benedito Sica de Toledo", new List<Turma>(), "8");
            var clelia = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Clélia Franco", new List<Turma>(), "8");
            var dante = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Dante Alves Medeiros Filho", new List<Turma>(), "8");
            var donizete = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Donizete Carlos Bruzarosco", new List<Turma>(), "8");
            var elisa = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Elisa Hatsue Moriya Huzita", new List<Turma>(), "8");
            var elvio = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Elvio João Leonardo", new List<Turma>(), "8");
            var flavio = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Flávio Arnaldo Braga da Silva", new List<Turma>(), "8");
            var itana = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Itana Maria de Souza Gimenes", new List<Turma>(), "8");
            var angelo = new Professor(new List<Turma>() { }, new List<Turma>() { }, "João Angelo Martini", new List<Turma>(), "8");
            var ze = new Professor(new List<Turma>() { }, new List<Turma>() { }, "José Roberto Vasconcelos", new List<Turma>(), "8");
            var josiane = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Josiane Melchiori Pinheiro", new List<Turma>(), "8");
            var jucelia = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Jucélia Geni Pereira Fregoneis", new List<Turma>(), "8");
            var linnyer = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Linnyer Beatrys Ruiz", new List<Turma>(), "8");
            var luciana = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Luciana Andréia Fondazzi Martimiano", new List<Turma>(), "8");
            var wigarashi = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Wagner Igarashi", new List<Turma>(), "8");
            var malbarbo = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Marco Aurélio Lopes Barbosa", new List<Turma>(), "8");
            var madalena = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Maria Madalena Dias", new List<Turma>(), "8");
            var yandre = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Yandre Maldonado e Gomes da Costa", new List<Turma>(), "8");
            var valeria = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Valéria Delisandra Feltrim", new List<Turma>(), "8");
            var thelma = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Thelma Elita Colanzi Lopes", new List<Turma>(), "8");
            var raqueline = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Raqueline Ritter de Moura", new List<Turma>(), "8");
            var ronaldo = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Ronaldo Augusto de Lara Gonçalves", new List<Turma>(), "8");
            var sergio = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Sérgio Roberto P. da Silva", new List<Turma>(), "8");
            var tania = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Tânia Fátima Calvi Tait", new List<Turma>(), "8");
            var wesley = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Wesley Romão", new List<Turma>(), "8");
            var andre = new Professor(new List<Turma>() { }, new List<Turma>() { }, "André Barbosa Verona", new List<Turma>(), "12");
            var franklin = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Franklin César Flores", new List<Turma>(), "8");
            var leandro = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Leandro Magno", new List<Turma>(), "16");
            var priscila = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Priscila Aparecida de Moraes", new List<Turma>(), "16");
            var nardenio = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Nardênio Almeida Martins", new List<Turma>(), "8");
            var uber = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Flávio Uber", new List<Turma>(), "8");
            var heloise = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Heloise  Manica", new List<Turma>(), "16");
            var munif = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Munif Gebara Júnior", new List<Turma>(), "16");
            var balancieri = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Renato Balancieri", new List<Turma>(), "8");
            var juliana = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Juliana Keiko Yamaguchi", new List<Turma>(), "12");
            var rodrigo = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Rodrigo Lankaites Pinheiro", new List<Turma>(), "16");
            var edson = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Edson Alves de Oliveira Júniuor", new List<Turma>(), "8");
            var carlosFransley = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Carlos Fransley", new List<Turma>(), "8");

            professores.Add(constantino);
            professores.Add(carniel);
            professores.Add(anderson);
            professores.Add(airton);
            professores.Add(sica);
            professores.Add(clelia);
            professores.Add(dante);
            professores.Add(donizete);
            professores.Add(elisa);
            professores.Add(elvio);
            professores.Add(andre);
            professores.Add(flavio);
            professores.Add(itana);
            professores.Add(tania);
            professores.Add(angelo);
            professores.Add(ze);
            professores.Add(josiane);
            professores.Add(jucelia);
            professores.Add(linnyer);
            professores.Add(luciana);
            professores.Add(wigarashi);
            professores.Add(malbarbo);
            professores.Add(madalena);
            professores.Add(yandre);
            professores.Add(valeria);
            professores.Add(thelma);
            professores.Add(raqueline);
            professores.Add(ronaldo);
            professores.Add(sergio);
            professores.Add(wesley);
            professores.Add(franklin);
            professores.Add(nardenio);
            professores.Add(uber);
            professores.Add(heloise);
            professores.Add(munif);
            professores.Add(balancieri);
            professores.Add(juliana);
            professores.Add(rodrigo);
            professores.Add(edson);
            professores.Add(carlosFransley);
            professores.Add(priscila);
            professores.Add(leandro);

            
            #endregion

            #region Turmas Consolidadas
            turmas.Add(new Turma(modelagem, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20"), new Horario(Dia.Terça, "09:40", "10:30"), new Horario(Dia.Terça, "10:30", "11:20"), new Horario(Dia.Sexta, "07:45", "08:35"), new Horario(Dia.Sexta, "08:45", "09:25") }, "102", Semestre.Semestre_2, constantino, 6903));
            turmas.Add(new Turma(topicosPesquisa, new List<Horario>() { new Horario(Dia.Segunda, "13:30", "14:20"), new Horario(Dia.Segunda, "14:20", "15:10"), new Horario(Dia.Quarta, "13:30", "14:20"), new Horario(Dia.Quarta, "14:20", "15:10"), new Horario(Dia.Quinta, "09:40", "10:30"), new Horario(Dia.Quinta, "10:30", "11:20") }, "102", Semestre.Semestre_2, constantino, 1760));
            turmas.Add(new Turma(modelagem, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00") }, "102", Semestre.Semestre_2, constantino, 5199));

            turmas.Add(new Turma(matematicaComputacional, new List<Horario>() { new Horario(Dia.Segunda, "15:30", "16:20"), new Horario(Dia.Segunda, "16:20", "17:10"), new Horario(Dia.Quinta, "13:30", "14:20"), new Horario(Dia.Quinta, "14:20", "15:10") }, "68", Semestre.Semestre_2, airton, 6900));
            turmas.Add(new Turma(mf1, new List<Horario>() { new Horario(Dia.Terça, "15:30", "16:20"), new Horario(Dia.Terça, "16:20", "17:10"), new Horario(Dia.Quinta, "15:30", "16:20"), new Horario(Dia.Quinta, "16:20", "17:10") }, "68", Semestre.Semestre_2, airton, 1744));
            turmas.Add(new Turma(algoritmos, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00"), new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Sexta, "20:20", "21:10") }, "102", Semestre.Semestre_2, airton, 5175));
            turmas.Add(new Turma(mf1, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Sexta, "20:20", "21:10") }, "68", Semestre.Semestre_1, airton, 5237));

            turmas.Add(new Turma(arq, new List<Horario>() { new Horario(Dia.Segunda, "07:45", "08:35"), new Horario(Dia.Segunda, "08:35", "09:25"), new Horario(Dia.Quinta, "15:30", "16:20"), new Horario(Dia.Quinta, "16:20", "17:10"), new Horario(Dia.Sexta, "07:45", "08:35"), new Horario(Dia.Sexta, "08:35", "09:25") }, "102", Semestre.Semestre_2, anderson, 6887));
            turmas.Add(new Turma(arq2, new List<Horario>() { new Horario(Dia.Terça, "07:45", "08:35"), new Horario(Dia.Terça, "08:35", "09:25"), new Horario(Dia.Quinta, "07:45", "08:35"), new Horario(Dia.Quinta, "08:35", "09:25") }, "68", Semestre.Semestre_1, anderson, 6895));
            turmas.Add(new Turma(compiladores, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Semestre_2, anderson, 5204));
            turmas.Add(new Turma(concorrente, new List<Horario>() { new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00"), new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Sexta, "20:20", "21:10") }, "68", Semestre.Semestre_2, anderson, 5205));

            turmas.Add(new Turma(automacao, new List<Horario>() { new Horario(Dia.Quarta, "09:40", "10:30"), new Horario(Dia.Quarta, "10:30", "11:20"), new Horario(Dia.Quarta, "11:20", "12:10") }, "102", Semestre.Anual, andre, 1034));
            turmas.Add(new Turma(cd2, new List<Horario>() { new Horario(Dia.Segunda, "07:45", "08:35"), new Horario(Dia.Segunda, "08:35", "09:25"), new Horario(Dia.Quarta, "07:45", "08:35"), new Horario(Dia.Quarta, "08:35", "09:25") }, "68", Semestre.Semestre_1, andre, 6882));
            turmas.Add(new Turma(fundamentosEletronica, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20"), new Horario(Dia.Terça, "07:45", "08:35"), new Horario(Dia.Terça, "08:35", "09:25"), new Horario(Dia.Sexta, "09:40", "10:30"), new Horario(Dia.Sexta, "10:30", "11:20") }, "102", Semestre.Semestre_1, andre, 6872));
            turmas.Add(new Turma(arq, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Sabado, "15:30", "16:20"), new Horario(Dia.Sabado, "16:20", "17:10") }, "68", Semestre.Semestre_2, andre, 7330));
            turmas.Add(new Turma(arq, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00") }, "68", Semestre.Semestre_1, andre, 5178));

            turmas.Add(new Turma(fundamentosEletronica, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20"), new Horario(Dia.Terça, "07:45", "08:35"), new Horario(Dia.Terça, "08:35", "09:25"), new Horario(Dia.Sexta, "09:40", "10:30"), new Horario(Dia.Sexta, "10:30", "11:20") }, "102", Semestre.Semestre_1, sica, "6872d"));
            turmas.Add(new Turma(simulacaoSistemas, new List<Horario>() { new Horario(Dia.Terça, "13:30", "14:20"), new Horario(Dia.Terça, "14:20", "15:10"), new Horario(Dia.Quinta, "07:45", "08:35"), new Horario(Dia.Quinta, "08:35", "09:25") }, "68", Semestre.Semestre_2, sica, 1762));
            turmas.Add(new Turma(sd, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Sexta, "20:20", "21:10") }, "68", Semestre.Semestre_1, sica, 5197));

            turmas.Add(new Turma(automacao, new List<Horario>() { new Horario(Dia.Quarta, "15:30", "16:20"), new Horario(Dia.Quarta, "16:20", "17:10"), new Horario(Dia.Quarta, "17:10", "18:00") }, "102", Semestre.Anual, carlosFransley, "1034b"));
            turmas.Add(new Turma(ia, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Sexta, "20:20", "21:10") }, "68", Semestre.Semestre_2, carlosFransley, "5236b"));

            turmas.Add(new Turma(fundProgramacao, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00"), new Horario(Dia.Sabado, "13:30", "14:20"), new Horario(Dia.Sabado, "14:20", "15:10") }, "102", Semestre.Semestre_1, clelia, 7239));

            turmas.Add(new Turma(cg, new List<Horario>() { new Horario(Dia.Quinta, "09:40", "10:30"), new Horario(Dia.Quinta, "10:30", "11:20"), new Horario(Dia.Quinta, "11:20", "12:10"), new Horario(Dia.Sexta, "09:40", "10:30"), new Horario(Dia.Sexta, "10:30", "11:20"), new Horario(Dia.Sexta, "11:20", "12:10") }, "102", Semestre.Semestre_1, dante, 1759));
            turmas.Add(new Turma(cg, new List<Horario>() { new Horario(Dia.Terça, "19:30", "20:20"), new Horario(Dia.Terça, "20:20", "21:10"), new Horario(Dia.Quinta, "21:20", "22:10"), new Horario(Dia.Quinta, "22:10", "23:00") }, "68", Semestre.Semestre_2, dante, 5202));
            turmas.Add(new Turma(pesquisa, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Semestre_1, dante, 5171));

            turmas.Add(new Turma(iss, new List<Horario>() { new Horario(Dia.Segunda, "07:45", "08:35"), new Horario(Dia.Segunda, "08:35", "09:25"), new Horario(Dia.Quinta, "09:40", "10:30"), new Horario(Dia.Quinta, "10:30", "11:20") }, "68", Semestre.Semestre_2, donizete, 6904));
            turmas.Add(new Turma(pss, new List<Horario>() { new Horario(Dia.Segunda, "07:45", "08:35"), new Horario(Dia.Segunda, "08:35", "09:25"), new Horario(Dia.Quinta, "09:40", "10:30"), new Horario(Dia.Quinta, "10:30", "11:20") }, "68", Semestre.Semestre_1, donizete, 6899));
            turmas.Add(new Turma(iss, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10") }, "68", Semestre.Semestre_2, donizete, 5191));
            turmas.Add(new Turma(iss, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00") }, "68", Semestre.Semestre_1, donizete, 5186));
            turmas.Add(new Turma(tcc, new List<Horario>() { new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00") }, "68", Semestre.Anual, donizete, 3513));

            turmas.Add(new Turma(ambienteDesenv, new List<Horario>() { new Horario(Dia.Terça, "13:30", "14:20"), new Horario(Dia.Terça, "14:20", "15:10"), new Horario(Dia.Quinta, "15:30", "16:20"), new Horario(Dia.Quinta, "16:20", "17:10") }, "68", Semestre.Semestre_2, edson, 1753));
            turmas.Add(new Turma(progSistWeb, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Sexta, "20:20", "21:10") }, "68", Semestre.Semestre_1, edson, 5188));
            turmas.Add(new Turma(poo, new List<Horario>() { new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "22:20", "21:10"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00") }, "68", Semestre.Semestre_1, edson, 5190));
            turmas.Add(new Turma(ass, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20"), new Horario(Dia.Quinta, "09:40", "10:30"), new Horario(Dia.Quinta, "10:30", "11:20") }, "68", Semestre.Semestre_2, edson, 6891));

            turmas.Add(new Turma(ti, new List<Horario>() { new Horario(Dia.Segunda, "13:30", "14:20"), new Horario(Dia.Segunda, "14:20", "15:10"), new Horario(Dia.Quinta, "09:40", "10:30"), new Horario(Dia.Quinta, "10:30", "11:20") }, "68", Semestre.Semestre_1, elisa, 6874));
            turmas.Add(new Turma(projetoSoftware, new List<Horario>() { new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Sexta, "20:20", "21:10") }, "68", Semestre.Anual, elisa, 1498));
            turmas.Add(new Turma(pss, new List<Horario>() { new Horario(Dia.Terça, "19:30", "20:20"), new Horario(Dia.Terça, "20:20", "21:10"), new Horario(Dia.Quinta, "21:20", "22:10"), new Horario(Dia.Quinta, "22:10", "23:00") }, "68", Semestre.Semestre_1, elisa, 5176));

            turmas.Add(new Turma(cd1, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20"), new Horario(Dia.Terça, "07:45", "08:35"), new Horario(Dia.Terça, "08:35", "09:25"), new Horario(Dia.Sexta, "09:40", "10:30"), new Horario(Dia.Sexta, "10:30", "11:20") }, "102", Semestre.Semestre_2, elvio, 6878));
            turmas.Add(new Turma(redes, new List<Horario>() { new Horario(Dia.Quarta, "09:40", "10:30"), new Horario(Dia.Quarta, "10:30", "11:20") }, "68", Semestre.Anual, elvio, 1023));
            turmas.Add(new Turma(cd1, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Semestre_2, elvio, 5174));
            turmas.Add(new Turma(redes, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10") }, "68", Semestre.Semestre_2, elvio, 5203));

            turmas.Add(new Turma(so, new List<Horario>() { new Horario(Dia.Segunda, "13:30", "14:20"), new Horario(Dia.Segunda, "14:20", "15:10"), new Horario(Dia.Quarta, "07:45", "08:35"), new Horario(Dia.Quarta, "08:35", "09:25"), new Horario(Dia.Sexta, "09:40", "10:30"), new Horario(Dia.Sexta, "10:30", "11:20") }, "102", Semestre.Semestre_2, flavio, 6901));
            turmas.Add(new Turma(so, new List<Horario>() { new Horario(Dia.Terça, "19:30", "20:20"), new Horario(Dia.Terça, "20:20", "21:10"), new Horario(Dia.Quinta, "21:20", "22:10"), new Horario(Dia.Quinta, "22:10", "23:00") }, "68", Semestre.Semestre_1, flavio, 5196));


            turmas.Add(new Turma(arq2, new List<Horario>() { new Horario(Dia.Terça, "13:30", "14:20"), new Horario(Dia.Terça, "14:20", "15:10"), new Horario(Dia.Quinta, "09:40", "10:30"), new Horario(Dia.Quinta, "10:30", "11:20") }, "68", Semestre.Semestre_1, uber, 1757));
            turmas.Add(new Turma(mf2, new List<Horario>() { new Horario(Dia.Segunda, "13:30", "14:20"), new Horario(Dia.Segunda, "14:20", "15:10"), new Horario(Dia.Quarta, "15:30", "16:20"), new Horario(Dia.Quarta, "16:20", "17:10") }, "68", Semestre.Semestre_2, uber, 1754));
            turmas.Add(new Turma(algoritmos, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00"), new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Sexta, "20:20", "21:10") }, "102", Semestre.Semestre_2, uber, "5175b"));
            turmas.Add(new Turma(mf2, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00") }, "68", Semestre.Semestre_1, uber, 1754));

            turmas.Add(new Turma(estrutura, new List<Horario>() { new Horario(Dia.Segunda, "13:30", "14:20"), new Horario(Dia.Segunda, "14:20", "15:10"), new Horario(Dia.Terça, "09:40", "10:30"), new Horario(Dia.Terça, "10:30", "11:20"), new Horario(Dia.Sexta, "07:45", "08:35"), new Horario(Dia.Sexta, "08:35", "09:25") }, "102", Semestre.Semestre_1, franklin, 6884));
            turmas.Add(new Turma(algoritmos, new List<Horario>() { new Horario(Dia.Segunda, "07:45", "08:35"), new Horario(Dia.Segunda, "08:35", "09:25"), new Horario(Dia.Terça, "15:30", "16:20"), new Horario(Dia.Terça, "16:20", "17:10"), new Horario(Dia.Quinta, "07:45", "08:35"), new Horario(Dia.Quinta, "08:35", "09:25") }, "102", Semestre.Semestre_2, franklin, 6879));
            turmas.Add(new Turma(estrutura, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00") }, "102", Semestre.Semestre_1, franklin, 5180));

            turmas.Add(new Turma(pesquisa, new List<Horario>() { new Horario(Dia.Terça, "15:30", "16:20"), new Horario(Dia.Terça, "16:20", "17:10") }, "34", Semestre.Semestre_1, heloise, 6875));
            turmas.Add(new Turma(pesquisa, new List<Horario>() { new Horario(Dia.Quinta, "15:30", "16:20"), new Horario(Dia.Quinta, "16:20", "17:10") }, "34", Semestre.Semestre_1, heloise, "6875b"));
            turmas.Add(new Turma(bd1, new List<Horario>() { new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00") }, "68", Semestre.Semestre_2, heloise, 5182));
            turmas.Add(new Turma(bd2, new List<Horario>() { new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00"), new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Sexta, "20:20", "21:10"), }, "68", Semestre.Semestre_2, heloise, 5192));
            turmas.Add(new Turma(pesquisa, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "22:20", "21:10") }, "68", Semestre.Semestre_1, heloise, "5171b"));

            turmas.Add(new Turma(pesquisa, new List<Horario>() { new Horario(Dia.Terça, "07:45", "08:35"), new Horario(Dia.Terça, "08:35", "09:25") }, "68", Semestre.Anual, itana, 1029));
            turmas.Add(new Turma(ass, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00") }, "68", Semestre.Semestre_2, itana, 5181));
            turmas.Add(new Turma(eng4, new List<Horario>() { new Horario(Dia.Terça, "19:30", "20:20"), new Horario(Dia.Terça, "20:20", "21:10"), new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10"), new Horario(Dia.Quinta, "21:20", "22:10"), new Horario(Dia.Quinta, "22:10", "23:00") }, "136", Semestre.Semestre_1, itana, 1837));

            turmas.Add(new Turma(cd1, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20"), new Horario(Dia.Terça, "07:45", "08:35"), new Horario(Dia.Terça, "08:35", "09:25"), new Horario(Dia.Sexta, "09:40", "10:30"), new Horario(Dia.Sexta, "10:30", "11:20") }, "102", Semestre.Semestre_2, angelo, "6878b"));
            turmas.Add(new Turma(cd1, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Semestre_2, angelo, "5174b"));

            turmas.Add(new Turma(computabilidade, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20"), new Horario(Dia.Terça, "09:40", "10:30"), new Horario(Dia.Terça, "10:30", "11:20") }, "68", Semestre.Semestre_1, ze, 6896));
            turmas.Add(new Turma(programacaoLinear, new List<Horario>() { new Horario(Dia.Quinta, "07:45", "08:35"), new Horario(Dia.Quinta, "08:35", "09:25"), new Horario(Dia.Sexta, "07:45", "08:35"), new Horario(Dia.Sexta, "08:35", "09:25") }, "68", Semestre.Semestre_1, ze, 1761));
            turmas.Add(new Turma(paa, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20"), new Horario(Dia.Segunda, "11:20", "12:10"), new Horario(Dia.Quarta, "09:40", "10:30"), new Horario(Dia.Quarta, "10:30", "11:20"), new Horario(Dia.Quarta, "11:20", "12:10") }, "102", Semestre.Semestre_2, ze, 6889));
            turmas.Add(new Turma(paa, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Semestre_2, ze, 5184));

            turmas.Add(new Turma(paradigmaImpOO, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Semestre_2, juliana, "5185b"));
            turmas.Add(new Turma(pss, new List<Horario>() { new Horario(Dia.Terça, "19:30", "20:20"), new Horario(Dia.Terça, "20:20", "21:10"), new Horario(Dia.Quinta, "21:20", "22:10"), new Horario(Dia.Quinta, "22:10", "23:00") }, "68", Semestre.Semestre_1, juliana, "5176b"));

            turmas.Add(new Turma(computabilidade, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20"), new Horario(Dia.Terça, "09:40", "10:30"), new Horario(Dia.Terça, "10:30", "11:20") }, "68", Semestre.Semestre_1, linnyer, "6896b"));
            turmas.Add(new Turma(lingFormaisAutomatos, new List<Horario>() { new Horario(Dia.Segunda, "15:30", "16:20"), new Horario(Dia.Segunda, "16:20", "17:10"), new Horario(Dia.Segunda, "17:10", "18:00"), new Horario(Dia.Terça, "15:30", "16:20"), new Horario(Dia.Terça, "16:20", "17:10"), new Horario(Dia.Terça, "17:10", "18:00") }, "102", Semestre.Semestre_1, linnyer, 6883));
            turmas.Add(new Turma(lingFormaisAutomatos, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Terça, "19:30", "20:20"), new Horario(Dia.Terça, "20:20", "21:10"), new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00") }, "102", Semestre.Semestre_1, linnyer, 5195));

            turmas.Add(new Turma(redes, new List<Horario>() { new Horario(Dia.Quarta, "09:40", "10:30"), new Horario(Dia.Quarta, "10:30", "11:20") }, "68", Semestre.Anual, luciana, "1023b"));
            turmas.Add(new Turma(sd, new List<Horario>() { new Horario(Dia.Terça, "15:30", "16:20"), new Horario(Dia.Terça, "16:20", "17:10"), new Horario(Dia.Terça, "17:10", "18:00"), new Horario(Dia.Quinta, "15:30", "16:20"), new Horario(Dia.Quinta, "16:20", "17:10"), new Horario(Dia.Quinta, "17:10", "18:00") }, "102", Semestre.Semestre_1, luciana, 1756));
            turmas.Add(new Turma(redes, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10") }, "68", Semestre.Semestre_2, luciana, "5203b"));
            turmas.Add(new Turma(sd, new List<Horario>() { new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10"), new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Sexta, "20:20", "21:10") }, "68", Semestre.Semestre_1, luciana, 1838));

            turmas.Add(new Turma(grafos, new List<Horario>() { new Horario(Dia.Quarta, "09:40", "10:30"), new Horario(Dia.Quarta, "10:30", "11:20"), new Horario(Dia.Sexta, "09:40", "10:30"), new Horario(Dia.Sexta, "10:30", "11:20") }, "68", Semestre.Semestre_1, malbarbo, 6898));
            turmas.Add(new Turma(lp, new List<Horario>() { new Horario(Dia.Quinta, "13:30", "14:20"), new Horario(Dia.Quinta, "14:20", "15:10") }, "68", Semestre.Anual, malbarbo, 1028));
            turmas.Add(new Turma(logica, new List<Horario>() { new Horario(Dia.Terça, "07:45", "08:35"), new Horario(Dia.Terça, "08:35", "09:25"), new Horario(Dia.Quinta, "07:45", "08:35"), new Horario(Dia.Quinta, "08:35", "09:25") }, "68", Semestre.Semestre_2, malbarbo, 6902));
            turmas.Add(new Turma(grafos, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10") }, "68", Semestre.Semestre_1, malbarbo, 5189));
            turmas.Add(new Turma(logica, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00") }, "68", Semestre.Semestre_1, malbarbo, 5200));

            turmas.Add(new Turma(bd1, new List<Horario>() { new Horario(Dia.Terça, "09:40", "10:30"), new Horario(Dia.Terça, "10:30", "11:20"), new Horario(Dia.Quinta, "07:45", "08:35"), new Horario(Dia.Quinta, "08:35", "09:25") }, "68", Semestre.Semestre_2, madalena, 6892));
            turmas.Add(new Turma(bd2, new List<Horario>() { new Horario(Dia.Terça, "09:40", "10:30"), new Horario(Dia.Terça, "10:30", "11:20"), new Horario(Dia.Terça, "11:20", "12:10"), new Horario(Dia.Quinta, "15:30", "16:20"), new Horario(Dia.Quinta, "16:20", "17:10"), new Horario(Dia.Quinta, "17:10", "18:00") }, "102", Semestre.Semestre_2, madalena, 1752));
            turmas.Add(new Turma(ti, new List<Horario>() { new Horario(Dia.Segunda, "13:30", "14:20"), new Horario(Dia.Segunda, "14:20", "15:10"), new Horario(Dia.Quinta, "09:40", "10:30"), new Horario(Dia.Quinta, "10:30", "11:20") }, "68", Semestre.Semestre_1, madalena, "6874b"));
            turmas.Add(new Turma(ti, new List<Horario>() { new Horario(Dia.Terça, "19:30", "20:20"), new Horario(Dia.Terça, "20:20", "21:10"), new Horario(Dia.Quinta, "21:20", "22:10"), new Horario(Dia.Quinta, "22:10", "23:00") }, "68", Semestre.Semestre_1, madalena, 5167));


            turmas.Add(new Turma(bd1, new List<Horario>() { new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10"), new Horario(Dia.Quarta, "21:20", "22:10") }, "102", Semestre.Anual, munif, 2495));

            turmas.Add(new Turma(cd1, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20"), new Horario(Dia.Terça, "07:45", "08:35"), new Horario(Dia.Terça, "08:35", "09:25"), new Horario(Dia.Sexta, "09:40", "10:30"), new Horario(Dia.Sexta, "10:30", "11:20") }, "102", Semestre.Semestre_2, nardenio, "6878c"));
            turmas.Add(new Turma(cd2, new List<Horario>() { new Horario(Dia.Segunda, "07:45", "08:35"), new Horario(Dia.Segunda, "08:35", "09:25"), new Horario(Dia.Quarta, "07:45", "08:35"), new Horario(Dia.Quarta, "08:35", "09:25") }, "68", Semestre.Semestre_1, nardenio, "6882b"));
            turmas.Add(new Turma(cd1, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Semestre_2, nardenio, "5174c"));

            turmas.Add(new Turma(bd1, new List<Horario>() { new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00") }, "68", Semestre.Semestre_2, balancieri, "5182b"));
            turmas.Add(new Turma(ti, new List<Horario>() { new Horario(Dia.Terça, "19:30", "20:20"), new Horario(Dia.Terça, "20:20", "21:10"), new Horario(Dia.Quinta, "21:20", "22:10"), new Horario(Dia.Quinta, "22:10", "23:00") }, "68", Semestre.Semestre_1, balancieri, "5167b"));

            turmas.Add(new Turma(algoritmos, new List<Horario>() { new Horario(Dia.Segunda, "07:45", "08:35"), new Horario(Dia.Segunda, "08:35", "09:25"), new Horario(Dia.Terça, "15:30", "16:20"), new Horario(Dia.Terça, "16:20", "17:10"), new Horario(Dia.Quinta, "07:45", "08:35"), new Horario(Dia.Quinta, "08:35", "09:25") }, "102", Semestre.Semestre_2, ronaldo, "6879b"));
            turmas.Add(new Turma(progInterfaceHardSoft, new List<Horario>() { new Horario(Dia.Terça, "13:30", "14:20"), new Horario(Dia.Terça, "14:20", "15:10"), new Horario(Dia.Sexta, "07:45", "08:35"), new Horario(Dia.Sexta, "08:35", "09:25") }, "68", Semestre.Semestre_1, ronaldo, 6894));
            turmas.Add(new Turma(estrutura, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00") }, "102", Semestre.Semestre_1, ronaldo, "5180b"));
            turmas.Add(new Turma(assembly, new List<Horario>() { new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00") }, "68", Semestre.Semestre_2, ronaldo, 5193));

            turmas.Add(new Turma(ia, new List<Horario>() { new Horario(Dia.Segunda, "15:30", "16:20"), new Horario(Dia.Segunda, "16:20", "17:10"), new Horario(Dia.Quarta, "13:30", "14:20"), new Horario(Dia.Quarta, "14:20", "15:10"), new Horario(Dia.Quinta, "13:30", "14:20"), new Horario(Dia.Quinta, "14:20", "15:10") }, "102", Semestre.Semestre_1, sergio, 1743));
            turmas.Add(new Turma(ihc, new List<Horario>() { new Horario(Dia.Terça, "13:30", "14:20"), new Horario(Dia.Terça, "14:20", "15:10"), new Horario(Dia.Quinta, "13:30", "14:20"), new Horario(Dia.Quinta, "14:20", "15:10") }, "68", Semestre.Semestre_2, sergio, 6890));
            turmas.Add(new Turma(ihc, new List<Horario>() { new Horario(Dia.Terça, "19:30", "20:20"), new Horario(Dia.Terça, "20:20", "21:10"), new Horario(Dia.Quinta, "21:20", "22:10"), new Horario(Dia.Quinta, "22:10", "23:00") }, "68", Semestre.Semestre_2, sergio, 5183));
            turmas.Add(new Turma(ia, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Sexta, "20:20", "21:10") }, "68", Semestre.Semestre_2, sergio, 5236));

            turmas.Add(new Turma(aspectosPsicologicos, new List<Horario>() { new Horario(Dia.Segunda, "15:30", "16:20"), new Horario(Dia.Segunda, "16:20", "17:10"), new Horario(Dia.Segunda, "17:10", "18:00") }, "68", Semestre.Anual, tania, 1027));
            turmas.Add(new Turma(eng3, new List<Horario>() { new Horario(Dia.Quinta, "09:40", "10:30"), new Horario(Dia.Quarta, "10:30", "11:20"), new Horario(Dia.Quinta, "11:20", "12:10") }, "102", Semestre.Anual, tania, 1030));
            turmas.Add(new Turma(qualidade, new List<Horario>() { new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Anual, tania, 1485));
            turmas.Add(new Turma(tecAvancadasSI, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00") }, "68", Semestre.Anual, tania, 1499));
            turmas.Add(new Turma(eng3, new List<Horario>() { new Horario(Dia.Terça, "19:30", "20:20"), new Horario(Dia.Terça, "20:20", "21:10"), new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10") }, "136", Semestre.Anual, tania, 1227));

            turmas.Add(new Turma(ord, new List<Horario>() { new Horario(Dia.Segunda, "13:30", "14:20"), new Horario(Dia.Segunda, "14:20", "15:10"), new Horario(Dia.Quarta, "13:30", "14:20"), new Horario(Dia.Quarta, "14:20", "15:10") }, "68", Semestre.Semestre_1, valeria, 6897));
            turmas.Add(new Turma(paradigmaImpOO, new List<Horario>() { new Horario(Dia.Segunda, "13:30", "14:20"), new Horario(Dia.Segunda, "14:20", "15:10"), new Horario(Dia.Quarta, "13:30", "14:20"), new Horario(Dia.Quarta, "14:20", "15:10") }, "68", Semestre.Semestre_2, valeria, 6888));
            turmas.Add(new Turma(ord, new List<Horario>() { new Horario(Dia.Terça, "19:30", "20:20"), new Horario(Dia.Terça, "20:20", "21:10"), new Horario(Dia.Quinta, "21:20", "22:10"), new Horario(Dia.Quinta, "22:10", "23:00") }, "68", Semestre.Semestre_1, valeria, 5187));
            turmas.Add(new Turma(paradigmaImpOO, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Semestre_2, valeria, 5185));

            turmas.Add(new Turma(compiladores, new List<Horario>() { new Horario(Dia.Quinta, "07:45", "08:35"), new Horario(Dia.Quinta, "08:35", "09:25") }, "68", Semestre.Anual, wigarashi, 1020));
            turmas.Add(new Turma(so2, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20") }, "68", Semestre.Anual, wigarashi, 1037));
            turmas.Add(new Turma(compiladores, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Semestre_2, wigarashi, "5204b"));
            turmas.Add(new Turma(progSistWeb, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Sexta, "20:20", "21:10") }, "68", Semestre.Semestre_1, wigarashi, "5188b"));
            turmas.Add(new Turma(poo, new List<Horario>() { new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "22:20", "21:10"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00") }, "68", Semestre.Semestre_1, wigarashi, "5190b"));

            turmas.Add(new Turma(estrutura, new List<Horario>() { new Horario(Dia.Segunda, "13:30", "14:20"), new Horario(Dia.Segunda, "14:20", "15:10"), new Horario(Dia.Terça, "09:40", "10:30"), new Horario(Dia.Terça, "10:30", "11:20"), new Horario(Dia.Sexta, "07:45", "08:35"), new Horario(Dia.Sexta, "08:35", "09:25") }, "102", Semestre.Semestre_1, wesley, "6884b"));
            turmas.Add(new Turma(algoritmos, new List<Horario>() { new Horario(Dia.Segunda, "07:45", "08:35"), new Horario(Dia.Segunda, "08:35", "09:25"), new Horario(Dia.Terça, "15:30", "16:20"), new Horario(Dia.Terça, "16:20", "17:10"), new Horario(Dia.Quinta, "07:45", "08:35"), new Horario(Dia.Quinta, "08:35", "09:25") }, "102", Semestre.Semestre_2, wesley, "6879c"));
            turmas.Add(new Turma(algoritmos, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00"), new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Sexta, "20:20", "21:10") }, "102", Semestre.Semestre_2, wesley, "5175c"));

            #endregion

            constantino.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { modelagem, metodosModelagem });
            carniel.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { bd1, bd2 });
            carniel.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { ti, pesquisa });
            airton.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { algoritmos, algoritmosEProg, mf1, mf2 });
            airton.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { estrutura, fundProgramacao });
            anderson.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { compiladores, concorrente });
            anderson.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { arq, arq2, assembly });
            sica.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { cd1, cd2 });
            sica.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { fundamentosEletronica });
            clelia.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { algoritmos, algoritmosEProg, fundProgramacao });
            clelia.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { estrutura });
            dante.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { cg, pesquisa });
            dante.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { topicosPesquisa });
            donizete.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { eng1, eng2, eng3, eng4, pss, iss });
            donizete.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { qualidade, ass });
            elisa.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { ass, qualidade, pss });
            elisa.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { eng1, eng2, eng3, eng4, iss });
            elvio.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { redes });
            elvio.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { fundamentosEletronica, sd });
            flavio.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { so, so2, sd });
            flavio.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { compiladores });
            itana.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { ass, pss, projetoSoftware, gerenciamentoProjeto });
            itana.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { eng1, eng2, eng3, eng4 });
            angelo.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { cd1, cd2 });
            angelo.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { fundamentosEletronica });
            josiane.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { infoAgro });
            ze.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { paa });
            ze.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { grafos, teoriaDosGrafos });
            jucelia.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { informaticaESociedade });
            jucelia.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { programacaoLinear });
            linnyer.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { lingFormaisAutomatos });
            linnyer.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { informaticaESociedade });
            luciana.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { redes, sd });
            luciana.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { fundamentosEletronica });
            wigarashi.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { ia, poo, progSistWeb });
            wigarashi.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { compiladores, eng1 });
            malbarbo.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { grafos, lp, paradigmaImpOO });
            malbarbo.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { paa });
            madalena.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { bd1, bd2 });
            madalena.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { ti, pesquisa });
            yandre.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { lingFormaisAutomatos });
            yandre.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { informaticaESociedade });
            valeria.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { ord, paradigmaImpOO, lp });
            valeria.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { bd1, bd2 });
            thelma.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { informaticaESociedade, pesquisaOperacional, pesquisa, topicosPesquisa });
            thelma.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { tecAvancadasSI });
            raqueline.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { concorrente });
            raqueline.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { eng3 });
            ronaldo.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { assembly, algoritmos, algoritmosEProg, estrutura });
            ronaldo.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { fundProgramacao });
            sergio.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { ia, ihc });
            sergio.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { lp });
            tania.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { eng1, eng2, eng3 });
            tania.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { ass, pss });
            wesley.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { algoritmos, estrutura, algoritmosEProg });
            wesley.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { lp });
            andre.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { arq, arq2 });
            andre.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { fundamentosEletronica });
            franklin.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { algoritmos, algoritmosEProg, estrutura });
            franklin.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { fundProgramacao });
            nardenio.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { cd1, cd2 });
            nardenio.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { fundamentosEletronica });
            uber.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { algoritmos, algoritmosEProg, estrutura, fundProgramacao });
            uber.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { fundComputacao });
            heloise.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { bd1, bd2, ti });
            heloise.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { pesquisa });
            munif.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { bd1, bd2, poo });
            munif.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { fundComputacao });
            balancieri.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { bd1, bd2, poo, algoritmos, progSistWeb });
            balancieri.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { qualidade });
            juliana.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { paa, pesquisa });
            juliana.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { organizacaoComputadores });
            rodrigo.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { cd1, cd2, paa });
            rodrigo.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { fundamentosEletronica });
            edson.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { poo, progSistWeb });
            edson.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { bd1, bd2 });
            carlosFransley.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { paa, grafos, teoriaDosGrafos });
            carlosFransley.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { paradigmaImpOO });
            priscila.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { pesquisa, pesquisaOperacional });
            priscila.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { aspectosPsicologicos });
            leandro.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { cg, assembly });
            leandro.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { paradigmaImpOO });

            //string texto = GeradorArquivo.getTexto(professores,
            //        turmas);

            //var textoEsperado =
            //    "a";

            //Assert.AreEqual(textoEsperado.ToString(), texto);


            new AvaliadorDeResultado().avalia(
                @"P32T1029b
P41T1747
P34T1754
P4T1762
P2T6887b
P39T6872b
P9T6872c
P8T6891b
P15T6889b
P32T1684
P7T1837b
P8T5181b
P38T1452
P10T1487
P8T1470
P37T1590
P24T1590b
P36T1640
P36T1640b
P5T2536
P5T2536b
P0T2541
P24T2541b
P32T2541c
P23T2541d
P32T208
P36T208c
P23T208d
P31T208e
P36T208f
P36T1512b
P32T1512c
P24T6900b
P31T6894b
P30T7256
P5T7256b
P35T7256c
P25T7256d
P1T7331
P21T7239b
P14T7239c
P23T7239d
P23T7328
P10T7329
P39T5178b
P26T5183b
P0T5200b
P31T5193b
P24T5205b
P39T5184b
P13T5184c
P5T5259
P3T5259b
P5T5259c
P3T5259d
P30T5259e
P28T5259f
P21T4549
P10T4549b
P1T4573
P28T4553
P12T4309
P31T4309b
P15T4309c
P36T2536c
P5T6633
P34T6633b
P14T4460
P14T4460b
P14T4460c
P14T4460d
P32T3411
P38T3411b
P12T6882c
P39T6882d
P27T6885
P8T6885b
P4T5174d
P23T5198
P0T6903
P0T1760
P0T5199
P3T6900
P3T1744
P3T5175
P3T5237
P2T6887
P2T6895
P2T5204
P2T5205
P29T1034
P29T6882
P29T6872
P29T7330
P29T5178
P4T6872d
P4T5197
P41T1034b
P41T5236b
P5T7239
P6T1759
P6T5202
P6T5171
P7T6904
P7T6899
P7T5191
P7T5186
P7T3513
P40T1753
P40T5188
P40T5190
P40T6891
P8T6874
P8T1498
P8T5176
P9T6878
P9T1023
P9T5174
P9T5203
P10T6901
P10T5196
P34T1757
P34T5175b
P30T6884
P30T6879
P30T5180
P35T6875
P35T6875b
P35T5182
P35T5192
P35T5171b
P11T1029
P11T5181
P11T1837
P12T6878b
P12T5174b
P13T6896
P13T1761
P13T6889
P13T5184
P38T5185b
P38T5176b
P16T6896b
P16T6883
P16T5195
P17T1023b
P17T1756
P17T5203b
P17T1838
P19T6898
P19T1028
P19T6902
P19T5189
P19T5200
P20T6892
P20T1752
P20T6874b
P20T5167
P36T2495
P33T6878c
P33T6882b
P33T5174c
P37T5182b
P37T5167b
P25T6879b
P25T6894
P25T5180b
P25T5193
P26T1743
P26T6890
P26T5183
P26T5236
P27T1027
P27T1030
P27T1485
P27T1499
P27T1227
P22T6897
P22T6888
P22T5187
P22T5185
P18T1020
P18T1037
P18T5204b
P18T5188b
P18T5190b
P28T6884b
P28T6879c
P28T5175c", professores, turmas);
        }


        private List<Turma> getTurmasDadoDisciplina(List<Turma> original, List<Disciplina> disciplina)
        {
            return original.Where(e => disciplina.Contains(e.Disciplina)).ToList();
        }
    }
}


