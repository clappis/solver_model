﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using solver.Dto;
using solver.Regra;

namespace solver
{
    public class ViewModel : INotifyPropertyChanged
    {
        public ViewModel()
        {
            var turmas = new List<Turma>();
            var professores = new List<Professor>();

            var compiladores = new Disciplina("Compiladores");
            var redes = new Disciplina("Redes de Computadores");
            var aspectosPsicologicos = new Disciplina("Aspectos Psicológicos e Sociais da Informática");
            var lp = new Disciplina("Linguagens de Programação");
            var tccCordenacao = new Disciplina("Trabalho de Graduação (Coordenação)");
            var eng3 = new Disciplina("Processo de Engenharia de Software III");
            var eng4 = new Disciplina("Processo de Engenharia de Software IV");
            var tcc = new Disciplina("Trabalho de Conclusão de Curso (Coordenação)");
            var automacao = new Disciplina("Automação");
            var progSistWeb = new Disciplina("Programação de sistemas WEB");
            var so2 = new Disciplina("Sistemas Operacionais II");
            var ia = new Disciplina("Inteligência Artificial");
            var mf1 = new Disciplina("Métodos Formais I");
            var teoriaDosGrafos = new Disciplina("Teoria dos Grafos e Análise de Algoritmos");
            var bd2 = new Disciplina("Banco de Dados II");
            var ambienteDesenv = new Disciplina("Ambientes de Desenvolvimento de Software");
            var mf2 = new Disciplina("Métodos Formais II");
            var sd = new Disciplina("Sistemas Distribuídos");
            var arq2 = new Disciplina("Arquitetura de Computadores II");
            var cg = new Disciplina("Computação Gráfica");
            var topicosPesquisa = new Disciplina("Tópicos em Pesquisa Operacional");
            var programacaoLinear = new Disciplina("Programação Linear");
            var simulacaoSistemas = new Disciplina("Simulação de Sistemas");
            var fundamentosEletronica = new Disciplina("Fundamentos de Eletrônica");
            var ti = new Disciplina("Fundamentos de Tecnologia da Informação");
            var pesquisa = new Disciplina("Fundamentos de Pesquisa");
            var cd1 = new Disciplina("Circuitos Digitais I");
            var algoritmos = new Disciplina("Fundamentos de Algoritmos");
            var cd2 = new Disciplina("Circuitos Digitais II");
            var lingFormaisAutomatos = new Disciplina("Linguagens Formais e Automatos");
            var estrutura = new Disciplina("Estruturas de Dados");
            var eng1 = new Disciplina("Proc. de Soft. e Eng. de Requisitos");
            var arq = new Disciplina("Arq. e Organiz. de Computadores");
            var paa = new Disciplina("Projeto e Análise de Algoritmos");
            var poo = new Disciplina("Par.de Prog.Imp.e Orient.a Objetos");
            var ihc = new Disciplina("Interação ser humano computador");
            var ass = new Disciplina("Análise de Sistemas de Software");
            var bd1 = new Disciplina("Banco de Dados I");
            var progInterfaceHardSoft = new Disciplina("Progr. Interfaceamento de Hard e Soft");
            var computabilidade = new Disciplina("Computabilidade");
            var ord = new Disciplina("Organização e Recuperação de Dados");
            var grafos = new Disciplina("Algortimos em Grafos");
            var pss = new Disciplina("Projeto de Sistemas de Software");
            var matematicaComputacional = new Disciplina("Matemática Computacional");
            var so = new Disciplina("Sistemas Operacionais");
            var logica = new Disciplina("Paradigma Prog. Lógica e Funcional");
            var modelagem = new Disciplina("Modelagem e Otimização Algorítmica");
            var iss = new Disciplina("Implementação de Sistemas de Software");
            var informaticaESociedade = new Disciplina("Informática e Sociedade");
            var concorrente = new Disciplina("Programação Concorrentes");
            var aprendizagemMaquina = new Disciplina("Aprendizagem de Máquina");
            var estagio = new Disciplina("Estágio Docência");
            var gerenciamentoProjeto = new Disciplina("Gerenciamento de Projetos de Software");
            var metodosModelagem = new Disciplina("Métodos de Modelagem de Software");
            var pesquisaOperacional = new Disciplina("Pesquisa Operacional");
            var seminario = new Disciplina("Seminários I");
            var seminarios2 = new Disciplina("Seminários II");
            var tc = new Disciplina("Teoria da Computação");
            var sistemasComputacao = new Disciplina("Tópicos em Sistemas de Computacão I");
            var sistemasComputacao2 = new Disciplina("Tópicos em Sistemas de Computacão II");
            var sistemasComputacao3 = new Disciplina("Tópicos em Sistemas de Computacão III");
            var sistemaInformacao = new Disciplina("Tópicos em Sistemas de Informação I");
            var sistemaInformacao2 = new Disciplina("Tópicos em Sistemas de Informação II");
            var trabIndividual = new Disciplina("Trabalho Individual");
            var organizacaoComputadores = new Disciplina("Organização de Computadores e Sistemas Operacionais");
            var eng2 = new Disciplina("Engenharia de Software II");
            var qualidade = new Disciplina("Qualidade de Software");
            var projetoSoftware = new Disciplina("Projeto de Software");
            var fundProgramacao = new Disciplina("Fundamentos de Programação");
            var tecAvancadasSI = new Disciplina("Tecnologias Avançadas para Sistemas de Informação");
            var progSistemas = new Disciplina("Programação de Sistemas");
            var algoritmosEProg = new Disciplina("Algoritmos e Prog Comp");
            var assembly = new Disciplina("Programação em Ling de Montagem");
            var paradigmaImpOO = new Disciplina("Par.de Prog.Imp.e Orient.a Objetos");
            var infoAgro = new Disciplina("Informática Aplicada a Agronomia");
            var fundComputacao = new Disciplina("Fundamentos da Computação (MEC)");
            var secretariado = new Disciplina("Introdução à Informática Secreatiado");
            var labProgramacao = new Disciplina("Lab. Fund de Programação");

            //turmas.Add(new Turma(redes, new List<Horario>() { new Horario(Dia.Quarta, "09:40", "10:30"), new Horario(Dia.Quarta, "10:30", "11:20") }, "68", Semestre.Anual, 1023));
            turmas.Add(new Turma(tccCordenacao, new List<Horario>() { new Horario(Dia.Terça, "13:30", "14:20"), new Horario(Dia.Terça, "14:20", "15:10") }, "68", Semestre.Anual, "1029b"));
            //new Turma(automacao, new List<Horario>() { new Horario(Dia.Quarta, "15:30", "16:20"), new Horario(Dia.Quarta, "16:20", "17:10"), new Horario(Dia.Quarta, "17:10", "18:00") }, "102", Semestre.Anual, 1034));
            //new Turma(mf1, new List<Horario>() { new Horario(Dia.Segunda, "13:30", "14:20"), new Horario(Dia.Segunda, "14:20", "15:10"), new Horario(Dia.Terça, "15:30", "16:20"), new Horario(Dia.Terça, "16:20", "17:10") }, "68", Semestre.Semestre_2, 1744));
            turmas.Add(new Turma(teoriaDosGrafos, new List<Horario>() { new Horario(Dia.Segunda, "13:30", "14:20"), new Horario(Dia.Segunda, "14:20", "15:10"), new Horario(Dia.Terça, "15:30", "16:20"), new Horario(Dia.Terça, "16:20", "17:10"), new Horario(Dia.Quinta, "13:30", "14:20"), new Horario(Dia.Quinta, "14:20", "13:10") }, "102", Semestre.Semestre_1, 1747));
            turmas.Add(new Turma(mf2, new List<Horario>() { new Horario(Dia.Terça, "15:30", "16:20"), new Horario(Dia.Terça, "16:20", "17:10"), new Horario(Dia.Quarta, "07:45", "08:35"), new Horario(Dia.Quarta, "08:35", "09:25") }, "68", Semestre.Semestre_2, 1754));
            //new Turma(topicosPesquisa, new List<Horario>() { new Horario(Dia.Segunda, "13:30", "14:20"), new Horario(Dia.Segunda, "14:20", "15:10"), new Horario(Dia.Quarta, "13:30", "14:20"), new Horario(Dia.Quarta, "14:20", "15:10"), new Horario(Dia.Quinta, "09:40", "10:30"), new Horario(Dia.Quinta, "10:30", "11:20") }, "102", Semestre.Semestre_2, 1760));
            //turmas.Add(new Turma(programacaoLinear, new List<Horario>() { new Horario(Dia.Quinta, "07:45", "08:35"), new Horario(Dia.Quinta, "08:35", "09:25"), new Horario(Dia.Sexta, "07:45", "08:35"), new Horario(Dia.Sexta, "08:35", "09:25")}, "68", Semestre.Semestre_1, 1761));
            turmas.Add(new Turma(simulacaoSistemas, new List<Horario>() { new Horario(Dia.Terça, "13:30", "14:20"), new Horario(Dia.Terça, "14:20", "15:10"), new Horario(Dia.Quinta, "07:45", "08:35"), new Horario(Dia.Quinta, "08:35", "09:25") }, "68", Semestre.Semestre_2, 1762));
            turmas.Add(new Turma(arq, new List<Horario>() { new Horario(Dia.Quarta, "09:40", "10:30"), new Horario(Dia.Quarta, "10:30", "11:20"), new Horario(Dia.Quinta, "15:30", "16:20"), new Horario(Dia.Quinta, "16:20", "17:10"), new Horario(Dia.Sexta, "07:45", "08:35"), new Horario(Dia.Sexta, "08:35", "09:25") }, "102", Semestre.Semestre_2, "6887b"));
            turmas.Add(new Turma(fundamentosEletronica, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20"), new Horario(Dia.Terça, "07:45", "08:35"), new Horario(Dia.Terça, "08:35", "09:25"), new Horario(Dia.Sexta, "09:40", "10:30"), new Horario(Dia.Sexta, "10:30", "11:20") }, "102", Semestre.Semestre_1, "6872b"));
            turmas.Add(new Turma(fundamentosEletronica, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20"), new Horario(Dia.Terça, "07:45", "08:35"), new Horario(Dia.Terça, "08:35", "09:25"), new Horario(Dia.Sexta, "09:40", "10:30"), new Horario(Dia.Sexta, "10:30", "11:20") }, "102", Semestre.Semestre_1, "6872c"));
            turmas.Add(new Turma(ass, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20"), new Horario(Dia.Quinta, "09:40", "10:30"), new Horario(Dia.Quinta, "10:30", "11:20") }, "68", Semestre.Semestre_2, "6891b"));
            turmas.Add(new Turma(programacaoLinear, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20"), new Horario(Dia.Segunda, "11:20", "12:10"), new Horario(Dia.Quarta, "09:40", "10:30"), new Horario(Dia.Quarta, "10:30", "11:20"), new Horario(Dia.Quarta, "11:20", "12:10") }, "102", Semestre.Semestre_2, "6889b"));

            turmas.Add(new Turma(mf2, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00") }, "68", Semestre.Semestre_1, 1684));
            //turmas.Add(new Turma(eng4, new List<Horario>() { new Horario(Dia.Terça, "19:30", "20:20"), new Horario(Dia.Terça, "20:20", "21:10"), new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10"), new Horario(Dia.Quinta, "21:20", "22:10"), new Horario(Dia.Quinta, "22:10", "23:00") }, "136", Semestre.Semestre_1, 1837));
            turmas.Add(new Turma(eng4, new List<Horario>() { new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10"), new Horario(Dia.Quinta, "21:20", "22:10"), new Horario(Dia.Quinta, "22:10", "23:00") }, "136", Semestre.Semestre_1, "1837b"));
            turmas.Add(new Turma(ass, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00") }, "68", Semestre.Semestre_2, "5181b"));

            turmas.Add(new Turma(organizacaoComputadores, new List<Horario>() { new Horario(Dia.Sabado, "13:30", "14:20"), new Horario(Dia.Sabado, "14:20", "15:10"), new Horario(Dia.Sabado, "15:30", "16:20") }, "102", Semestre.Anual, 1452));
            turmas.Add(new Turma(sd, new List<Horario>() { new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00") }, "68", Semestre.Anual, 1487));
            turmas.Add(new Turma(eng2, new List<Horario>() { new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Anual, 1470));

            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Terça, "07:45", "08:35"), new Horario(Dia.Quarta, "07:45", "08:35"), new Horario(Dia.Quarta, "08:35", "09:25") }, "136", Semestre.Anual, 1590));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Terça, "08:35", "09:25"), new Horario(Dia.Quarta, "07:45", "08:35"), new Horario(Dia.Quarta, "08:35", "09:25") }, "136", Semestre.Anual, "1590b"));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10") }, "68", Semestre.Semestre_2, 1640));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10") }, "68", Semestre.Semestre_2, "1640b"));
            turmas.Add(new Turma(fundProgramacao, new List<Horario>() { new Horario(Dia.Quarta, "09:40", "10:30"), new Horario(Dia.Quarta, "10:30", "11:20"), new Horario(Dia.Quarta, "11:20", "12:10") }, "51", Semestre.Semestre_2, 2536));
            turmas.Add(new Turma(fundProgramacao, new List<Horario>() { new Horario(Dia.Quarta, "15:30", "16:20"), new Horario(Dia.Quarta, "16:20", "17:10"), new Horario(Dia.Quarta, "17:10", "18:00") }, "51", Semestre.Semestre_2, "2536b"));
            turmas.Add(new Turma(labProgramacao, new List<Horario>() { new Horario(Dia.Quarta, "13:30", "14:20") }, "17", Semestre.Semestre_2, 2541));
            turmas.Add(new Turma(labProgramacao, new List<Horario>() { new Horario(Dia.Quarta, "07:45", "08:35") }, "17", Semestre.Semestre_2, "2541b"));
            turmas.Add(new Turma(labProgramacao, new List<Horario>() { new Horario(Dia.Quinta, "13:30", "14:20") }, "17", Semestre.Semestre_2, "2541c"));
            turmas.Add(new Turma(labProgramacao, new List<Horario>() { new Horario(Dia.Quinta, "07:45", "08:35") }, "17", Semestre.Semestre_2, "2541d"));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Sexta, "09:40", "10:30"), new Horario(Dia.Sexta, "10:30", "11:20"), new Horario(Dia.Quarta, "15:30", "16:20") }, "68", Semestre.Anual, "208"));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Quarta, "16:20", "17:10"), new Horario(Dia.Sexta, "09:40", "10:30"), new Horario(Dia.Sexta, "10:30", "11:20") }, "68", Semestre.Anual, "208c"));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Quarta, "17:10", "18:00"), new Horario(Dia.Sexta, "09:40", "10:30"), new Horario(Dia.Sexta, "10:30", "11:20") }, "68", Semestre.Anual, "208d"));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Segunda, "15:30", "16:20"), new Horario(Dia.Segunda, "16:20", "17:10"), new Horario(Dia.Terça, "09:40", "10:30") }, "68", Semestre.Anual, "208d"));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Segunda, "15:30", "16:20"), new Horario(Dia.Segunda, "16:20", "17:10"), new Horario(Dia.Terça, "10:30", "11:20") }, "68", Semestre.Anual, "208e"));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Segunda, "15:30", "16:20"), new Horario(Dia.Segunda, "16:20", "17:10"), new Horario(Dia.Terça, "11:20", "12:10") }, "68", Semestre.Anual, "208f"));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Quarta, "09:40", "10:30"), new Horario(Dia.Quarta, "10:30", "11:20"), new Horario(Dia.Quarta, "11:20", "12:10") }, "68", Semestre.Anual, "1512b"));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Quarta, "08:35", "09:25"), new Horario(Dia.Quarta, "09:40", "10:30"), new Horario(Dia.Quarta, "10:30", "11:20") }, "68", Semestre.Anual, "1512c"));


            turmas.Add(new Turma(matematicaComputacional, new List<Horario>() { new Horario(Dia.Segunda, "15:30", "16:20"), new Horario(Dia.Segunda, "16:20", "17:10"), new Horario(Dia.Quinta, "13:30", "14:20"), new Horario(Dia.Quinta, "14:20", "15:10") }, "68", Semestre.Semestre_2, "6900b"));
            turmas.Add(new Turma(progInterfaceHardSoft, new List<Horario>() { new Horario(Dia.Terça, "13:30", "14:20"), new Horario(Dia.Terça, "14:20", "15:10"), new Horario(Dia.Sexta, "07:45", "08:35"), new Horario(Dia.Sexta, "08:35", "09:25") }, "68", Semestre.Semestre_1, "6894b"));
            turmas.Add(new Turma(estrutura, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Sabado, "13:30", "14:20"), new Horario(Dia.Sabado, "14:20", "15:10") }, "68", Semestre.Semestre_2, 7256));
            turmas.Add(new Turma(estrutura, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Sabado, "13:30", "14:20"), new Horario(Dia.Sabado, "14:20", "15:10") }, "68", Semestre.Semestre_2, "7256b"));
            turmas.Add(new Turma(estrutura, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Sabado, "13:30", "14:20"), new Horario(Dia.Sabado, "14:20", "15:10") }, "68", Semestre.Semestre_2, "7256c"));
            turmas.Add(new Turma(estrutura, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Sabado, "13:30", "14:20"), new Horario(Dia.Sabado, "14:20", "15:10") }, "68", Semestre.Semestre_2, "7256d"));
            turmas.Add(new Turma(bd1, new List<Horario>() { new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10"), new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00") }, "68", Semestre.Semestre_2, 7331));
            turmas.Add(new Turma(fundProgramacao, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00"), new Horario(Dia.Sabado, "13:30", "14:20"), new Horario(Dia.Sabado, "14:20", "15:10") }, "102", Semestre.Semestre_1, "7239b"));
            turmas.Add(new Turma(fundProgramacao, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00"), new Horario(Dia.Sabado, "13:30", "14:20"), new Horario(Dia.Sabado, "14:20", "15:10") }, "102", Semestre.Semestre_1, "7239c"));
            turmas.Add(new Turma(fundProgramacao, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00"), new Horario(Dia.Sabado, "13:30", "14:20"), new Horario(Dia.Sabado, "14:20", "15:10") }, "102", Semestre.Semestre_1, "7239d"));
            turmas.Add(new Turma(progSistemas, new List<Horario>() { new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00"), new Horario(Dia.Sabado, "15:30", "16:20"), new Horario(Dia.Sabado, "16:20", "17:10") }, "102", Semestre.Semestre_1, 7328));
            turmas.Add(new Turma(so, new List<Horario>() { new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10"), new Horario(Dia.Sabado, "13:30", "14:20"), new Horario(Dia.Sabado, "14:20", "15:10") }, "68", Semestre.Semestre_1, 7329));
            turmas.Add(new Turma(arq, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00") }, "68", Semestre.Semestre_1, "5178b"));
            turmas.Add(new Turma(ihc, new List<Horario>() { new Horario(Dia.Terça, "19:30", "20:20"), new Horario(Dia.Terça, "20:20", "21:10"), new Horario(Dia.Quinta, "21:20", "22:10"), new Horario(Dia.Quinta, "22:10", "23:00") }, "68", Semestre.Semestre_2, "5183b"));
            turmas.Add(new Turma(logica, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00") }, "68", Semestre.Semestre_1, "5200b"));
            turmas.Add(new Turma(assembly, new List<Horario>() { new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00") }, "68", Semestre.Semestre_2, "5193b"));
            turmas.Add(new Turma(concorrente, new List<Horario>() { new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00"), new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Sexta, "20:20", "21:10") }, "68", Semestre.Semestre_2, "5205b"));
            turmas.Add(new Turma(paa, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Semestre_2, "5184b"));
            turmas.Add(new Turma(paa, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Semestre_2, "5184c"));
            turmas.Add(new Turma(algoritmosEProg, new List<Horario>() { new Horario(Dia.Segunda, "13:30", "14:20"), new Horario(Dia.Segunda, "14:20", "15:10"), new Horario(Dia.Terça, "15:30", "16:20") }, "136", Semestre.Anual, 5259));
            turmas.Add(new Turma(algoritmosEProg, new List<Horario>() { new Horario(Dia.Segunda, "13:30", "14:20"), new Horario(Dia.Segunda, "14:20", "15:10"), new Horario(Dia.Terça, "16:20", "17:10") }, "136", Semestre.Anual, "5259b"));
            turmas.Add(new Turma(algoritmosEProg, new List<Horario>() { new Horario(Dia.Quarta, "13:30", "14:20"), new Horario(Dia.Quarta, "14:20", "15:10"), new Horario(Dia.Quarta, "15:30", "16:20") }, "136", Semestre.Anual, "5259c"));
            turmas.Add(new Turma(algoritmosEProg, new List<Horario>() { new Horario(Dia.Quarta, "13:30", "14:20"), new Horario(Dia.Quarta, "14:20", "15:10"), new Horario(Dia.Quarta, "16:20", "17:10") }, "136", Semestre.Anual, "5259d"));
            turmas.Add(new Turma(algoritmosEProg, new List<Horario>() { new Horario(Dia.Quinta, "15:30", "16:20"), new Horario(Dia.Quinta, "16:20", "17:10"), new Horario(Dia.Sexta, "15:30", "16:20") }, "136", Semestre.Anual, "5259e"));
            turmas.Add(new Turma(algoritmosEProg, new List<Horario>() { new Horario(Dia.Quinta, "15:30", "16:20"), new Horario(Dia.Quinta, "16:20", "17:10"), new Horario(Dia.Sexta, "16:20", "17:10") }, "136", Semestre.Anual, "5259f"));
            turmas.Add(new Turma(algoritmosEProg, new List<Horario>() { new Horario(Dia.Segunda, "15:30", "16:20"), new Horario(Dia.Segunda, "16:20", "17:10"), new Horario(Dia.Quarta, "15:30", "16:20"), new Horario(Dia.Quarta, "16:20", "17:10"), new Horario(Dia.Quinta, "15:30", "16:20"), new Horario(Dia.Quinta, "16:20", "17:10") }, "102", Semestre.Semestre_1, 4549));
            turmas.Add(new Turma(algoritmosEProg, new List<Horario>() { new Horario(Dia.Segunda, "15:30", "16:20"), new Horario(Dia.Segunda, "16:20", "17:10"), new Horario(Dia.Quarta, "13:30", "14:20"), new Horario(Dia.Quarta, "14:20", "15:10"), new Horario(Dia.Quarta, "15:30", "16:20"), new Horario(Dia.Quarta, "16:20", "17:10") }, "102", Semestre.Semestre_1, "4549b"));
            turmas.Add(new Turma(bd1, new List<Horario>() { new Horario(Dia.Segunda, "15:30", "16:20"), new Horario(Dia.Segunda, "16:20", "17:10"), new Horario(Dia.Quarta, "13:30", "14:20"), new Horario(Dia.Quarta, "14:20", "15:10"), new Horario(Dia.Quarta, "15:30", "16:20"), new Horario(Dia.Quarta, "16:20", "17:10") }, "68", Semestre.Semestre_2, 4573));
            turmas.Add(new Turma(estrutura, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00"), new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Semestre_2, 4553));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00"), new Horario(Dia.Sabado, "13:30", "14:20"), new Horario(Dia.Sabado, "14:20", "15:10") }, "102", Semestre.Anual, 4309));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00"), new Horario(Dia.Sabado, "13:30", "14:20"), new Horario(Dia.Sabado, "14:20", "15:10") }, "102", Semestre.Anual, "4309b"));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00"), new Horario(Dia.Sabado, "13:30", "14:20"), new Horario(Dia.Sabado, "14:20", "15:10") }, "102", Semestre.Anual, "4309c"));
            turmas.Add(new Turma(fundComputacao, new List<Horario>() { new Horario(Dia.Segunda, "15:30", "16:20"), new Horario(Dia.Segunda, "16:20", "17:10"), new Horario(Dia.Segunda, "17:10", "18:00") }, "51", Semestre.Semestre_2, "2536c"));
            turmas.Add(new Turma(fundProgramacao, new List<Horario>() { new Horario(Dia.Sexta, "09:40", "10:30"), new Horario(Dia.Sexta, "10:30", "11:20"), new Horario(Dia.Sexta, "11:20", "12:10") }, "51", Semestre.Semestre_1, 6633));
            turmas.Add(new Turma(fundProgramacao, new List<Horario>() { new Horario(Dia.Quinta, "15:30", "16:20"), new Horario(Dia.Quinta, "16:20", "17:10"), new Horario(Dia.Quinta, "17:10", "18:00") }, "51", Semestre.Semestre_1, "6633b"));
            turmas.Add(new Turma(infoAgro, new List<Horario>() { new Horario(Dia.Terça, "13:30", "14:20"), new Horario(Dia.Terça, "14:20", "15:10"), new Horario(Dia.Quinta, "13:30", "14:20"), new Horario(Dia.Quinta, "14:20", "15:10") }, "68", Semestre.Semestre_2, 4460));
            turmas.Add(new Turma(infoAgro, new List<Horario>() { new Horario(Dia.Terça, "13:30", "14:20"), new Horario(Dia.Terça, "14:20", "15:10"), new Horario(Dia.Quinta, "13:30", "14:20"), new Horario(Dia.Quinta, "14:20", "15:10") }, "68", Semestre.Semestre_2, "4460b"));
            turmas.Add(new Turma(infoAgro, new List<Horario>() { new Horario(Dia.Terça, "13:30", "14:20"), new Horario(Dia.Terça, "14:20", "15:10"), new Horario(Dia.Quinta, "13:30", "14:20"), new Horario(Dia.Quinta, "14:20", "15:10") }, "68", Semestre.Semestre_2, "4460c"));
            turmas.Add(new Turma(infoAgro, new List<Horario>() { new Horario(Dia.Terça, "13:30", "14:20"), new Horario(Dia.Terça, "14:20", "15:10"), new Horario(Dia.Quinta, "13:30", "14:20"), new Horario(Dia.Quinta, "14:20", "15:10") }, "68", Semestre.Semestre_2, "4460d"));
            turmas.Add(new Turma(secretariado, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00") }, "68", Semestre.Semestre_2, 3411));
            turmas.Add(new Turma(secretariado, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00") }, "68", Semestre.Semestre_2, "3411b"));
            turmas.Add(new Turma(cd2, new List<Horario>() { new Horario(Dia.Segunda, "07:45", "08:35"), new Horario(Dia.Segunda, "08:35", "09:25"), new Horario(Dia.Quarta, "07:45", "08:35"), new Horario(Dia.Quarta, "08:35", "09:25") }, "68", Semestre.Semestre_1, "6882c"));
            turmas.Add(new Turma(cd2, new List<Horario>() { new Horario(Dia.Segunda, "07:45", "08:35"), new Horario(Dia.Segunda, "08:35", "09:25"), new Horario(Dia.Quarta, "07:45", "08:35"), new Horario(Dia.Quarta, "08:35", "09:25") }, "68", Semestre.Semestre_1, "6882d"));
            turmas.Add(new Turma(pss, new List<Horario>() { new Horario(Dia.Terça, "07:45", "08:35"), new Horario(Dia.Terça, "08:35", "09:25"), new Horario(Dia.Quinta, "13:30", "14:20"), new Horario(Dia.Quinta, "14:20", "15:10") }, "68", Semestre.Semestre_1, 6885));
            turmas.Add(new Turma(pss, new List<Horario>() { new Horario(Dia.Terça, "07:45", "08:35"), new Horario(Dia.Terça, "08:35", "09:25"), new Horario(Dia.Quinta, "13:30", "14:20"), new Horario(Dia.Quinta, "14:20", "15:10") }, "68", Semestre.Semestre_1, "6885b"));
            turmas.Add(new Turma(cd1, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Semestre_2, "5174d"));


            #region Professores

            var constantino = new Professor(new List<Turma>(getTurmasDadoDisciplina(turmas, new List<Disciplina>() { modelagem, metodosModelagem })), new List<Turma>(getTurmasDadoDisciplina(turmas, new List<Disciplina>() { })), "Ademir Aparecido Constantino", new List<Turma>(), "8");
            var carniel = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Ademir Carniel", new List<Turma>(), "8");
            var anderson = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Anderson Faustino da Silva", new List<Turma>(), "8");
            var airton = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Airton Marco Polidório", new List<Turma>(), "8");
            var sica = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Carlos Benedito Sica de Toledo", new List<Turma>(), "8");
            var clelia = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Clélia Franco", new List<Turma>(), "8");
            var dante = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Dante Alves Medeiros Filho", new List<Turma>(), "8");
            var donizete = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Donizete Carlos Bruzarosco", new List<Turma>(), "8");
            var elisa = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Elisa Hatsue Moriya Huzita", new List<Turma>(), "8");
            var elvio = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Elvio João Leonardo", new List<Turma>(), "8");
            var flavio = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Flávio Arnaldo Braga da Silva", new List<Turma>(), "8");
            var itana = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Itana Maria de Souza Gimenes", new List<Turma>(), "8");
            var angelo = new Professor(new List<Turma>() { }, new List<Turma>() { }, "João Angelo Martini", new List<Turma>(), "8");
            var ze = new Professor(new List<Turma>() { }, new List<Turma>() { }, "José Roberto Vasconcelos", new List<Turma>(), "8");
            var josiane = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Josiane Melchiori Pinheiro", new List<Turma>(), "8");
            var jucelia = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Jucélia Geni Pereira Fregoneis", new List<Turma>(), "8");
            var linnyer = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Linnyer Beatrys Ruiz", new List<Turma>(), "8");
            var luciana = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Luciana Andréia Fondazzi Martimiano", new List<Turma>(), "8");
            var wigarashi = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Wagner Igarashi", new List<Turma>(), "8");
            var malbarbo = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Marco Aurélio Lopes Barbosa", new List<Turma>(), "8");
            var madalena = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Maria Madalena Dias", new List<Turma>(), "8");
            var yandre = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Yandre Maldonado e Gomes da Costa", new List<Turma>(), "8");
            var valeria = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Valéria Delisandra Feltrim", new List<Turma>(), "8");
            var thelma = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Thelma Elita Colanzi Lopes", new List<Turma>(), "8");
            var raqueline = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Raqueline Ritter de Moura", new List<Turma>(), "8");
            var ronaldo = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Ronaldo Augusto de Lara Gonçalves", new List<Turma>(), "8");
            var sergio = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Sérgio Roberto P. da Silva", new List<Turma>(), "8");
            var tania = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Tânia Fátima Calvi Tait", new List<Turma>(), "8");
            var wesley = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Wesley Romão", new List<Turma>(), "8");
            var andre = new Professor(new List<Turma>() { }, new List<Turma>() { }, "André Barbosa Verona", new List<Turma>(), "8");
            var franklin = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Franklin César Flores", new List<Turma>(), "8");
            var nardenio = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Nardênio Almeida Martins", new List<Turma>(), "8");
            var uber = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Flávio Uber", new List<Turma>(), "8");
            var heloise = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Heloise  Manica", new List<Turma>(), "8");
            var munif = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Munif Gebara Júnior", new List<Turma>(), "8");
            var balancieri = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Renato Balancieri", new List<Turma>(), "8");
            var juliana = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Juliana Keiko Yamaguchi", new List<Turma>(), "8");
            var rodrigo = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Rodrigo Lankaites Pinheiro", new List<Turma>(), "8");
            var edson = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Edson Alves de Oliveira Júniuor", new List<Turma>(), "8");
            var carlosFransley = new Professor(new List<Turma>() { }, new List<Turma>() { }, "Carlos Fransley", new List<Turma>(), "8");

            professores.Add(constantino);
            professores.Add(carniel);
            professores.Add(anderson);
            professores.Add(airton);
            professores.Add(sica);
            professores.Add(clelia);
            professores.Add(dante);
            professores.Add(donizete);
            professores.Add(elisa);
            professores.Add(elvio);
            professores.Add(flavio);
            professores.Add(itana);
            professores.Add(angelo);
            professores.Add(ze);
            professores.Add(josiane);
            professores.Add(jucelia);
            professores.Add(linnyer);
            professores.Add(luciana);
            professores.Add(wigarashi);
            professores.Add(malbarbo);
            professores.Add(madalena);
            professores.Add(yandre);
            professores.Add(valeria);
            professores.Add(thelma);
            professores.Add(raqueline);
            professores.Add(ronaldo);
            professores.Add(sergio);
            professores.Add(wesley);
            professores.Add(franklin);
            professores.Add(nardenio);
            professores.Add(uber);
            professores.Add(heloise);
            professores.Add(munif);
            professores.Add(balancieri);
            professores.Add(juliana);
            professores.Add(rodrigo);
            professores.Add(edson);
            professores.Add(carlosFransley);
            #endregion

            #region Turmas Consolidadas
            turmas.Add(new Turma(modelagem, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20"), new Horario(Dia.Terça, "09:40", "10:30"), new Horario(Dia.Terça, "10:30", "11:20"), new Horario(Dia.Sexta, "07:45", "08:35"), new Horario(Dia.Sexta, "08:45", "09:25") }, "102", Semestre.Semestre_2, 6903));
            turmas.Add(new Turma(topicosPesquisa, new List<Horario>() { new Horario(Dia.Segunda, "13:30", "14:20"), new Horario(Dia.Segunda, "14:20", "15:10"), new Horario(Dia.Quarta, "13:30", "14:20"), new Horario(Dia.Quarta, "14:20", "15:10"), new Horario(Dia.Quinta, "09:40", "10:30"), new Horario(Dia.Quinta, "10:30", "11:20") }, "102", Semestre.Semestre_2, 1760));
            turmas.Add(new Turma(modelagem, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00") }, "102", Semestre.Semestre_2, 5199));

            turmas.Add(new Turma(matematicaComputacional, new List<Horario>() { new Horario(Dia.Segunda, "15:30", "16:20"), new Horario(Dia.Segunda, "16:20", "17:10"), new Horario(Dia.Quinta, "13:30", "14:20"), new Horario(Dia.Quinta, "14:20", "15:10") }, "68", Semestre.Semestre_2, 6900));
            turmas.Add(new Turma(mf1, new List<Horario>() { new Horario(Dia.Terça, "15:30", "16:20"), new Horario(Dia.Terça, "16:20", "17:10"), new Horario(Dia.Quinta, "15:30", "16:20"), new Horario(Dia.Quinta, "16:20", "17:10") }, "68", Semestre.Semestre_2, 1744));
            turmas.Add(new Turma(algoritmos, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00"), new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Sexta, "20:20", "21:10") }, "102", Semestre.Semestre_2, 5175));
            turmas.Add(new Turma(mf1, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Sexta, "20:20", "21:10") }, "68", Semestre.Semestre_1, 5237));

            turmas.Add(new Turma(arq, new List<Horario>() { new Horario(Dia.Segunda, "07:45", "08:35"), new Horario(Dia.Segunda, "08:35", "09:25"), new Horario(Dia.Quinta, "15:30", "16:20"), new Horario(Dia.Quinta, "16:20", "17:10"), new Horario(Dia.Sexta, "07:45", "08:35"), new Horario(Dia.Sexta, "08:35", "09:25") }, "102", Semestre.Semestre_2, 6887));
            turmas.Add(new Turma(arq2, new List<Horario>() { new Horario(Dia.Terça, "07:45", "08:35"), new Horario(Dia.Terça, "08:35", "09:25"), new Horario(Dia.Quinta, "07:45", "08:35"), new Horario(Dia.Quinta, "08:35", "09:25") }, "68", Semestre.Semestre_1, 6895));
            turmas.Add(new Turma(compiladores, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Semestre_2, 5204));
            turmas.Add(new Turma(concorrente, new List<Horario>() { new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00"), new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Sexta, "20:20", "21:10") }, "68", Semestre.Semestre_2, 5205));

            turmas.Add(new Turma(automacao, new List<Horario>() { new Horario(Dia.Quarta, "09:40", "10:30"), new Horario(Dia.Quarta, "10:30", "11:20"), new Horario(Dia.Quarta, "11:20", "12:10") }, "102", Semestre.Anual, 1034));
            turmas.Add(new Turma(cd2, new List<Horario>() { new Horario(Dia.Segunda, "07:45", "08:35"), new Horario(Dia.Segunda, "08:35", "09:25"), new Horario(Dia.Quarta, "07:45", "08:35"), new Horario(Dia.Quarta, "08:35", "09:25") }, "68", Semestre.Semestre_1, 6882));
            turmas.Add(new Turma(fundamentosEletronica, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20"), new Horario(Dia.Terça, "07:45", "08:35"), new Horario(Dia.Terça, "08:35", "09:25"), new Horario(Dia.Sexta, "09:40", "10:30"), new Horario(Dia.Sexta, "10:30", "11:20") }, "102", Semestre.Semestre_1, 6872));
            turmas.Add(new Turma(arq, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Sabado, "15:30", "16:20"), new Horario(Dia.Sabado, "16:20", "17:10") }, "68", Semestre.Semestre_2, 7330));
            turmas.Add(new Turma(arq, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00") }, "68", Semestre.Semestre_1, 5178));

            turmas.Add(new Turma(fundamentosEletronica, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20"), new Horario(Dia.Terça, "07:45", "08:35"), new Horario(Dia.Terça, "08:35", "09:25"), new Horario(Dia.Sexta, "09:40", "10:30"), new Horario(Dia.Sexta, "10:30", "11:20") }, "102", Semestre.Semestre_1, "6872d"));
            turmas.Add(new Turma(simulacaoSistemas, new List<Horario>() { new Horario(Dia.Terça, "13:30", "14:20"), new Horario(Dia.Terça, "14:20", "15:10"), new Horario(Dia.Quinta, "07:45", "08:35"), new Horario(Dia.Quinta, "08:35", "09:25") }, "68", Semestre.Semestre_2, 1762));
            turmas.Add(new Turma(sd, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Sexta, "20:20", "21:10") }, "68", Semestre.Semestre_1, 5197));

            turmas.Add(new Turma(automacao, new List<Horario>() { new Horario(Dia.Quarta, "15:30", "16:20"), new Horario(Dia.Quarta, "16:20", "17:10"), new Horario(Dia.Quarta, "17:10", "18:00") }, "102", Semestre.Anual, 1034));
            turmas.Add(new Turma(ia, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Sexta, "20:20", "21:10") }, "68", Semestre.Semestre_2, "5236b"));

            turmas.Add(new Turma(fundProgramacao, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00"), new Horario(Dia.Sabado, "13:30", "14:20"), new Horario(Dia.Sabado, "14:20", "15:10") }, "102", Semestre.Semestre_1, 7239));

            turmas.Add(new Turma(cg, new List<Horario>() { new Horario(Dia.Quinta, "09:40", "10:30"), new Horario(Dia.Quinta, "10:30", "11:20"), new Horario(Dia.Quinta, "11:20", "12:10"), new Horario(Dia.Sexta, "09:40", "10:30"), new Horario(Dia.Sexta, "10:30", "11:20"), new Horario(Dia.Sexta, "11:20", "12:10") }, "102", Semestre.Semestre_1, 1759));
            turmas.Add(new Turma(cg, new List<Horario>() { new Horario(Dia.Terça, "19:30", "20:20"), new Horario(Dia.Terça, "20:20", "21:10"), new Horario(Dia.Quinta, "21:20", "22:10"), new Horario(Dia.Quinta, "22:10", "23:00") }, "68", Semestre.Semestre_2, 5202));
            turmas.Add(new Turma(pesquisa, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Semestre_1, 5171));

            turmas.Add(new Turma(iss, new List<Horario>() { new Horario(Dia.Segunda, "07:45", "08:35"), new Horario(Dia.Segunda, "08:35", "09:25"), new Horario(Dia.Quinta, "09:40", "10:30"), new Horario(Dia.Quinta, "10:30", "11:20") }, "68", Semestre.Semestre_2, 6904));
            turmas.Add(new Turma(pss, new List<Horario>() { new Horario(Dia.Segunda, "07:45", "08:35"), new Horario(Dia.Segunda, "08:35", "09:25"), new Horario(Dia.Quinta, "09:40", "10:30"), new Horario(Dia.Quinta, "10:30", "11:20") }, "68", Semestre.Semestre_1, 6899));
            turmas.Add(new Turma(iss, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10") }, "68", Semestre.Semestre_2, 5191));
            turmas.Add(new Turma(iss, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00") }, "68", Semestre.Semestre_1, 5186));
            turmas.Add(new Turma(tcc, new List<Horario>() { new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00") }, "68", Semestre.Anual, 3513));

            turmas.Add(new Turma(ambienteDesenv, new List<Horario>() { new Horario(Dia.Terça, "13:30", "14:20"), new Horario(Dia.Terça, "14:20", "15:10"), new Horario(Dia.Quinta, "15:30", "16:20"), new Horario(Dia.Quinta, "16:20", "17:10") }, "68", Semestre.Semestre_2, 1753));
            turmas.Add(new Turma(progSistWeb, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Sexta, "20:20", "21:10") }, "68", Semestre.Semestre_1, 5188));
            turmas.Add(new Turma(poo, new List<Horario>() { new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "22:20", "21:10"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00") }, "68", Semestre.Semestre_1, 5190));
            turmas.Add(new Turma(ass, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20"), new Horario(Dia.Quinta, "09:40", "10:30"), new Horario(Dia.Quinta, "10:30", "11:20") }, "68", Semestre.Semestre_2, 6891));

            turmas.Add(new Turma(ti, new List<Horario>() { new Horario(Dia.Segunda, "13:30", "14:20"), new Horario(Dia.Segunda, "14:20", "15:10"), new Horario(Dia.Quinta, "09:40", "10:30"), new Horario(Dia.Quinta, "10:30", "11:20") }, "68", Semestre.Semestre_1, 6874));
            turmas.Add(new Turma(projetoSoftware, new List<Horario>() { new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Sexta, "20:20", "21:10") }, "68", Semestre.Anual, 1498));
            turmas.Add(new Turma(pss, new List<Horario>() { new Horario(Dia.Terça, "19:30", "20:20"), new Horario(Dia.Terça, "20:20", "21:10"), new Horario(Dia.Quinta, "21:20", "22:10"), new Horario(Dia.Quinta, "22:10", "23:00") }, "68", Semestre.Semestre_1, 5176));

            turmas.Add(new Turma(cd1, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20"), new Horario(Dia.Terça, "07:45", "08:35"), new Horario(Dia.Terça, "08:35", "09:25"), new Horario(Dia.Sexta, "09:40", "10:30"), new Horario(Dia.Sexta, "10:30", "11:20") }, "102", Semestre.Semestre_2, 6878));
            turmas.Add(new Turma(redes, new List<Horario>() { new Horario(Dia.Quarta, "09:40", "10:30"), new Horario(Dia.Quarta, "10:30", "11:20") }, "68", Semestre.Anual, 1023));
            turmas.Add(new Turma(cd1, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Semestre_2, 5174));
            turmas.Add(new Turma(redes, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10") }, "68", Semestre.Semestre_2, 5203));

            turmas.Add(new Turma(so, new List<Horario>() { new Horario(Dia.Segunda, "13:30", "14:20"), new Horario(Dia.Segunda, "14:20", "15:10"), new Horario(Dia.Quarta, "07:45", "08:35"), new Horario(Dia.Quarta, "08:35", "09:25"), new Horario(Dia.Sexta, "09:40", "10:30"), new Horario(Dia.Sexta, "10:30", "11:20") }, "102", Semestre.Semestre_2, 6901));
            turmas.Add(new Turma(so, new List<Horario>() { new Horario(Dia.Terça, "19:30", "20:20"), new Horario(Dia.Terça, "20:20", "21:10"), new Horario(Dia.Quinta, "21:20", "22:10"), new Horario(Dia.Quinta, "22:10", "23:00") }, "68", Semestre.Semestre_1, 5196));


            turmas.Add(new Turma(arq2, new List<Horario>() { new Horario(Dia.Terça, "13:30", "14:20"), new Horario(Dia.Terça, "14:20", "15:10"), new Horario(Dia.Quinta, "09:40", "10:30"), new Horario(Dia.Quinta, "10:30", "11:20") }, "68", Semestre.Semestre_1, 1757));
            turmas.Add(new Turma(mf2, new List<Horario>() { new Horario(Dia.Segunda, "13:30", "14:20"), new Horario(Dia.Segunda, "14:20", "15:10"), new Horario(Dia.Quarta, "15:30", "16:20"), new Horario(Dia.Quarta, "16:20", "17:10") }, "68", Semestre.Semestre_2, 1754));
            turmas.Add(new Turma(algoritmos, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00"), new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Sexta, "20:20", "21:10") }, "102", Semestre.Semestre_2, "5175b"));
            turmas.Add(new Turma(mf2, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00") }, "68", Semestre.Semestre_1, 1754));

            turmas.Add(new Turma(estrutura, new List<Horario>() { new Horario(Dia.Segunda, "13:30", "14:20"), new Horario(Dia.Segunda, "14:20", "15:10"), new Horario(Dia.Terça, "09:40", "10:30"), new Horario(Dia.Terça, "10:30", "11:20"), new Horario(Dia.Sexta, "07:45", "08:35"), new Horario(Dia.Sexta, "08:35", "09:25") }, "102", Semestre.Semestre_1, 6884));
            turmas.Add(new Turma(algoritmos, new List<Horario>() { new Horario(Dia.Segunda, "07:45", "08:35"), new Horario(Dia.Segunda, "08:35", "09:25"), new Horario(Dia.Terça, "15:30", "16:20"), new Horario(Dia.Terça, "16:20", "17:10"), new Horario(Dia.Quinta, "07:45", "08:35"), new Horario(Dia.Quinta, "08:35", "09:25") }, "102", Semestre.Semestre_2, 6879));
            turmas.Add(new Turma(estrutura, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00") }, "102", Semestre.Semestre_1, 5180));

            turmas.Add(new Turma(pesquisa, new List<Horario>() { new Horario(Dia.Terça, "15:30", "16:20"), new Horario(Dia.Terça, "16:20", "17:10") }, "34", Semestre.Semestre_1, 6875));
            turmas.Add(new Turma(pesquisa, new List<Horario>() { new Horario(Dia.Quinta, "15:30", "16:20"), new Horario(Dia.Quinta, "16:20", "17:10") }, "34", Semestre.Semestre_1, "6875b"));
            turmas.Add(new Turma(bd1, new List<Horario>() { new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00") }, "68", Semestre.Semestre_2, 5182));
            turmas.Add(new Turma(bd2, new List<Horario>() { new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00"), new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Sexta, "20:20", "21:10"), }, "68", Semestre.Semestre_2, 5192));
            turmas.Add(new Turma(pesquisa, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "22:20", "21:10") }, "68", Semestre.Semestre_1, "5171b"));

            turmas.Add(new Turma(pesquisa, new List<Horario>() { new Horario(Dia.Terça, "07:45", "08:35"), new Horario(Dia.Terça, "08:35", "09:25") }, "68", Semestre.Anual, 1029));
            turmas.Add(new Turma(ass, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00") }, "68", Semestre.Semestre_2, 5181));
            turmas.Add(new Turma(eng4, new List<Horario>() { new Horario(Dia.Terça, "19:30", "20:20"), new Horario(Dia.Terça, "20:20", "21:10"), new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10"), new Horario(Dia.Quinta, "21:20", "22:10"), new Horario(Dia.Quinta, "22:10", "23:00") }, "136", Semestre.Semestre_1, 1837));

            turmas.Add(new Turma(cd1, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20"), new Horario(Dia.Terça, "07:45", "08:35"), new Horario(Dia.Terça, "08:35", "09:25"), new Horario(Dia.Sexta, "09:40", "10:30"), new Horario(Dia.Sexta, "10:30", "11:20") }, "102", Semestre.Semestre_2, "6878b"));
            turmas.Add(new Turma(cd1, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Semestre_2, "5174b"));

            turmas.Add(new Turma(computabilidade, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20"), new Horario(Dia.Terça, "09:40", "10:30"), new Horario(Dia.Terça, "10:30", "11:20") }, "68", Semestre.Semestre_1, 6896));
            turmas.Add(new Turma(programacaoLinear, new List<Horario>() { new Horario(Dia.Quinta, "07:45", "08:35"), new Horario(Dia.Quinta, "08:35", "09:25"), new Horario(Dia.Sexta, "07:45", "08:35"), new Horario(Dia.Sexta, "08:35", "09:25") }, "68", Semestre.Semestre_1, 1761));
            turmas.Add(new Turma(paa, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20"), new Horario(Dia.Segunda, "11:20", "12:10"), new Horario(Dia.Quarta, "09:40", "10:30"), new Horario(Dia.Quarta, "10:30", "11:20"), new Horario(Dia.Quarta, "11:20", "12:10") }, "102", Semestre.Semestre_2, 6889));
            turmas.Add(new Turma(paa, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Semestre_2, 5184));

            turmas.Add(new Turma(paradigmaImpOO, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Semestre_2, "5185b"));
            turmas.Add(new Turma(pss, new List<Horario>() { new Horario(Dia.Terça, "19:30", "20:20"), new Horario(Dia.Terça, "20:20", "21:10"), new Horario(Dia.Quinta, "21:20", "22:10"), new Horario(Dia.Quinta, "22:10", "23:00") }, "68", Semestre.Semestre_1, "5176b"));

            turmas.Add(new Turma(computabilidade, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20"), new Horario(Dia.Terça, "09:40", "10:30"), new Horario(Dia.Terça, "10:30", "11:20") }, "68", Semestre.Semestre_1, "6896b"));
            turmas.Add(new Turma(lingFormaisAutomatos, new List<Horario>() { new Horario(Dia.Segunda, "15:30", "16:20"), new Horario(Dia.Segunda, "16:20", "17:10"), new Horario(Dia.Segunda, "17:10", "18:00"), new Horario(Dia.Terça, "15:30", "16:20"), new Horario(Dia.Terça, "16:20", "17:10"), new Horario(Dia.Terça, "17:10", "18:00") }, "102", Semestre.Semestre_1, 6883));
            turmas.Add(new Turma(lingFormaisAutomatos, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Terça, "19:30", "20:20"), new Horario(Dia.Terça, "20:20", "21:10"), new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00") }, "102", Semestre.Semestre_1, 5195));

            turmas.Add(new Turma(redes, new List<Horario>() { new Horario(Dia.Quarta, "09:40", "10:30"), new Horario(Dia.Quarta, "10:30", "11:20") }, "68", Semestre.Anual, "1023b"));
            turmas.Add(new Turma(sd, new List<Horario>() { new Horario(Dia.Terça, "15:30", "16:20"), new Horario(Dia.Terça, "16:20", "17:10"), new Horario(Dia.Terça, "17:10", "18:00"), new Horario(Dia.Quinta, "15:30", "16:20"), new Horario(Dia.Quinta, "16:20", "17:10"), new Horario(Dia.Quinta, "17:10", "18:00") }, "102", Semestre.Semestre_1, 1756));
            turmas.Add(new Turma(redes, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10") }, "68", Semestre.Semestre_2, "5203b"));
            turmas.Add(new Turma(sd, new List<Horario>() { new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10"), new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Sexta, "20:20", "21:10") }, "68", Semestre.Semestre_1, 1838));

            turmas.Add(new Turma(grafos, new List<Horario>() { new Horario(Dia.Quarta, "09:40", "10:30"), new Horario(Dia.Quarta, "10:30", "11:20"), new Horario(Dia.Sexta, "09:40", "10:30"), new Horario(Dia.Sexta, "10:30", "11:20") }, "68", Semestre.Semestre_1, 6898));
            turmas.Add(new Turma(lp, new List<Horario>() { new Horario(Dia.Quinta, "13:30", "14:20"), new Horario(Dia.Quinta, "14:20", "15:10") }, "68", Semestre.Anual, 1028));
            turmas.Add(new Turma(logica, new List<Horario>() { new Horario(Dia.Terça, "07:45", "08:35"), new Horario(Dia.Terça, "08:35", "09:25"), new Horario(Dia.Quinta, "07:45", "08:35"), new Horario(Dia.Quinta, "08:35", "09:25") }, "68", Semestre.Semestre_2, 6902));
            turmas.Add(new Turma(grafos, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10") }, "68", Semestre.Semestre_1, 5189));
            turmas.Add(new Turma(logica, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00") }, "68", Semestre.Semestre_1, 5200));

            turmas.Add(new Turma(bd1, new List<Horario>() { new Horario(Dia.Terça, "09:40", "10:30"), new Horario(Dia.Terça, "10:30", "11:20"), new Horario(Dia.Quinta, "07:45", "08:35"), new Horario(Dia.Quinta, "08:35", "09:25") }, "68", Semestre.Semestre_2, 6892));
            turmas.Add(new Turma(bd2, new List<Horario>() { new Horario(Dia.Terça, "09:40", "10:30"), new Horario(Dia.Terça, "10:30", "11:20"), new Horario(Dia.Terça, "11:20", "12:10"), new Horario(Dia.Quinta, "15:30", "16:20"), new Horario(Dia.Quinta, "16:20", "17:10"), new Horario(Dia.Quinta, "17:10", "18:00") }, "102", Semestre.Semestre_2, 1752));
            turmas.Add(new Turma(ti, new List<Horario>() { new Horario(Dia.Segunda, "13:30", "14:20"), new Horario(Dia.Segunda, "14:20", "15:10"), new Horario(Dia.Quinta, "09:40", "10:30"), new Horario(Dia.Quinta, "10:30", "11:20") }, "68", Semestre.Semestre_1, "6874b"));
            turmas.Add(new Turma(ti, new List<Horario>() { new Horario(Dia.Terça, "19:30", "20:20"), new Horario(Dia.Terça, "20:20", "21:10"), new Horario(Dia.Quinta, "21:20", "22:10"), new Horario(Dia.Quinta, "22:10", "23:00") }, "68", Semestre.Semestre_1, 5167));


            turmas.Add(new Turma(bd1, new List<Horario>() { new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10"), new Horario(Dia.Quarta, "21:20", "22:10") }, "102", Semestre.Anual, 2495));

            turmas.Add(new Turma(cd1, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20"), new Horario(Dia.Terça, "07:45", "08:35"), new Horario(Dia.Terça, "08:35", "09:25"), new Horario(Dia.Sexta, "09:40", "10:30"), new Horario(Dia.Sexta, "10:30", "11:20") }, "102", Semestre.Semestre_2, "6878c"));
            turmas.Add(new Turma(cd2, new List<Horario>() { new Horario(Dia.Segunda, "07:45", "08:35"), new Horario(Dia.Segunda, "08:35", "09:25"), new Horario(Dia.Quarta, "07:45", "08:35"), new Horario(Dia.Quarta, "08:35", "09:25") }, "68", Semestre.Semestre_1, "6882b"));
            turmas.Add(new Turma(cd1, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Semestre_2, "5174c"));

            turmas.Add(new Turma(bd1, new List<Horario>() { new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00") }, "68", Semestre.Semestre_2, "5182b"));
            turmas.Add(new Turma(ti, new List<Horario>() { new Horario(Dia.Terça, "19:30", "20:20"), new Horario(Dia.Terça, "20:20", "21:10"), new Horario(Dia.Quinta, "21:20", "22:10"), new Horario(Dia.Quinta, "22:10", "23:00") }, "68", Semestre.Semestre_1, "5167b"));

            turmas.Add(new Turma(algoritmos, new List<Horario>() { new Horario(Dia.Segunda, "07:45", "08:35"), new Horario(Dia.Segunda, "08:35", "09:25"), new Horario(Dia.Terça, "15:30", "16:20"), new Horario(Dia.Terça, "16:20", "17:10"), new Horario(Dia.Quinta, "07:45", "08:35"), new Horario(Dia.Quinta, "08:35", "09:25") }, "102", Semestre.Semestre_2, "6879b"));
            turmas.Add(new Turma(progInterfaceHardSoft, new List<Horario>() { new Horario(Dia.Terça, "13:30", "14:20"), new Horario(Dia.Terça, "14:20", "15:10"), new Horario(Dia.Sexta, "07:45", "08:35"), new Horario(Dia.Sexta, "08:35", "09:25") }, "68", Semestre.Semestre_1, 6894));
            turmas.Add(new Turma(estrutura, new List<Horario>() { new Horario(Dia.Segunda, "21:20", "22:10"), new Horario(Dia.Segunda, "22:10", "23:00"), new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00") }, "102", Semestre.Semestre_1, "5180b"));
            turmas.Add(new Turma(assembly, new List<Horario>() { new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00") }, "68", Semestre.Semestre_2, 5193));

            turmas.Add(new Turma(ia, new List<Horario>() { new Horario(Dia.Segunda, "15:30", "16:20"), new Horario(Dia.Segunda, "16:20", "17:10"), new Horario(Dia.Quarta, "13:30", "14:20"), new Horario(Dia.Quarta, "14:20", "15:10"), new Horario(Dia.Quinta, "13:30", "14:20"), new Horario(Dia.Quinta, "14:20", "15:10") }, "102", Semestre.Semestre_1, 1743));
            turmas.Add(new Turma(ihc, new List<Horario>() { new Horario(Dia.Terça, "13:30", "14:20"), new Horario(Dia.Terça, "14:20", "15:10"), new Horario(Dia.Quinta, "13:30", "14:20"), new Horario(Dia.Quinta, "14:20", "15:10") }, "68", Semestre.Semestre_2, 6890));
            turmas.Add(new Turma(ihc, new List<Horario>() { new Horario(Dia.Terça, "19:30", "20:20"), new Horario(Dia.Terça, "20:20", "21:10"), new Horario(Dia.Quinta, "21:20", "22:10"), new Horario(Dia.Quinta, "22:10", "23:00") }, "68", Semestre.Semestre_2, 5183));
            turmas.Add(new Turma(ia, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Sexta, "20:20", "21:10") }, "68", Semestre.Semestre_2, 5236));

            turmas.Add(new Turma(aspectosPsicologicos, new List<Horario>() { new Horario(Dia.Segunda, "15:30", "16:20"), new Horario(Dia.Segunda, "16:20", "17:10"), new Horario(Dia.Segunda, "17:10", "18:00") }, "68", Semestre.Anual, 1027));
            turmas.Add(new Turma(eng3, new List<Horario>() { new Horario(Dia.Quinta, "09:40", "10:30"), new Horario(Dia.Quarta, "10:30", "11:20"), new Horario(Dia.Quinta, "11:20", "12:10") }, "102", Semestre.Anual, 1030));
            turmas.Add(new Turma(qualidade, new List<Horario>() { new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Anual, 1485));
            turmas.Add(new Turma(tecAvancadasSI, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00") }, "68", Semestre.Anual, 1499));
            turmas.Add(new Turma(eng3, new List<Horario>() { new Horario(Dia.Terça, "19:30", "20:20"), new Horario(Dia.Terça, "20:20", "21:10"), new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10") }, "136", Semestre.Anual, 1227));
            turmas.Add(new Turma(informaticaESociedade, new List<Horario>() { new Horario(Dia.Quarta, "19:30", "20:20"), new Horario(Dia.Quarta, "20:20", "21:10") }, "34", Semestre.Anual, 5198));

            turmas.Add(new Turma(ord, new List<Horario>() { new Horario(Dia.Segunda, "13:30", "14:20"), new Horario(Dia.Segunda, "14:20", "15:10"), new Horario(Dia.Quarta, "13:30", "14:20"), new Horario(Dia.Quarta, "14:20", "15:10") }, "68", Semestre.Semestre_1, 6897));
            turmas.Add(new Turma(paradigmaImpOO, new List<Horario>() { new Horario(Dia.Segunda, "13:30", "14:20"), new Horario(Dia.Segunda, "14:20", "15:10"), new Horario(Dia.Quarta, "13:30", "14:20"), new Horario(Dia.Quarta, "14:20", "15:10") }, "68", Semestre.Semestre_2, 6888));
            turmas.Add(new Turma(ord, new List<Horario>() { new Horario(Dia.Terça, "19:30", "20:20"), new Horario(Dia.Terça, "20:20", "21:10"), new Horario(Dia.Quinta, "21:20", "22:10"), new Horario(Dia.Quinta, "22:10", "23:00") }, "68", Semestre.Semestre_1, 5187));
            turmas.Add(new Turma(paradigmaImpOO, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Semestre_2, 5185));

            turmas.Add(new Turma(compiladores, new List<Horario>() { new Horario(Dia.Quinta, "07:45", "08:35"), new Horario(Dia.Quinta, "08:35", "09:25") }, "68", Semestre.Anual, 1020));
            turmas.Add(new Turma(so2, new List<Horario>() { new Horario(Dia.Segunda, "09:40", "10:30"), new Horario(Dia.Segunda, "10:30", "11:20") }, "68", Semestre.Anual, 1037));
            turmas.Add(new Turma(compiladores, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "20:20", "21:10") }, "68", Semestre.Semestre_2, "5204b"));
            turmas.Add(new Turma(progSistWeb, new List<Horario>() { new Horario(Dia.Terça, "21:20", "22:10"), new Horario(Dia.Terça, "22:10", "23:00"), new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Sexta, "20:20", "21:10") }, "68", Semestre.Semestre_1, "5188b"));
            turmas.Add(new Turma(poo, new List<Horario>() { new Horario(Dia.Quinta, "19:30", "20:20"), new Horario(Dia.Quinta, "22:20", "21:10"), new Horario(Dia.Sexta, "21:20", "22:10"), new Horario(Dia.Sexta, "22:10", "23:00") }, "68", Semestre.Semestre_1, "5190b"));

            turmas.Add(new Turma(estrutura, new List<Horario>() { new Horario(Dia.Segunda, "13:30", "14:20"), new Horario(Dia.Segunda, "14:20", "15:10"), new Horario(Dia.Terça, "09:40", "10:30"), new Horario(Dia.Terça, "10:30", "11:20"), new Horario(Dia.Sexta, "07:45", "08:35"), new Horario(Dia.Sexta, "08:35", "09:25") }, "102", Semestre.Semestre_1, "6884b"));
            turmas.Add(new Turma(algoritmos, new List<Horario>() { new Horario(Dia.Segunda, "07:45", "08:35"), new Horario(Dia.Segunda, "08:35", "09:25"), new Horario(Dia.Terça, "15:30", "16:20"), new Horario(Dia.Terça, "16:20", "17:10"), new Horario(Dia.Quinta, "07:45", "08:35"), new Horario(Dia.Quinta, "08:35", "09:25") }, "102", Semestre.Semestre_2, "6879c"));
            turmas.Add(new Turma(algoritmos, new List<Horario>() { new Horario(Dia.Segunda, "19:30", "20:20"), new Horario(Dia.Segunda, "20:20", "21:10"), new Horario(Dia.Quarta, "21:20", "22:10"), new Horario(Dia.Quarta, "22:10", "23:00"), new Horario(Dia.Sexta, "19:30", "20:20"), new Horario(Dia.Sexta, "20:20", "21:10") }, "102", Semestre.Semestre_2, "5175c"));

            #endregion


            constantino.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { modelagem, metodosModelagem });
            carniel.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { bd1, bd2 });
            carniel.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { ti, pesquisa });
            airton.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { algoritmos, algoritmosEProg, mf1, mf2 });
            airton.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { estrutura, fundProgramacao });
            anderson.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { compiladores, concorrente });
            anderson.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { arq, arq2, assembly });
            sica.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { cd1, cd2 });
            sica.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { fundamentosEletronica });
            clelia.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { algoritmos, algoritmosEProg, fundProgramacao });
            clelia.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { estrutura });
            dante.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { cg, pesquisa });
            dante.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { topicosPesquisa });
            donizete.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { eng1, eng2, eng3, eng4, pss, iss });
            donizete.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { qualidade, ass });
            elisa.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { ass, qualidade, pss });
            elisa.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { eng1, eng2, eng3, eng4, iss });
            elvio.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { redes });
            elvio.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { fundamentosEletronica, sd });
            flavio.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { so, so2, sd });
            flavio.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { compiladores });
            itana.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { ass, pss, projetoSoftware, gerenciamentoProjeto });
            itana.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { eng1, eng2, eng3, eng4 });
            angelo.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { cd1, cd2 });
            angelo.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { fundamentosEletronica });
            ze.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { paa });
            ze.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { grafos, teoriaDosGrafos });
            jucelia.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { informaticaESociedade });
            jucelia.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { programacaoLinear });
            linnyer.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { lingFormaisAutomatos });
            linnyer.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { informaticaESociedade });
            luciana.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { redes, sd });
            luciana.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { fundamentosEletronica });
            wigarashi.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { ia, poo, progSistWeb });
            wigarashi.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { compiladores, eng1 });
            malbarbo.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { grafos, lp, paradigmaImpOO });
            malbarbo.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { paa });
            madalena.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { bd1, bd2 });
            madalena.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { ti, pesquisa });
            yandre.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { lingFormaisAutomatos });
            yandre.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { informaticaESociedade });
            valeria.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { ord, paradigmaImpOO, lp });
            valeria.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { bd1, bd2 });
            thelma.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { informaticaESociedade, pesquisaOperacional, pesquisa, topicosPesquisa });
            thelma.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { tecAvancadasSI });
            raqueline.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { concorrente });
            raqueline.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { eng3 });
            ronaldo.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { assembly, algoritmos, algoritmosEProg, estrutura });
            ronaldo.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { fundProgramacao });
            sergio.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { ia, ihc });
            sergio.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { lp });
            tania.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { eng1, eng2, eng3 });
            tania.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { ass, pss });
            wesley.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { algoritmos, estrutura, algoritmosEProg });
            wesley.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { lp });
            andre.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { arq, arq2 });
            andre.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { fundamentosEletronica });
            franklin.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { algoritmos, algoritmosEProg, estrutura });
            franklin.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { fundProgramacao });
            nardenio.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { cd1, cd2 });
            nardenio.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { fundamentosEletronica });
            uber.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { algoritmos, algoritmosEProg, estrutura, fundProgramacao });
            uber.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { fundComputacao });
            heloise.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { bd1, bd2, ti });
            heloise.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { pesquisa });
            munif.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { bd1, bd2, poo });
            munif.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { fundComputacao });
            balancieri.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { bd1, bd2, poo, algoritmos, progSistWeb });
            balancieri.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { qualidade });
            juliana.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { paa, pesquisa });
            juliana.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { organizacaoComputadores });
            rodrigo.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { cd1, cd2, paa });
            rodrigo.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { fundamentosEletronica });
            edson.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { poo, progSistWeb });
            edson.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { bd1, bd2 });
            carlosFransley.TurmaAltaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { paa, grafos, teoriaDosGrafos });
            carlosFransley.TurmaMediaPreferencia = getTurmasDadoDisciplina(turmas, new List<Disciplina>() { paradigmaImpOO });



            Disciplinas = new ObservableCollection<Disciplina>(){ compiladores,bd1,bd2,cd1,cd2,cg,computabilidade,concorrente,eng1,eng2,eng3,eng4,estagio,estrutura,fundComputacao,fundProgramacao,fundamentosEletronica,grafos,ihc,informaticaESociedade,iss,labProgramacao,lingFormaisAutomatos,logica,lp,matematicaComputacional,metodosModelagem,mf1,mf2,modelagem,ord,organizacaoComputadores,paa,paradigmaImpOO,pesquisa,pesquisaOperacional,poo,progInterfaceHardSoft,progSistWeb,progSistemas};
            AltaPreferencia = new ObservableCollection<Turma>(turmas);
            MediaPreferencia = new ObservableCollection<Turma>(turmas);
            Indisponibilidade = new ObservableCollection<Turma>(turmas);
            Professores = new ObservableCollection<Professor>(professores);
            Turmas = new ObservableCollection<Turma>(turmas);
            Horarios = new ObservableCollection<Horario>();
            RaisePropertyChanged("Disciplinas");
            RaisePropertyChanged("Professores");
            RaisePropertyChanged("Indisponibilidade");
        }

        public ObservableCollection<Disciplina> Disciplinas { get; set; }
        public ObservableCollection<Turma> AltaPreferencia { get; set; }
        public ObservableCollection<Turma> Indisponibilidade { get; set; }
        public ObservableCollection<Turma> MediaPreferencia { get; set; }
        public ObservableCollection<Professor> Professores { get; set; }
        public ObservableCollection<Turma> Turmas { get; set; }
        public ObservableCollection<Horario> Horarios { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
        public void RaisePropertyChanged(string propertyName)
        {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private List<Turma> getTurmasDadoDisciplina(List<Turma> original, List<Disciplina> disciplina)
        {
            return original.Where(e => disciplina.Contains(e.Disciplina)).ToList();
        }

        internal void gerarArquivo()
        {
            GeradorArquivo.exec(Professores.ToList(), Turmas.ToList());
        }
    }
}
